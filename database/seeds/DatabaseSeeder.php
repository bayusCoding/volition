<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PoolsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(BluesTableSeeder::class);
        $this->call(BlueCategoriesTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    }
}
