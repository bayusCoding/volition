<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Create admin and one test user
        DB::table('users')->delete();

        $users = [
            [
                'firstname' => 'Super',
                'lastname' => 'Admin',
                'email' => 'superadmin@volitioncap.com',
                'password' => Hash::make('superadmin123'),
                'isAdmin' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'firstname' => 'Cregital',
                'lastname' => 'Agency',
                'email' => 'afolabi@cregital.com',
                'password' => Hash::make('folabi'),
                'isAdmin' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'firstname' => 'Adebayo',
                'lastname' => 'Mustafa',
                'email' => 'bayo@cregital.com',
                'password' => Hash::make('muslim'),
                'isAdmin' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        DB::table('users')->insert($users);

        //Create their Wallet account
        DB::table('wallets')->delete();

        $wallets = [
            [
                'user_id' => 1,
                'balance' => 0,
            ],

            [
                'user_id' => 2,
                'balance' => 0,
            ],

            [
                'user_id' => 3,
                'balance' => 0,
            ]
        ];

        DB::table('wallets')->insert($wallets);
    }
}
