<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pools')->delete();

        $ref = [

            [
                'name' => 'Volition Green',
                'slug' => 'green',
                'upgrade' => 250000,
                'admin_fees' => 20000,
                'expiry' => 240,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'Volition Black',
                'slug' => 'black',
                'upgrade' => 500000,
                'admin_fees' => 20000,
                'expiry' => 240,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],

            [
                'name' => 'Volition Red',
                'slug' => 'red',
                'upgrade' => 365000,
                'admin_fees' => 20000,
                'expiry' => 240,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


            [
                'name' => 'Volition Blue',
                'slug' => 'blue',
                'upgrade' => 0,
                'admin_fees' => 0,
                'expiry' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


            [
                'name' => 'Volition Privilege',
                'slug' => 'privilege',
                'upgrade' => 5000000,
                'admin_fees' => 250000,
                'expiry' => 365,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


            [
                'name' => 'Volition Business',
                'slug' => 'business',
                'upgrade' => 5000000,
                'admin_fees' => 250000,
                'expiry' => 365,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


            [
                'name' => 'Volition Green Plus',
                'slug' => 'green-plus',
                'upgrade' => 250000,
                'admin_fees' => 0,
                'expiry' => 365,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],


            [
                'name' => 'Volition Red Plus',
                'slug' => 'red-plus',
                'upgrade' => 365000,
                'admin_fees' => 0,
                'expiry' => 365,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],



            [
                'name' => 'Volition Black Plus',
                'slug' => 'black-plus',
                'upgrade' => 500000,
                'admin_fees' => 0,
                'expiry' => 365,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        DB::table('pools')->insert($ref);
    }
}
