<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('isReferred')->default(0);
            $table->string('referral_id')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('password')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('gender')->nullable();
            $table->string('dob')->nullable();
            $table->string('account_number')->nullable();
            $table->string('account_name')->nullable();
            $table->string('account_bank')->nullable();
            $table->string('account_bvn')->nullable();
            $table->string('occupation')->nullable();
            $table->string('industry')->nullable();
            $table->string('nok_name')->nullable();
            $table->string('nok_email')->nullable();
            $table->string('nok_phone')->nullable();
            $table->string('nok_address')->nullable();
            $table->boolean('isAdmin')->default(0);
            $table->boolean('isActive')->default(1);
            $table->boolean('isBanned')->default(0);
            $table->boolean('isWarned')->default(0);
            $table->boolean('first_login')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
