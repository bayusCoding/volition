<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subscription_number')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('pool_id')->nullable();
            $table->integer('child_pool')->default(0);
            $table->integer('child_pool_origin')->default(0);
            $table->string('child_pool_slot')->default(0);
            $table->string('child_pool_slot_duration')->default(0);
            $table->integer('slots')->nullable();
            $table->string('subscription_fee')->nullable();
            $table->tinyInteger('admin_fees_inc')->default(0);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->tinyInteger('return_period')->default(1);
            $table->tinyInteger('has_paid')->default(0);
            $table->string('payment_ref')->nullable();
            $table->tinyInteger('is_expired')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
