<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Message extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that are date mutable.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * Retrieve owning user
     *
     * @return boolean
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

}
