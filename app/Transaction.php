<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are date mutable.
     *
     * @var array
     */
    protected $guarded = [];
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * one to one relationship between User and Pool
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function subscription(){

        return $this->belongsTo('App\Subscription');
    }

    /**
     * one to one relationship between User and Pool
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function referral(){

        return $this->belongsTo('App\Referral');
    }


    /**
     * one to one relationship between User and Subscription
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user(){

        return $this->belongsTo('App\User');
    }
}
