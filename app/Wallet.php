<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    /**
     * Retrieve owning user
     *
     * @return boolean
     */
    public function user() {
        return $this->belongsTo('App\User');
    }
}
