<?php

namespace App\Console;

use App\Subscription;
use App\User;
use App\Pool;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //check if subscription expires in 3days
        $schedule->call(function () {
            $almost_expired = Subscription::where('end_date', '=', Carbon::now()->addDays(3))->get();
            foreach ($almost_expired as $expire){
                $user = User::where('id', $expire['user_id'])->first();
                $pool = Pool::where('id', $expire['pool_id'])->first();

                //send email to the user
                Mail::send('emails.subtoexpire', ['user' => $user, 'pool' => $pool], function ($mail) use ($user) {
                    $mail->to([$user->email, 'members@volitioncap.com'])
                        ->subject('Your Subscription Expires In 3 Days');
                });
            }
        })->daily();


        //check if subscription  expires today
        $schedule->call(function () {
            $almost_expired = Subscription::where('end_date', Carbon::now())->get();
            foreach ($almost_expired as $expire){
                $user = User::where('id', $expire['user_id'])->first();
                $pool = Pool::where('id', $expire['pool_id'])->first();

                //send email to the user
                Mail::send('emails.subexpiredtoday', ['user' => $user, 'pool' => $pool], function ($mail) use ($user) {
                    $mail->to([$user->email, 'members@volitioncap.com'])
                        ->subject('Your Subscription Expires Today');
                });
            }
        })->daily();


        /**
         * remove this comment to launch the warning
        //check if user has been active for 30days, never been warned and has made no subscription
        $schedule->call(function () {
            $_30DaysUser = User::where('created_at', Carbon::now()->addDays(30))->where('isWarned', 0)->get();
            foreach ($_30DaysUser as $_30Days){
                $activeSub = Subscription::where('user_id', $_30Days['id'])->count();

                if($activeSub == 0){
                    //mark this user as warned
                   User::find($_30Days['id'])->update(['isWarned' => 1]);

                    //send email to the user
                    Mail::send('emails.nosub30days', ['user' => $_30Days], function ($mail) use ($_30Days) {
                        $mail->to([$_30Days['email'], 'members@volitioncap.com'])
                            ->subject('Your Account Will Soon be Archived');
                    });
                }
            }
        })->daily();


        //check if user has been warned and has made no subscription
        $schedule->call(function () {
            $_30DaysUser = User::where('created_at', Carbon::now()->addDays(35))->where('isWarned', 1)->get();
            foreach ($_30DaysUser as $_30Days){
                $activeSub = Subscription::where('user_id', $_30Days['id'])->count();

                if($activeSub == 0){
                    //archive this user
                    User::find($_30Days['id'])->delete();
                }
            }
        })->daily();
         *
         * */
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
