<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Subscription;
use App\Pool;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'isAdmin' => 'boolean',
    ];

    /**
     * The attributes that are date mutable.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * Retrieve messages that belong to user.
     *
     * @return boolean
     */
    public function messages() {
        return $this->hasMany('App\Message');
    }

    /**
     * Retrieve messages that belong to user.
     *
     * @return boolean
     */
    public function referrals() {
        return $this->hasMany('App\Referral');
    }

    /**
     * The attributes would help check if a user KYC details are filled.
     *
     * @return boolean 
     */
    public function getIsKycAttribute() {
        if ($this->account_number == null || $this->account_bank == null || $this->account_name == null) {
            return false;
        }
        return true;
    }

    /**
     * The attributes would help check if a user KYC details are filled.
     *
     * @return boolean
     */
    public function getFullnameAttribute() {
        return $this->firstname . ' ' . $this->lastname;
    }

    /**
     * The attributes would help check if a user KYC details are filled.
     *
     * @return boolean 
     */
    public function getHasActiveSubscriptionAttribute() {
        $subscriptions_count = Subscription::where('user_id', '=', $this->id)->whereDate('end_date', '>', Carbon::now()->toDateString())->count();
        
        if ($subscriptions_count > 0) {
            return true;
        }
        return false;
    }


    public function getActiveSubscriptionAttribute() {
        $latest = Subscription::where('user_id', $this->id)->whereDate('end_date', '>', Carbon::now()->toDateString())->first();

       if(!$latest){
           return 'x';
       }else{
           $pool = Pool::where('id', $latest->pool_id)->first();
           return $pool->name;
       }

    }

    /**
     * Retrieve wallet balance that belong to user.
     *
     * @return boolean
     */
    public function wallet() {
        return $this->hasOne('App\Wallet');
    }
}
