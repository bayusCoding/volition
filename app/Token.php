<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'referral_id',
        'token', 
        'expiry',
    ];

    /**
     * The attributes that are date mutable.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * Retrieve messages that belong to user.
     *
     * @return boolean
     */
    public function referral() {
        return $this->belongTo('App\Referral');
    }
}
