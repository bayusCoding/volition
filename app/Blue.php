<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blue extends Model
{
    /**
     * Date attribues
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get category.
     */
    public function category()
    {
        return $this->belongsTo('App\BlueCategory');
    }
}
