<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Portfolio;
use Carbon\Carbon;
use DatePeriod;
use DateInterval;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are date mutable.
     *
     * @var array
     */
    protected $dates = ['start_date','end_date','created_at', 'deleted_at'];

    /**
     * one to many relationship between User and Pool
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function pool() {

        return $this->belongsTo('App\Pool');
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('has_paid', function (Builder $builder) {
            $builder->where('has_paid', '=', 1);
        });
    }


    /**
     * one to many relationship between User and Subscription
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user(){

        return $this->belongsTo('App\User');
    }

    /**
     * one to one relationship between User and Subscription
     * @return Array
     */
    public function getPortfolioRangeAttribute() {
        //date passed since investment commencement
        $date_range = $this->current_date_range;

        $portfolio = [];

        foreach ($date_range as $date) {
            $date = new Carbon($date);

            $port = Portfolio::where([['month', '=', $date->month], ['year', '=', $date->year], ['pool_id', '=', $this->pool_id]])->first();

            if (is_null($port)) {
               $portfolio[] = 0; 
            } else {
                $portfolio[] = $port->change;
            }   
        }
        return $portfolio;
    }

    /**
     * Get date/months passed from commencement of investment to current date
     * @return Array
     */
    public function getCurrentDateRangeAttribute() {
        $months = [
            '01' => 'Jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec',
        ];
        $current_month = Carbon::now()->startOfMonth();
        $start_month = new Carbon($this->start_date);

        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start_month, $interval, $current_month);

        $month_array = [];

        // if ($start_month->month < $current_month->month) {
            foreach ($period as $date) {
                $month_array[] = $months[$date->format("m")] . " " . $date->format("Y");
            }
        // }
        
        return $month_array;
    }
}
