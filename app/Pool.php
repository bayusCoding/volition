<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pool extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * one to one relationship between User and Subscription
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function subscriptions(){

        return $this->hasMany('App\Subscription');
    }

    /**
     * one to one relationship between User and Pool
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function portfolio(){

        return $this->hasMany('App\Portfolio');
    }


}
