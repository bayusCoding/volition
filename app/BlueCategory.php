<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlueCategory extends Model
{
    /*
     * Date attribues
     *
     * @var array
     */

    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];

    public $table = 'blue_categories';
    
   /**
     * Get Blue Posts.
     */
    public function blues()
    {
        return $this->hasMany('App\Blue');
    }
      
}
