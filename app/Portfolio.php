<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    
    /**
     * The attributes that are date mutable.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * Pool
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function pool(){

        return $this->belongsTo('App\Pool');
    }


}
