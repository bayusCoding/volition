<?php
namespace App\Traits;


trait Functions
{

    protected $result;

    /**
     * Formats timestamps into date and time strings
     * @param $timestamp
     * @param bool $time
     * @return false|string
     */
    public function formatDate($timestamp,$time=true){

        if($time){

            $this->result = date('M j, Y h:ia',strtotime($timestamp));

        }

        return $this->result;

    }


}