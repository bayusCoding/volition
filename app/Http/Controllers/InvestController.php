<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PaymentController as Pay;
use App\Pool;
use App\User;
use App\Subscription;
use App\Transaction;
use App\AdminFees;
use Carbon\Carbon;
use App\Wallet;
use App\Log;
use App\Portfolio;
use Mail;
use Illuminate\Database\Eloquent\Collection;
use League\Csv\Writer;
use Schema;
use SplTempFileObject;
class InvestController extends Controller
{
    // protected $mailchimp;
    // protected $listId = '966e94760e';

    // public function __construct (\Mailchimp $mailchimp){
    //     $this->mailchimp = $mailchimp;
    // }

    /**
     * Get Privilege Pool Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function privilege()
    {
        $metadata = [
            'title' => 'Volition Privilege',
            'menu_active' => 'pool',
        ];
        if (Auth::user()->isAdmin) {
            $subscriptions = Subscription::where('pool_id', '=', 5)->paginate($this->page_limit);
            return view('pools.privilege.index', compact('metadata', 'subscriptions'));
        } else {
            return view('pools.privilege.index', compact('metadata'));
        }
    }
    /**
     * Register user for the Privilege pool.
     *
     * @return \Illuminate\Http\Response
     */
    public function privilegeRegister(Request $request)
    {
        $selected = Pool::findOrFail(5);
        $start_date = new Carbon('first day of next month');
        $start_date->modify('+4 day');
        $data = [
            'subscription_number' => uniqid(),
            'user_id' => $request->user()->id,
            'pool_id' => $selected->id,
            'slots' => 1,
            'has_paid' => 1,
            'start_date' => $start_date,
            'end_date' => $start_date->copy()->addDays($selected->expiry),
        ];
        Subscription::unguard();
        Subscription::create($data);
        Subscription::reguard();
        //Log action/event
        Log::create(['user_id' => $request->user()->id, 'log_desc' => 'registered for Volition Privilege']);
        //Send Email Notification
        $mail_data = ['name' => $selected->name, 'amount' => 0, 'expiry' => $start_date->copy()->addDays($selected->expiry)->format('l jS F Y')];
        Mail::send('emails.subscription', $mail_data, function($message) use ($request, $selected) {
            $message->to([$request->user()->email, 'members@volitioncap.com'])
                ->subject('New ' . $selected->name . ' Pool Subscription');
        });
        return back()->with('success', 'You have successfully registered for Volition Privilege. We will contact you shortly');
    }
    /**
     * Get Business Pool Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function business()
    {
        $metadata = [
            'title' => 'Volition Business',
            'menu_active' => 'pool',
        ];
        if (Auth::user()->isAdmin) {
            $subscriptions = Subscription::where('pool_id', '=', 6)->paginate($this->page_limit);
            return view('pools.business.index', compact('metadata', 'subscriptions'));
        } else {
            return view('pools.business.index', compact('metadata'));
        }
    }
    /**
     * Register user for the business pool.
     *
     * @return \Illuminate\Http\Response
     */
    public function businessRegister(Request $request)
    {
        $selected = Pool::findOrFail(6);
        $start_date = new Carbon('first day of next month');
        $start_date->modify('+4 day');
        $data = [
            'subscription_number' => uniqid(),
            'user_id' => $request->user()->id,
            'pool_id' => $selected->id,
            'slots' => 1,
            'has_paid' => 1,
            'start_date' => $start_date,
            'end_date' => $start_date->copy()->addDays($selected->expiry),
        ];
        Subscription::unguard();
        Subscription::create($data);
        Subscription::reguard();
        //Log action/event
        Log::create(['user_id' => $request->user()->id, 'log_desc' => 'registered for Volition Business']);
        //Send Email Notification
        $mail_data = ['name' => $selected->name, 'amount' => 0, 'expiry' => $start_date->copy()->addDays($selected->expiry)->format('l jS F Y')];
        Mail::send('emails.subscription', $mail_data, function($message) use ($request, $selected) {
            $message->to([$request->user()->email, 'members@volitioncap.com'])
                ->subject('New ' . $selected->name . ' Pool Subscription');
        });
        return back()->with('success', 'You have successfully registered for Volition Business. We will contact you shortly');
    }
    /**
     * Show the form for creating pool.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $metadata = [
            'title' => 'Pool',
            'menu_active' => 'pool',
        ];
        $pools = Pool::pluck('name', 'id');
        $pool = $request->pool;
        $selected = Pool::where('name', '=', $pool)->firstOrFail();
        $fees = AdminFees::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $selected->id]])->whereDate('expiry', '>', Carbon::now()->toDateString())->first();
        $p_b_admin_fee_exist = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $selected->id], ['admin_fees_inc', '=', 1], ['has_paid', 1]])->first();
        $admin_fees = (is_null($p_b_admin_fee_exist)) ? true : false;
        $p_b_admin_fee_exist = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $selected->id], ['admin_fees_inc', '=', 1], ['has_paid', 1]])->count();
        //populate the plus names and values
        $green_plus = Pool::where('slug', 'green_plus')->first();
        $red_plus = Pool::where('slug', 'red_plus')->first();
        $black_plus = Pool::where('slug', 'black_plus')->first();
        return view('pools.create', compact('pools', 'selected', 'admin_fees', 'metadata', 'p_b_admin_fee_exist', 'green_plus', 'red_plus', 'black_plus'));
    }
    /**
     * Show Investment history for all or specific pool
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if ($request->pool != null)
        {
            $pool = $request->pool;
            $selected = Pool::where('name', '=', $pool)->firstOrFail();
            $admin_fees = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $selected->id], ['admin_fees_inc', '=', 1], ['has_paid', 1]])->whereDate('end_date', '>', Carbon::now())->first();
            $admin_fees_expiry = (is_null($admin_fees)) ? null : $admin_fees->end_date;
            //Get current pool portfolio metric for  previous month
            $sub = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $selected->id]]);
            if($sub->count() == 0) {
                $change = 0;
            }else{
                $sub_month = ltrim($sub->first()->created_at->format('m'), 0);
                $sub_year = ltrim($sub->first()->created_at->format('Y'), 0);
                $currentdate = new Carbon('first day of previous month');
                $currentmonth = $currentdate->month;
                if($sub_month > $currentmonth){
                    $currentmonth = $sub_month;
                }else{
                    $currentmonth = $currentmonth;
                }
                /**
                $month = $currentmonth - $sub_month;
                $currentmonth = $sub_month + $month;
                 * **/
                $port = Portfolio::where([['month', '=', $currentmonth], ['year', '=', $currentdate->year], ['pool_id', '=', $selected->id]])->first();
                if(is_null($port)){
                    $change = 0;
                }else{
                    $change = $port->change;
                }
            }
            $metadata = [
                'title' => $selected->name,
                'slug' => $selected->slug,
                'menu_active' => 'pool',
                'current_pool_change' => $change
            ];
            if ($request->user()->isAdmin) {
                $subscriptions = Subscription::where('pool_id', '=', $selected->id)->whereDate('end_date', '>', Carbon::now()->toDateString())->get();
            } else {
                $subscriptions = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $selected->id]])->whereDate('end_date', '>', Carbon::now())->get();
                //get count of commpleted investements
                $commpleted_count = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $selected->id]])->whereDate('end_date', '<', Carbon::now())->count();
                $metadata['pool_completed'] = $commpleted_count;
            }
            $pool = Pool::all();
            return view('pools.show', compact('subscriptions', 'admin_fees_expiry', 'metadata', 'pool'));
        } else {
            $metadata = [
                'title' => 'Investment History',
                'menu_active' => 'investments',
            ];
            if ($request->user()->isAdmin) {
                $subscriptions = Subscription::get();
            } else {
                $subscriptions = Subscription::where('user_id', '=', $request->user()->id)->get();
            }
            return view('pools.history', compact('subscriptions','metadata'));
        }
    }
    /**
     * Show Investment history for all or specific pool
     *
     * @return \Illuminate\Http\Response
     */
    public function investment(Request $request)
    {
        if ($request->number != null) {
            $subscription = Subscription::where('subscription_number', '=', $request->number)->first();
            //get the number of months the subscription has been added
            $pool = $subscription->pool;
            if($pool->id == 5 || $pool->id == 6){
                //get the latest pool management update
                $last_pool_green_plus = Portfolio::where('pool_id', 7)->orderBy('month', 'desc')->first();
                $last_port_date_green_plus = Carbon::create($last_pool_green_plus->year, $last_pool_green_plus->month, 1);
                $last_pool_red_plus = Portfolio::where('pool_id', 8)->orderBy('created_at', 'desc')->first();
                $last_port_date_red_plus = Carbon::create($last_pool_red_plus->year, $last_pool_red_plus->month, 1);
                $last_pool_black_plus = Portfolio::where('pool_id', 9)->orderBy('created_at', 'desc')->first();
                $last_port_date_black_plus = Carbon::create($last_pool_black_plus->year, $last_pool_black_plus->month, 1);
                //Get current pool portfolio metric for  previous month
                $sub = Subscription::where([['user_id', '=', $subscription->user_id], ['pool_id', '=', $pool->id]]);
                if($sub->count() == 0) {
                    $change = 0;
                }else{
                    $sub_date = Carbon::parse($subscription->start_date);
                    $sub_month = ltrim($subscription->start_date->format('m'), 0);
                    $sub_year = ltrim($subscription->start_date->format('Y'), 0);
                    $diff_green_plus = $last_port_date_green_plus->diffInDays($sub_date);
                    if($diff_green_plus  < 25){
                        $diff_green_plus  = 1;
                    }elseif ($diff_green_plus  < 50){
                        $diff_green_plus  = 2;
                    }elseif ($diff_green_plus  < 75){
                        $diff_green_plus  = 3;
                    }elseif ($diff_green_plus  < 100){
                        $diff_green_plus  = 4;
                    }elseif ($diff_green_plus  < 125){
                        $diff_green_plus  = 5;
                    }elseif ($diff_green_plus  < 150){
                        $diff_green_plus  = 6;
                    }elseif ($diff_green_plus  < 175){
                        $diff_green_plus  = 7;
                    }elseif ($diff_green_plus  < 200){
                        $diff_green_plus  = 8;
                    }else{
                        $diff_green_plus  = 0;
                    }
                    $diff_red_plus = $last_port_date_red_plus->diffInDays($sub_date);
                    if($diff_red_plus  < 25){
                        $diff_red_plus  = 1;
                    }elseif ($diff_red_plus  < 50){
                        $diff_red_plus  = 2;
                    }elseif ($diff_red_plus  < 75){
                        $diff_red_plus  = 3;
                    }elseif ($diff_red_plus  < 100){
                        $diff_red_plus  = 4;
                    }elseif ($diff_red_plus  < 125){
                        $diff_red_plus  = 5;
                    }elseif ($diff_red_plus  < 150){
                        $diff_red_plus  = 6;
                    }elseif ($diff_red_plus  < 175){
                        $diff_red_plus  = 7;
                    }elseif ($diff_red_plus  < 200){
                        $diff_red_plus  = 8;
                    }else{
                        $diff_red_plus  = 0;
                    }
                    $diff_black_plus = $last_port_date_black_plus->diffInDays($sub_date);
                    if($diff_black_plus  < 25){
                        $diff_black_plus  = 1;
                    }elseif ($diff_black_plus  < 50){
                        $diff_black_plus  = 2;
                    }elseif ($diff_black_plus  < 75){
                        $diff_black_plus  = 3;
                    }elseif ($diff_black_plus  < 100){
                        $diff_black_plus  = 4;
                    }elseif ($diff_black_plus  < 125){
                        $diff_black_plus  = 5;
                    }elseif ($diff_black_plus  < 150){
                        $diff_black_plus  = 6;
                    }elseif ($diff_black_plus  < 175){
                        $diff_black_plus  = 7;
                    }elseif ($diff_black_plus  < 200){
                        $diff_black_plus  = 8;
                    }else{
                        $diff_black_plus  = 0;
                    }
                    $currentdate = new Carbon('first day of previous month');
                    $currentmonth = $currentdate->month;
                    if($sub_month > $currentmonth){
                        $currentmonth = $sub_month;
                    }else{
                        $currentmonth = $currentmonth;
                    }
                    $green_port = Portfolio::where([['month', '=', $currentmonth], ['year', '=', $currentdate->year], ['pool_id', '=', 7]])->first();
                    $red_port = Portfolio::where([['month', '=', $currentmonth], ['year', '=', $currentdate->year], ['pool_id', '=', 8]])->first();
                    $black_port = Portfolio::where([['month', '=', $currentmonth], ['year', '=', $currentdate->year], ['pool_id', '=', 9]])->first();
                    if(is_null($green_port) && is_null($red_port) && is_null($black_port)){
                        $green_change = 0 * $diff_green_plus;
                        $red_change = 0 * $diff_red_plus;
                        $black_change = 0 * $diff_black_plus;
                    }else{
                        $green_change = $green_port->change * $diff_green_plus;
                        $red_change = $red_port->change * $diff_red_plus;
                        $black_change = $black_port->change * $diff_black_plus;
                    }
                    $change = 0;
                }
            }else{
                //get the latest pool management update
                $last_pool = Portfolio::where('pool_id', $pool->id)->orderBy('month', 'desc')->first();
                $last_port_date = Carbon::create($last_pool->year, $last_pool->month, 1);
                //Get current pool portfolio metric for  previous month
                $sub = Subscription::where([['user_id', '=', $subscription->user_id], ['pool_id', '=', $pool->id]]);
                if($sub->count() == 0) {
                    $change = 0;
                }else{
                    $sub_date = Carbon::parse($subscription->start_date);
                    $sub_month = (int)ltrim($subscription->start_date->format('m'), 0);
                    $sub_year = (int)ltrim($subscription->start_date->format('Y'), 0);

                    $diff = $last_port_date->diffInDays($sub_date);

                    if($diff < 25){
                        $diff = 1;
                    }elseif ($diff < 50){
                        $diff = 2;
                    }elseif ($diff < 75){
                        $diff = 3;
                    }elseif ($diff < 100){
                        $diff = 4;
                    }elseif ($diff < 125){
                        $diff = 5;
                    }elseif ($diff < 150){
                        $diff = 6;
                    }elseif ($diff < 175){
                        $diff = 7;
                    }elseif ($diff < 200){
                        $diff = 8;
                    }else{
                        $diff = 0;
                    }
                    $currentdate = new Carbon('first day of previous month');
                    $currentmonth = $currentdate->month;
                    if($sub_month > $currentmonth){
                        $currentmonth = $sub_month;
                    }else{
                        $currentmonth = $currentmonth;
                    }
                    $port = Portfolio::where([['month', '=', $currentmonth], ['year', '=', $currentdate->year], ['pool_id', '=', $pool->id]])->first();
                    if(is_null($port)){
                        $change = 0 * $diff;
                    }else{
                        $change = $port->change * $diff;
                    }
                    $green_change = 0;
                    $red_change = 0;
                    $black_change = 0;
                }
            }
            $metadata = [
                'title' => 'Investment Details',
                'menu_active' => 'pool',
                'slug' => $pool->slug,
                'current_pool_change' => $change,
                'current_pool_green_plus_change' => $green_change,
                'current_pool_red_plus_change' => $red_change,
                'current_pool_black_plus_change' => $black_change
            ];
            $pool = Pool::all();
            return view('pools.details', compact('subscription', 'metadata', 'pool'));
        } else {
            return back();
        }
    }
    /**
     * Show the form for creating pool.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $selected = Pool::findOrFail($request->pool);
        // $path_array = explode("/", urldecode (parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH)));
        // $selected->name == $path_array[2]
        if (Auth::user()->is_Kyc) {
            //generate subscription_id
            $subscription_number = uniqid();
            $subscription_fee = 0;
            $amount = 0;
            $admin_fees = AdminFees::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $request->pool]])->whereDate('expiry', '>', Carbon::now())->first();
            $p_b_admin_fee_exist = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $request->pool], ['admin_fees_inc', '=', 1], ['has_paid', 1]])->first();
            $admin_fees = (is_null($p_b_admin_fee_exist)) ? true : false;
            //calculate the actual subscription fee to be paid for the current subscription transaction
            //sub is for business or privilege
            if($selected->id == 5 || $selected->id == 6){
                if ($admin_fees) {
                    $subscription_fee = $request->upgrade;
                    $fees = $subscription_fee + $selected->admin_fees;
                    //bank deduction charges to client
                    $amount = $fees + 0;
                    $request->request->add([
                        'admin_fees_inc' => 1,
                    ]);
                } else {
                    $subscription_fee = $request->upgrade;
                    $fees = $request->upgrade;
                    //bank deduction charges to client
                    $amount = $fees + 0;
                    $request->request->add([
                        'admin_fees_inc' => 0,
                    ]);
                }
                $child = 1;
                $child_slot = $request->green_plus.'+'.$request->red_plus.'+'.$request->black_plus;
                $child_slot_duration = $request->green_month.'+'.$request->red_month.'+'.$request->black_month;
                $request->request->add([
                    'slots' => 1,
                ]);
            }else{
                if ($admin_fees) {
                    $subscription_fee = $selected->upgrade * $request->slots;
                    $fees = $subscription_fee + $selected->admin_fees;
                    //bank deduction charges to client
                    $amount = $fees + 0;
                    $request->request->add([
                        'admin_fees_inc' => 1,
                    ]);
                } else {
                    $subscription_fee = $selected->upgrade * $request->slots;
                    $fees = $selected->upgrade * $request->slots;
                    //bank deduction charges to client
                    $amount = $fees + 0;
                    $request->request->add([
                        'admin_fees_inc' => 0,
                    ]);
                }
                $child = 0;
                $child_slot = 0;
                $child_slot_duration = 0;
            }

            //set subscription start dates
            $start_date = new Carbon('first day of next month');
            $start_date->modify('+4 day');
            $end_date = $start_date->copy()->addDays($selected->expiry)->startOfMonth()->modify('+4 day');

            $metaarray = array(
                ['metaname' => 'subscription_number', 'metavalue' => $subscription_number],
                ['metaname' => 'pool', 'metavalue' => $selected->id],
                ['metaname' => 'admin_fees', 'metavalue' => $request->admin_fees_inc]
            );

            $request->request->add([
                'user_id' => $request->user()->id,
                'pool_id' => $request->pool,
                'subscription_fee' => $subscription_fee,
                'subscription_number' => $subscription_number,
                'start_date' => $start_date->toDateString(),
                'end_date' => $end_date->toDateString(),
                //metadata for Payment Platform
                'firstname' => $request->user()->firstname,
                'lastname' => $request->user()->lastname,
                'email' => $request->user()->email,
                'phonenumber' => $request->user()->phone,
                'amount' => $amount,
                'payment_method' => 'card',
                'description' => 'Volition Pool Subscription',
                'title' => 'Volition Capital',
                'pay_button_text' => 'Complete Payment',
                'country' => 'NG',
                'currency' => 'NGN',
                'metadata' => json_encode($metaarray),
                'child_pool' => $child,
                'child_pool_slot' => $child_slot,
                'child_pool_slot_duration' => $child_slot_duration
            ]);

            $data = $request->except(['_token', 'pool', 'upgrade', 'admin_fees', 'firstname', 'lastname', 'email', 'phonenumber',
                'amount', 'country', 'currency', 'metadata', 'green_plus', 'red_plus', 'black_plus', 'green_month', 'red_month',
                'black_month', 'payment_method', 'description', 'title', 'pay_button_text']);
            Subscription::unguard();
            Subscription::create($data);
            Subscription::reguard();

            // try{
            //     $this->mailchimp
            //     ->lists
            //     ->subscribe(
            //         $this->listId,
            //         ['email' => $request->user()->email],
            //         [
            //             'FNAME' => $request->user()->firstname,
            //             'LNAME' => $request->user()->lastname,
            //             'ADDRESS' => ' ',
            //             'PHONE' => $request->user()->phone,
            //             'groupings' => [
            //                 0 => array(
            //                     'name' => "Investment Pools",
            //                     'groups' => [$request->pool()->slug.'_'.$start_date]
            //                 )
            //             ]
            //         ],
            //         ['status' => 'subscribed'],
            //         ['double_optin' => false]
            //     );
            // }
            // catch (\Mailchimp_List_AlreadySubscribed $e) {
            // // do something
            // } catch (\Mailchimp_Error $e) {
            // // do something
            // }
            

            if(env('PAYMENT_METHOD') == 'paystack'){
                Pay::paystackPay();
            }else{
                Pay::ravePay($subscription_number, 'payment.callback');
            }
        } else {
            return redirect()->back()->with('error', 'You cannot invest. Please ensure KYC detail is filled. Click <a href="/settings">here</a>');
        }

    }


    /**
     * Process Payment Gateway Callback.
     *
     * @return \Illuminate\Http\Response
     */
    public function handlePaymentGatewayCallback(Request $request)
    {

        if (!$request->cancelled) {

            if(env('PAYMENT_METHOD') == 'paystack') {
                $callback = Pay::paystackPayCallback();
                $subscription_number = $callback->metadata[0]->metavalue;
                $payment_ref = $callback->metadata[0]->metavalue; //$callback->reference;
                $amount = $callback->amount / 100;
            }else{
                $callback = Pay::ravePayCallback(); //dd($callback);
                $subscription_number = $callback->txref; //dd($subscription_number); //$callback->metadata->subscription_number;
                $payment_ref = $callback->txref; //$callback->reference;
                $amount = $callback->amount;
            }

            $today = Carbon::now();
            $subscription = Subscription::withoutGlobalScopes()->where('subscription_number', $subscription_number)->firstOrFail(); //dd($subscription);
            $selected = Pool::findOrFail($subscription->pool_id);//$callback->metadata->pool); //dd($selected);
            $user = Auth::user();
            //update subscription to has paid
            $subscription->has_paid = 1;
            $subscription->save();
            //Save admin fees payment
            if ($subscription->admin_fees_inc == 1) {
                //Check if this is the first admin fee payment for pool
                // $admin_fees = AdminFees::where([['user_id', '=', $user->id], ['pool_id', '=', $selected->id],]); //dd($admin_fees);
                $admin_fees = Subscription::where([['user_id', '=', $user->id], ['pool_id', '=', $selected->id], ['admin_fees_inc', '=', 1], ['has_paid', 1]])->whereDate('end_date', '>', Carbon::now());
                if (!is_null($admin_fees)) {
                    // pay referrer 20% of admin_fee
                    if(!is_null($user->referall)) {
                        $balance = $user->referral->user->wallet->balance;
                        $wallet = Wallet::findOrFail($user->referral->user->id);
                        $wallet->balance = $balance + ($selected->admin_fees * 0.2);
                        $wallet->update();
                    }
                }

                $admin_fees_data = [
                    'user_id' => $user->id,
                    'pool_id' => $selected->id,
                    'payment_ref' => $payment_ref,
                    'has_paid' => 1,
                    'expiry' => $today->addDays($selected->expiry)->toDateString(),
                ];
                //save admin fees
                AdminFees::unguard();
                AdminFees::create($admin_fees_data);
                AdminFees::reguard();
            }
            $transaction_data = [
                'user_id' => $user->id,
                'subscription_id' => $subscription->id,
                'amount' => $amount,
                'trans_ref' => $payment_ref,
                'trans_desc' => $selected->name . ' Pool subscription',
                'trans_type' => 0
            ];
            Transaction::unguard();
            Transaction::create($transaction_data);
            Transaction::reguard();
            //Log action/event
            Log::create(['user_id' => Auth::user()->id, 'log_desc' => 'invested ' . $subscription->subscription_fee .' in to the ' . $subscription->pool->name . ' pool']);
            //Send Email Notification
            $mail_data = ['name' => $selected->name, 'amount' => $subscription->subscription_fee, 'expiry' => $subscription->end_date->format('l jS F Y')];
            Mail::send('emails.subscription', $mail_data, function($message) use ($user, $selected) {
                $message->to([$user->email, 'members@volitioncap.com', 'ask@volitioncap.com'])
                    ->subject('New ' . $selected->name . ' Pool Subscription');
            });

            //redirect to the subscribed pool history
            return redirect(route('pools.pool.show', ['name' => $selected->name]));
        } else {
            return redirect(route('users.home'));
        }
    }


    private function createCsv(Collection $modelCollection, $tableName){
        $csv = Writer::createFromFileObject(new SplTempFileObject());

        // This creates header columns in the CSV file - probably not needed in some cases.
        $csv->insertOne(Schema::getColumnListing($tableName));

        foreach ($modelCollection as $data){
            $csv->insertOne($data->toArray());
        }

        $csv->output($tableName . '.csv');

    }
    public function getMainInvestmentData(){
        $user = Subscription::all();
        // Note: $mainMeta is a Collection object
        //(returning a 'collection' of data from using 'all()' function),
        //so can be passed in below.
        $this->createCsv($user, 'investment');
    }
}
