<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Message;
use App\User;
use App\Log;
use App\Subscription;
use Mail;

class MessageController extends Controller
{
    /**
     * Get Index
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata = [
            'title' => 'Support',
            'menu_active' => 'messages',
        ];

        if ($request->user()->isAdmin) {
           //$messages = Message::where('message_to', '=', 1)->orderBy('id', 'desc')->paginate(50);
            $messages = DB::table('messages')
              ->join('users', 'messages.user_id', '=', 'users.id')
              ->select('messages.*', 'users.email', 'users.id', 'users.firstname', 'users.lastname')
              ->where('messages.message_to', '=', 1)
              ->orderBy('messages.created_at', 'desc')
            ->paginate(50);
        } else {
            $messages = Message::where('user_id', '=', $request->user()->id)->orWhere('message_to', '=', $request->user()->id)->orderBy('created_at', 'desc')->get();
        }

        return view('messages.index', compact('messages', 'metadata'));
    }

    /**
     * Create
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show contact
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $messages = Message::where('user_id', '=', $request->to)->orWhere('message_to', '=', $request->to)->orderBy('created_at', 'desc')->get();

        $user = User::find($request->to);

        $metadata = [
            'to' => $user->id,
            'name' => $user->firstname . ' ' . $user->lastname,
            'menu_active' => 'messages',
        ];

        return view('messages.show', compact('messages', 'metadata'));
    }

    /**
     * Store a new message.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation());

        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        if($request->user()->isAdmin) {
            $request->request->add([
                'user_id' => 1,
                'type' => 1,
            ]);
        } else {
            $request->request->add([
                'user_id' => $request->user()->id,
                'message_to' => 1,
            ]);
        }

        $data = $request->except(['_token']);

        Message::create($data);

        if($request->user()->isAdmin) {
            //Log action/event
            $user = User::find($request->message_to);
            Log::create(['user_id' => $request->user()->id, 'log_desc' => 'sent a message to ' . $user->firstname . ' ' . $user->lastname]);

            //Send Email Notification of Message to ask and members
            $mail_data = [
                'title' => 'Admin sent you a message on Volition',
                'body' => '
           '.nl2br($request->body).'<br>
            <br> <a href="' . route("users.contact") . '" style="padding: 14px 50px;   border: 1px solid #A0830B;  border-radius: 3px; background-color: #A0830B; color: #fff;  box-shadow: 0 3px 12px 2px rgba(160,131,11,0.3);font-size: 18px;   font-weight: 500; font-family: Helvetica;   line-height: 22px;text-decoration: none">Read</a><br><br>',
            ];
            Mail::send('emails.messaging', $mail_data, function($message) use ($user) {
                $message->to($user->email)
                    ->subject('New Message from Admin on Volition');
            });

        } else {
            Log::create(['user_id' => $request->user()->id, 'log_desc' => 'sent a message to support']);

            //Send Email Notification of Message to ask and members
            $mail_data = [
                'title' => $request->user()->firstname . ' ' . $request->user()->lastname . ' sent a message',
                'body' => $request->user()->firstname . ' ' . $request->user()->lastname . ' with email address ' . $request->user()->email . ' has sent a message to Admin on Volition. <br> <br>
          '.nl2br($request->body).'<br>
 <br> <a href="' . route("messages.show", ["to" =>  $request->user()->id]) . '" style="padding: 14px 50px;   border: 1px solid #A0830B;  border-radius: 3px; background-color: #A0830B; color: #fff;  box-shadow: 0 3px 12px 2px rgba(160,131,11,0.3);font-size: 18px;   font-weight: 500; font-family: Helvetica;   line-height: 22px;text-decoration: none">Read</a><br><br>',
            ];
            Mail::send('emails.messaging', $mail_data, function($message) use ($request) {
                $message->to(['members@volitioncap.com', 'ask@volitioncap.com'])
                ->subject('New Message from ' . $request->user()->firstname . ' ' . $request->user()->lastname);
            });
        }

        return redirect()->back();
    }

    public function broadcast(Request $request) {

        if($request->receipient == 'all') {
            $users = User::where('isAdmin', '=', false)->get();
            foreach($users as $user) {
                //Log::create(['user_id' => 1, 'log_desc' => 'sent a message to ' . $user->firstname . ' ' . $user->lastname]);
                Message::create(['user_id' => 1, 'type' => 1, 'message_to' => $user->id, 'body' => $request->msg]);


                //Send Email Notification of Message to ask and members
                $mail_data = [
                    'title' => $request->subject,
                    'body' => '
                    '.nl2br($request->msg).'<br>
 <br> <a href="' . route("users.contact") . '" style="padding: 14px 50px;   border: 1px solid #A0830B;  border-radius: 3px; background-color: #A0830B; color: #fff;  box-shadow: 0 3px 12px 2px rgba(160,131,11,0.3);font-size: 18px;   font-weight: 500; font-family: Helvetica;   line-height: 22px;text-decoration: none">Read</a><br><br>',
                ];


                Mail::send('emails.messaging', $mail_data, function($message) use ($user, $request) {
                    $message->to($user->email)
                        ->subject($request->subject); //New Message from Super Admin on Volition replaced
                });

            }

        }else{
            $users = DB::table('subscriptions')
                ->join('users', 'subscriptions.user_id', '=', 'users.id')
                ->select('subscriptions.*', 'users.*')
                ->where('has_paid', '=', '1')
                ->groupBy('subscriptions.user_id')
                ->get();

                foreach($users as $user) {
                    //Log::create(['user_id' => 1, 'log_desc' => 'sent a message to ' . $user->firstname . ' ' . $user->lastname]);
                    Message::create(['user_id' => 1, 'type' => 1, 'message_to' => $user->id, 'body' => $request->msg]);


                    //Send Email Notification of Message to ask and members
                    $mail_data = [
                        'title' => $request->subject,
                        'body' => '
  '.nl2br($request->msg).'<br>
  <br> <a href="' . route("users.contact") . '" style="padding: 14px 50px;   border: 1px solid #A0830B;  border-radius: 3px; background-color: #A0830B; color: #fff;  box-shadow: 0 3px 12px 2px rgba(160,131,11,0.3);font-size: 18px;   font-weight: 500; font-family: Helvetica;   line-height: 22px;text-decoration: none">Read</a><br><br>',
                    ];


                    Mail::send('emails.messaging', $mail_data, function($message) use ($user, $request) {
                        $message->to($user->email)
                            ->subject($request->subject); //New Message from Super Admin on Volition replaced
                    });


                }
        }

        return redirect(route('messages.index'))->with('success', 'Broadcast message sent');
    }


    /**
     * Get a validator for an incoming message request.
     *
     * @return array
     */
    protected function validation()
    {
        return [
            'body' => 'required'
        ];
    }


    /**
     * Delete
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $message = Message::findOrFail($request->id);
        $to = $message->message_to;

        $message->delete();

        return redirect(url('/messages/show?to='.$to));
    }
}
