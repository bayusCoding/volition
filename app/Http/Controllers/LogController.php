<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Log;

class LogController extends Controller
{

    /**
     * Get Blue Pool Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata = [
            'title' => 'Logs',
            'menu_active' => 'logs',
        ];

        if ($request->user()->isAdmin) {
        	$logs = Log::orderBy('id', 'desc')->paginate($this->page_limit);
        } else {
        	$logs = Log::where('user_id', '=', $request->user()->id)->orderBy('id', 'desc')->paginate($this->page_limit);
        }

        return view('logs.index', compact('logs', 'metadata'));
    }

}