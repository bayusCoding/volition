<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Message;
use App\User;
use App\Subscription;
use App\Transaction;
use App\Log;
use Carbon\Carbon;
use App\Portfolio;
use App\Blue;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata = [
            'title' => 'Dashboard',
            'menu_active' => 'dashboard',
        ];

        if ($request->user()->isAdmin) {

            //Get current pool portfolio metric for  previous month
            $date = new Carbon('first day of previous month');
            $green_port = Portfolio::where([['month', '=', $date->month], ['year', '=', $date->year], ['pool_id', '=', 1]])->first();
            $black_port = Portfolio::where([['month', '=', $date->month], ['year', '=', $date->year], ['pool_id', '=', 2]])->first();
            $red_port = Portfolio::where([['month', '=', $date->month], ['year', '=', $date->year], ['pool_id', '=', 3]])->first();

            $greens = Subscription::where('pool_id', '=', 1);
            $green_metrics = [
                'count' => $greens->count(),
                'amount' => $greens->sum('subscription_fee'),
                'change' => (is_null($green_port)) ? 0 : $green_port->change
            ];

            $blacks = Subscription::where('pool_id', '=', 2);
            $black_metrics = [
                'count' => $blacks->count(),
                'amount' => $blacks->sum('subscription_fee'),
                'change' => (is_null($black_port)) ? 0 : $black_port->change
            ];

            $reds = Subscription::where('pool_id', '=', 3);
            $red_metrics = [
                'count' => $reds->count(),
                'amount' => $reds->sum('subscription_fee'),
                'change' => (is_null($red_port)) ? 0 : $red_port->change
            ];

            $transactions = Transaction::orderBy('id', 'desc')->limit(5)->get();

            $logs = Log::orderBy('id', 'desc')->limit(5)->get();
        } else {


            $greens = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', 1]]);
            if($greens->count() == 0){
                $change = 0;
            }else{
                $green_month = ltrim($greens->first()->created_at->format('m'), 0);
                $green_year = ltrim($greens->first()->created_at->format('Y'), 0);

                $currentdate = new Carbon('first day of previous month');
                $currentmonth = $currentdate->month;

                if($green_month > $currentmonth){
                    $currentmonth = $green_month;
                }else{
                    $currentmonth = $currentmonth;
                }

                $green_port = Portfolio::where([['month', '=', $currentmonth], ['year', '=', $currentdate->year], ['pool_id', '=', 1]])->first();
                if(is_null($green_port)){
                    $change = 0;
                }else{
                    $change = $green_port->change;
                }
            }
            $green_metrics = [
                'count' => $greens->count(),
                'amount' => $greens->sum('subscription_fee'),
                'change' => $change
            ];



            $blacks = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', 2]]);
            if($blacks->count() == 0){
                $change = 0;
            }else{
                $black_month = ltrim($blacks->first()->created_at->format('m'), 0);
                $black_year = ltrim($blacks->first()->created_at->format('Y'), 0);

                $currentdate = new Carbon('first day of previous month');
                $currentmonth = $currentdate->month;

                if($black_month > $currentmonth){
                    $currentmonth = $black_month;
                }else{
                    $currentmonth = $currentmonth;
                }

                $black_port = Portfolio::where([['month', '=', $currentmonth], ['year', '=', $currentdate->year], ['pool_id', '=', 2]])->first();
                if(is_null($black_port)){
                    $change = 0;
                }else{
                    $change = $black_port->change;
                }
            }
            $black_metrics = [
                'count' => $blacks->count(),
                'amount' => $blacks->sum('subscription_fee'),
                'change' => $change
            ];



            $reds = Subscription::where([['user_id', '=', $request->user()->id], ['pool_id', '=', 3]]);
            if($reds->count() == 0){
                $change = 0;
            }else{
                $red_month = ltrim($reds->first()->created_at->format('m'), 0);
                $red_year = ltrim($reds->first()->created_at->format('Y'), 0);

                $currentdate = new Carbon('first day of previous month');
                $currentmonth = $currentdate->month;

                if($red_month > $currentmonth){
                    $currentmonth = $red_month;
                }else{
                    $currentmonth = $currentmonth;
                }

                $red_port = Portfolio::where([['month', '=', $currentmonth], ['year', '=', $currentdate->year], ['pool_id', '=', 3]])->first();
                if(is_null($red_port)){
                    $change = 0;
                }else{
                    $change = $red_port->change;
                }
            }
            $red_metrics = [
                'count' => $reds->count(),
                'amount' => $reds->sum('subscription_fee'),
                'change' => $change
            ];



            $transactions = Transaction::where('user_id', '=', $request->user()->id)->orderBy('id', 'desc')->limit(5)->get();

            $logs = Log::where('user_id', '=', $request->user()->id)->orderBy('id', 'desc')->limit(5)->get();
        }

        return view('index', compact('transactions', 'logs', 'green_metrics', 'black_metrics', 'red_metrics', 'metadata'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function gettingStarted()
    {
        $metadata = [
            'title' => 'Getting Started',
            'menu_active' => 'started',
        ];

        $page_content = Blue::where([['title', '=','Getting Started'], ['blue_category_id', '=', 2]])->first()->markdown;

        return view('getting_started', compact('page_content', 'metadata'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function tandc()
    {
        $metadata = [
            'title' => 'Terms and Conditions',
            'menu_active' => 'started',
        ];

        $page_content = Blue::where([['title', '=','Terms and Conditions'], ['blue_category_id', '=', 2]])->first()->markdown;

        return view('tandc', compact('page_content', 'metadata'));
    }


    public function bylaw()
    {
        $metadata = [
            'title' => 'Cooperative Bye-Laws',
            'menu_active' => 'started',
        ];

        $page_content = Blue::where([['title', '=','Cooperative Bye-Laws'], ['blue_category_id', '=', 2]])->first()->markdown;

        return view('bylaw', compact('page_content', 'metadata'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function help()
    {
        $metadata = [
            'title' => 'Help',
            'menu_active' => 'help',
        ];

        return view('help', compact('metadata'));
    }
}
