<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Blue;
use App\BlueCategory;
use App\Log;
use App\User;
use Mail;
use App\Mail\NewBluePost;

class BlueController extends Controller
{

    /**
     * Get Blue Pool Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata = [
            'title' => 'Volition Blue',
            'menu_active' => 'pool',
        ];

        if($request->user()->has_active_subscription || $request->user()->isAdmin) {
            if ($request->has('q')) {
                $search_term = $request->q;
                // Build search query
                if ($request->user()->isAdmin) {
                    $blues = Blue::where('title', 'LIKE', $search_term)
                        ->orWhere('markdown', 'LIKE', $search_term)
                        ->paginate($this->page_limit);
                } else {
                    $blues = Blue::where('title', 'LIKE', $search_term)
                        ->orWhere('markdown', 'LIKE', $search_term)
                        ->orWhere('blue_category_id', '!=', 2)
                        ->paginate($this->page_limit);
                }

            } else {
                //get all blues
                if ($request->user()->isAdmin) {
                    $blues = Blue::orderBy('created_at', 'desc')->paginate($this->page_limit);
                } else {
                    $blues = Blue::where('blue_category_id', '!=', 2)->orderBy('created_at', 'desc')->paginate($this->page_limit);
                }
            }
        } else {
            return redirect()->route('users.home')->with('warning', 'You need to have an active subscription in a pool to access Volition Blue');
        }

        return view('pools.blue.index', compact('blues', 'metadata'));
    }

    /**
     * Get Blue Pool Create Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $metadata = [
            'title' => 'Volition Blue',
            'menu_active' => 'pool',
        ];

        $category = BlueCategory::pluck('title', 'id');
        return view('pools.blue.create', compact('category', 'metadata'));
    }

    /**
     * Store Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->validation($request->blue);

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        $data = array_merge(['user_id' => auth()->user()->id], $request->except(['_token']));

        //Store new blue pool information
        Blue::unguard();
        $blue = Blue::create($data);
        Blue::reguard();

        //Log action/event
        Log::create(['user_id' => $request->user()->id, 'log_desc' => 'posted content titled ' . $blue->title]);

        if($blue->blue_category_id != 2) {
            //Send Email Notification of Volition Blue to all subscribed members
            $user = User::all();

            foreach($user as $user) {
                if (!$user->isAdmin) {
                    $mail_data = [
                        'title' => 'New Volition Blue Opportunity',
                        'body' => '<h3>' . $blue->title . '</h3>',
                        'action' => 'Explore opportunity',
                    ];
                    Mail::to($user)->queue(new NewBluePost($mail_data));
                }
            }
            return redirect()->route('blue.index');
        } else {
            return redirect()->route('blue.index');
        }
    }

    /**
     * Show Blue Pool Information.
     *
     * @return View
     */
    public function show(Request $request)
    {
        $metadata = [
            'title' => 'Volition Blue',
            'menu_active' => 'pool',
        ];

        $blue_id = $request->blue;

        $blue = Blue::findOrFail($blue_id);

        return view('pools.blue.show', compact('blue', 'metadata'));
    }

    /**
     * Edit Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $metadata = [
            'title' => 'Volition Blue',
            'menu_active' => 'pool',
        ];

        $blue_id = $request->blue;

        $blue = Blue::findOrFail($blue_id);
        $category = BlueCategory::pluck('title', 'id');

        return view('pools.blue.edit', compact('blue', 'category', 'metadata'));
    }

    /**
     * Update Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $blue_id = $request->blue;

        $blue = Blue::findOrFail($blue_id);

        $validation = $this->validation($blue_id);

        $validator = Validator::make($request->all(), $validation);

        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        $data = $request->except(['_token', '_method']);

        Blue::unguard();
        $blue->update($data);
        Blue::reguard();

        //Log action/event
        Log::create(['user_id' => $request->user()->id, 'log_desc' => 'updated content titled ' . $blue->title]);

        return back()->with('success', 'Post updated successfully');;
    }

    /**
     * Delete Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $blue_id = $request->blue;

        Blue::findOrFail($blue_id)->delete();

        //Log action/event
        Log::create(['user_id' => $request->user()->id, 'log_desc' => 'deleted post with id ' . $blue_id]);

        return redirect()->route('blue.index');
    }

    /**
     * Get Blue Pool Information Rules.
     *
     * @return array
     */
    protected function validation($blue_id)
    {
        return [
            'title' => 'bail|required|unique:blues,title,'.$blue_id.'|max:150',
        ];
    }

}
