<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }


    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials  = array('email' => $request->email, 'password' => $request->password);

        if (Auth::attempt($credentials, $request->has('remember'))){

            //set user first time to false.
            $user = User::where('email', $request->email)->first();
            $user->first_login = 0;
            $user->save();

            return redirect()->intended($this->redirectTo);
        }

        $checkArchive = User::onlyTrashed()->where('email', $request->email)->first();
        if($checkArchive){
            Session::flash('error','Your account is inactive.<br>Please <a href="http://volitioncap.com/reactivate/'.$checkArchive->id.'">click here to reactivate your account</a> and to subscribe to an investment pool.');

            return redirect('/login');
        }else{
            Session::flash('error','Invalid login credentials');

            return redirect('/login');
        }

    }
}
