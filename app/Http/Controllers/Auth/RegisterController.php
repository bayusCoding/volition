<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

use App\User;
use App\Token;
use App\Referral;
use Carbon\Carbon;
use App\Wallet;
use Mail;
use App\Log;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Change registration page
     *
     * @return 
     */
    public function showRegistrationForm(Request $request)
    {
        if($request->has('token') && $request->token != null) {
            $token = $request->token;
            if ($this->tokenValidation($token)) {
                return view('auth.referral', compact('token'));
            } else {
                $request->session()->flash('error', 'This referral has expired. Ask your referrer to send you a new invite or request from admin below');
                return view('auth.request');
            }
        } else {
            return view('auth.request');
        }
    }

    /**
     * Page for users without a referral id to request from Admin.
     *
     * @return 
     */
    public function request()
    {
        return view('auth.request');
    }

    /**
     * Full registration page once referral check has been completed.
     *
     * @return 
     */
    public function registerAccount(Request $request)
    {
        $referral_code = $request->referral_code;
        return view('auth.register', compact('referral_code'));
    }

    public function referralVerification(Request $request)
    {
        //check if referral code is correct
        if ($this->referralChecker($request->referral_code)) {
            return redirect(route('register.account', ['referral_code' => $request->referral_code]));
        } else {
            return back()->with('error', 'This referral code does not exist. Please try again or you can request for a referral <a href="/register">here</a>');
        }
    }

    /**
     * Validate the referral ID of the registered user
     */
    public function referralChecker($code)
    {
        $referral = Referral::where('referral_code', $code)->first();

        if (is_null($referral)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Validate the referral ID of the registered user
     */
    public function tokenValidation($token)
    {
        $token = Token::where('token', $token)->first();

        if (is_null($token)) {
            return false;
        } else {
            $date = Carbon::now();
            if ($token->expiry < $date) {
                return false;
            } else {
                return true;
            }
        }
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation());
        
        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        if ($request->has('referral_code')) {
            $referral = Referral::where('referral_code', $request->referral_code)->first();

            //check if user is registering with email the referral was sent to.
            if (strtolower($referral->email) != strtolower($request->email)) {
                return back()->withInput()->with('error', 'You can only register with the email this referral code was sent to');
            }

            $is_referred = 1;
            $referral_id = $referral->id;
        } else {
            $is_referred = 0;
            $referral_id = null;
        }

        //create the new user
        $user = User::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'isAdmin' => 0,
            'isReferred' => $is_referred,
            'referral_id' => $referral_id,
        ]);

        //create user wallet data
        Wallet::unguard();
        Wallet::create([
            'user_id' => $user['id'],
            'balance' => 0,
        ]);
        Wallet::reguard();

        //Log action/event
        Log::create(['user_id' => $user['id'], 'log_desc' => 'completed registration']);

        //Send Email Notification
        $mail_data = [
            'title' => $request->firstname . ' ' . $request->lastname . ' registered on Volition', 
            'body' => $request->firstname . ' ' . $request->lastname . ' with email address ' . $request->email . ' completed their registration on Volition. <br> <br> <br> <a href="' . route("users.show", $user['id']) . '" style="padding: 14px 50px;   border: 1px solid #A0830B;  border-radius: 3px; background-color: #A0830B; color: #fff;  box-shadow: 0 3px 12px 2px rgba(160,131,11,0.3);font-size: 18px;   font-weight: 500; font-family: Helvetica;   line-height: 22px;text-decoration: none">View user</a><br><br>'
        ];
        Mail::send('emails.general', $mail_data, function($message) {
            $message->to(['members@volitioncap.com'])
            ->subject('New Member Registration');
        });

        return redirect()->route('users.help');
    }

    /**
     * Get validation rule.
     *
     * @return array
     */
    protected function validation()
    {
        return [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

}
