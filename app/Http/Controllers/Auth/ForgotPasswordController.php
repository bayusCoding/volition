<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Log;
use Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $validator = Validator::make($request->all(), ['email' => 'required|email']);
        
        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        $user = User::where('email', $request->email)->first();

        if (is_null($user)) {
            return back()->withInput()->with('error', 'The email does not exist');
        }

        // Generate a new reset password token
        $token = app('auth.password.broker')->createToken($user);

        //Log action/event
        Log::create(['user_id' => $user->id, 'log_desc' => 'requested a password reset']);

        $mail_data = [
            'name' => $user->firstname . ' ' . $user->lastname,
            'token' => $token 
        ];
        Mail::send('emails.password_reset', $mail_data, function($message) use($user) {
            $message->to([$user->email])
            ->subject('Password Reset on Volition');
        });

        return back()->with('success', 'Password reset link has been sent to your email');
    }
}
