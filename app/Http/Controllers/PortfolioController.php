<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Pool;
use App\Portfolio;

class PortfolioController extends Controller
{

    /**
     * Get Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $metadata = [
            'title' => 'Portfolio',
            'menu_active' => 'portfolio',
        ];
        $pools = Pool::pluck('name', 'id');

        $portfolios = Portfolio::orderBy('created_at', 'desc')->get();

        return view('portfolio.index', compact('portfolios', 'pools', 'metadata'));
    }

    /**
     * Get Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $metadata = [
            'title' => 'Portfolio',
            'menu_active' => 'portfolio',
        ];
        $pools = Pool::pluck('name', 'id');

        $portfolio = Portfolio::find($request->portfolio);

        return view('portfolio.edit', compact('portfolio', 'pools', 'metadata'));
    }


    /**
     * Update.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation());

        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        $portfolio = Portfolio::find($request->portfolio);

        $portfolio->month = $request->month;
        $portfolio->pool_id = $request->pool_id;
        $portfolio->change = $request->change;

        $portfolio->save();

        return redirect()->route('portfolio.index');
    }

    /**
     * Store.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation());

        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        //check if value has been stored before for the pool particular month and year
        $check = Portfolio::where([['month', '=', $request->month], ['year', '=', date('Y')], ['pool_id', '=', $request->pool_id]])->first();

        if(is_null($check)) {
            $request->request->add([
                'year' => date("Y"),
            ]);

            $data = $request->except(['_token']);

            Portfolio::create($data);

            return back()->with('success', 'Pool values successfully updated');
        }

        return back()->withInput()->with('error', 'You have already register values for this pool for set month');
    }

    /**
     * Get a validation rule for an incoming request.
     *
     * @return array
     */
    protected function validation()
    {
        return [
            'month' => 'required'
        ];
    }
}
