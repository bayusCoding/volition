<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ReferralController as Referral;
use App\User;
use App\Referral as Referrals;
use App\Wallet;
use App\Message;
use App\Transaction;
use App\Log;
use App\Subscription;
use App\Pool;
use Auth;
use Mail;
use Illuminate\Support\Facades\Session;

use Illuminate\Database\Eloquent\Collection;
use League\Csv\Writer;
use Schema;
use SplTempFileObject;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata = [
            'title' => 'Users',
            'menu_active' => 'users',
        ]; 

        if ($request->has('q')) {
            $search_term = $request->q;
            // Build the user search query
            $users = User::where('email', 'LIKE', $search_term)
                ->orWhere('firstname', 'LIKE', $search_term)
                ->orWhere('lastname', 'LIKE', $search_term)
                ->orWhere('phone', 'LIKE', $search_term)
                ->get();
        } else {
            //get all the users
            $users= User::orderBy('id', 'desc')->get();
        }

        return view('users.index', compact('users', 'metadata'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation());
        
        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        if ($request->has('referral_code')) {
            $referral = Referral::where('referral_code', $request->referral_code)->first();

            $is_referred = 1;
            $referral_id = $referral->id;
        } else {
            $is_referred = 0;
            $referral_id = null;
        }

        //create the new user
        $user = User::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($request->password),
            'isAdmin' => 0,
            'isReferred' => $is_referred,
            'referral_id' => $referral_id,
        ]);

        //create user wallet data
        Wallet::unguard();
        Wallet::create([
            'user_id' => $user['id'],
            'balance' => 0,
        ]);
        Wallet::reguard();

        return redirect()->route('users.home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //get count of commpleted investements
        $green_count = Subscription::where([['user_id', '=', $request->user], ['pool_id', '=', 1]])->count();
        $black_count = Subscription::where([['user_id', '=', $request->user], ['pool_id', '=', 2]])->count();
        $red_count = Subscription::where([['user_id', '=', $request->user], ['pool_id', '=', 3]])->count();
        $business_count = Subscription::where([['user_id', '=', $request->user], ['pool_id', '=', 6]])->count();
        $privilege_count = Subscription::where([['user_id', '=', $request->user], ['pool_id', '=', 5]])->count();
        $referral_count = Referrals::where('user_id', '=', $request->user)->count();

        $metadata = [
            'title' => 'Users',
            'menu_active' => 'users',
            'green_count' => $green_count,
            'red_count' => $red_count,
            'black_count' => $black_count,
            'business_count' => $business_count,
            'privilege_count' => $privilege_count,
            'referral_count' => $referral_count,
        ];

        $user = User::findOrFail($request->user);


        if($user->isReferred){

            //get the user referred
            $_ref = Referrals::findOrFail($user->referral_id);

            $ref = User::where('id', $_ref->user_id)->first();
            $ref_full_name = $ref->firstname. ' '.$ref->lastname;
            $ref_user_id = $_ref->user_id;
        }else{
            $ref_full_name = 'n/a';
            $ref_user_id = 0;
        }


        $referrals = Referrals::where('user_id', '=', $request->user)->orderBy('id', 'desc')->limit(5)->get();
        $wallet = Wallet::where('user_id', '=', $request->user)->firstOrFail();
        $messages = Message::where('user_id', '=', $request->user)->orWhere('message_to', '=', $request->user)->limit(5)->get();
        $transactions = Transaction::where('user_id', '=', $request->user)->orderBy('id', 'desc')->limit(5)->get();
        $logs = Log::where('user_id', '=', $request->user)->orderBy('id', 'desc')->limit(5)->get();

        return view('users.user', compact('user', 'ref_full_name', 'ref_user_id', 'referrals', 'wallet', 'messages', 'transactions', 'logs', 'metadata'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function transactions(Request $request)
    {
        $metadata = [
            'title' => 'Users',
            'menu_active' => 'users',
        ];

        $user = User::findOrFail($request->user);

        $transactions = Transaction::where('user_id', '=', $request->user)->orderBy('id', 'desc')->paginate(15);

        return view('users.transactions', compact('user', 'transactions', 'metadata'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function referrals(Request $request)
    {
        $metadata = [
            'title' => 'Users',
            'menu_active' => 'users',
        ];

        $user = User::findOrFail($request->user);
        
        $referrals = Referrals::where('user_id', '=', $request->user)->orderBy('id', 'desc')->get();
        $wallet = Wallet::where('user_id', '=', $request->user)->firstOrFail();

        return view('users.referrals', compact('user', 'referrals', 'wallet', 'metadata'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function pools(Request $request)
    {
        $metadata = [
            'title' => 'Users',
            'menu_active' => 'users',
        ];

        $user = User::findOrFail($request->user);
        
        $subscriptions = Subscription::where('user_id', '=', $request->user)->orderBy('id', 'desc')->paginate(15);

        return view('users.pools', compact('user', 'subscriptions', 'metadata'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function poolCreate(Request $request)
    {
        $metadata = [
            'title' => 'Users',
            'menu_active' => 'users',
        ];
        $green_plus = Pool::where('slug', 'green_plus')->first();
        $red_plus = Pool::where('slug', 'red_plus')->first();
        $black_plus = Pool::where('slug', 'black_plus')->first();
        
        $user = User::findOrFail($request->user);
        $pools = Pool::pluck('name', 'id');
        return view('users.pool_create', compact('user', 'pools', 'metadata', 'green_plus', 'red_plus', 'black_plus'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function poolEdit(Request $request)
    {
        $metadata = [
            'title' => 'Users',
            'menu_active' => 'users',
        ];
        $green_plus = Pool::where('slug', 'green_plus')->first();
        $red_plus = Pool::where('slug', 'red_plus')->first();
        $black_plus = Pool::where('slug', 'black_plus')->first();

        $pools = Pool::pluck('name', 'id');

        $subscription = Subscription::findOrFail($request->pool);

        $user = User::findOrFail($subscription->user_id);

        return view('users.pool_edit', compact('subscription', 'user', 'pools', 'metadata', 'green_plus', 'red_plus', 'black_plus'));
    }

    /**
     * Update user subscription.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function poolUpdate(Request $request)
    {
        $subscription = Subscription::find($request->pool);
        $selected = Pool::findOrFail($request->pool_id);

        if($subscription->pool_id == 5 || $subscription->pool_id == 6){

            if($request->green_plus == '' || $request->green_plus == 0){
                $green_amount = 0;
            }else{
                $green_amount = $request->green_plus;
            }

            if($request->red_plus == '' || $request->red_plus == 0){
                $red_amount = 0;
            }else{
                $red_amount = $request->red_plus;
            }

            if($request->black_plus == '' || $request->black_plus == 0){
                $black_amount = 0;
            }else{
                $black_amount = $request->black_plus;
            }

            $green_total = $green_amount * 1;
            $red_total = $red_amount * 1;
            $black_total = $black_amount * 1;
            $subscription_fee = $green_total + $red_total + $black_total;

            $child_slot = $green_amount.'+'.$red_amount.'+'.$black_amount;
            $child_slot_duration = $request->green_month.'+'.$request->red_month.'+'.$request->black_month;
            $request->request->add([
                'slots' => 1,
            ]);


        }else{
            $subscription_fee = $request->slots * $selected->upgrade;

            $child_slot = 0;
            $child_slot_duration = 0;
        }

        $subscription->subscription_number = 'OFF'.$subscription->subscription_number;
        $subscription->pool_id = $request->pool_id;
        $subscription->slots = $request->slots;
        $subscription->subscription_fee = $subscription_fee;
        $subscription->child_pool_slot = $child_slot;
        $subscription->child_pool_slot_duration = $child_slot_duration;
        $subscription->start_date = $request->start_date;
        $subscription->end_date = $request->end_date;

        $subscription->save();

        $user = User::findOrFail($request->user);

        //Log action/event
        Log::create(['user_id' => Auth::user()->id, 'log_desc' => 'changed investment of ' . $user->firstname . ' ' . $user->lastname . ' with subscription number ' . $subscription->subscription_number]);

        return redirect()->route('users.pools', $request->user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function poolDestroy(Request $request)
    {
        $user = User::findOrFail($request->user);
        $pools = Pool::pluck('name', 'id');

        $subscription = Subscription::find($request->pool);

        $subscription_number = $subscription->subscription_number;

        $subscription->delete();

        //create a transaction reversal for deleted pools
        $transaction = Transaction::where('trans_ref', $subscription_number)->first();

        Transaction::create([
            'user_id' => $request->user,
            'subscription_id' => $transaction->subscription_id,
            'amount' => $transaction->amount,
            'trans_ref' => $subscription_number,
            'trans_desc' => $transaction->trans_desc . ' transaction reversal',
            'trans_type' => 1
        ]);

        //Log action/event
        Log::create(['user_id' => Auth::user()->id, 'log_desc' => 'deleted investment of ' . $user->firstname . ' ' . $user->lastname . ' with subscription number ' . $subscription_number]);

        return redirect()->route('users.pools', $request->user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function poolStore(Request $request)
    {
        $user = User::findOrFail($request->user);

        $selected = Pool::findOrFail($request->pool_id);
        $subscription_number = 'OFF' . uniqid();

        if(is_null($request->admin_fees_inc)){
            $admin_fees_inc = 0;
        }else{
            $admin_fees_inc = 1;
        }

        if($request->green_plus == ''){
            $green_amount = 0;
        }else{
            $green_amount = $request->green_plus;
        }

        if($request->red_plus == ''){
            $red_amount = 0;
        }else{
            $red_amount = $request->red_plus;
        }

        if($request->black_plus == ''){
            $black_amount = 0;
        }else{
            $black_amount = $request->black_plus;
        }

        //sub is for business or privilege
        if($selected->id == 5 || $selected->id == 6) {
            $green_total = $green_amount * 1;
            $red_total = $red_amount * 1;
            $black_total = $black_amount * 1;
            $subscription_fee = $green_total + $red_total + $black_total;

            $child = 1;
            $child_slot = $green_amount.'+'.$red_amount.'+'.$black_amount;
            $child_slot_duration = $request->green_month.'+'.$request->red_month.'+'.$request->black_month;
            $request->request->add([
                'slots' => 1,
            ]);

        }else{
            $subscription_fee = $request->slots * $selected->upgrade;

            $child = 0;
            $child_slot = 0;
            $child_slot_duration = 0;

        }


        $request->request->add([
            'user_id' => $request->user,
            'subscription_fee' => $subscription_fee,
            'admin_fees_inc' => $admin_fees_inc,
            'subscription_number' => $subscription_number,
            'has_paid' => 1,
            'child_pool' => $child,
            'child_pool_slot' => $child_slot,
            'child_pool_slot_duration' => $child_slot_duration
        ]);

        $data = $request->except(['_token', 'user', 'upgrade', 'green_plus', 'red_plus', 'black_plus', 'green_month', 'red_month', 'black_month']);
        Subscription::unguard();
        $subscription = Subscription::create($data);
        Subscription::reguard();

        $transaction_data = [
            'user_id' => $request->user,
            'subscription_id' => $subscription->id,
            'amount' => $subscription_fee,
            'trans_ref' => $subscription_number,
            'trans_desc' => $selected->name . ' Pool subscription',
            'trans_type' => 0
        ];
        Transaction::unguard();
        Transaction::create($transaction_data);
        Transaction::reguard();

        //Log action/event
        Log::create(['user_id' => Auth::user()->id, 'log_desc' => 'created an investment of ' . $subscription_fee .' in to the ' . $selected->name . ' pool for ' . $user->firstname . ' ' . $user->lastname]);

        return redirect()->route('users.show', $request->user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logs(Request $request)
    {
        $metadata = [
            'title' => 'Users',
            'menu_active' => 'users',
        ];

        $user = User::findOrFail($request->user);
        
        $logs = Log::where('user_id', '=', $request->user)->orderBy('created_at', 'desc')->paginate(15);

        return view('users.logs', compact('user', 'logs', 'metadata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return $users = User::findOrFail($request->user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = User::findOrFail($request->user);

        $user->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = User::findOrFail($request->user);

        $user->delete();

        return redirect()->route('users.index');
    }

    private function createCsv(Collection $modelCollection, $tableName){

        $csv = Writer::createFromFileObject(new SplTempFileObject());
    
        // This creates header columns in the CSV file - probably not needed in some cases.
        $csv->insertOne(Schema::getColumnListing($tableName));
    
        foreach ($modelCollection as $data){
            $csv->insertOne($data->toArray());
        }
    
        $csv->output($tableName . '.csv');
    
    }

    public function getMainUserData(){
        $user = User::all();

        // Note: $mainMeta is a Collection object 
        //(returning a 'collection' of data from using 'all()' function), 
        //so can be passed in below.
        $this->createCsv($user, 'user');
    }


    /**
     * Display archived users
     */
    public function trashed()
    {
        $metadata = [
            'title' => 'Archived Users',
            'menu_active' => 'archived',
        ];

        //get all the archived users
        $users = User::onlyTrashed()->orderBy('id', 'desc')->get();

        return view('users.trashed', compact('users', 'metadata'));

    }


    /**
     * Untrash a user
     */
    public function untrash(Request $request)
    {
        User::withTrashed()
            ->where('id', $request->id)
            ->restore();

        $user = User::findOrFail($request->id);

        //send an email, that account has been unarchived.
        Mail::send('emails.unarchive', ['user' => $user], function ($mail) use ($user) {
            $mail->to($user->email)
                ->subject('Your Account Is Now Active');
        });

        return redirect()->route('users.archived');

    }


    /**
     * User asked to be reactivated
     */

    public function reactivate(Request $request)
    {
        $user = User::withTrashed()->where('id', $request->id)->first();

        //send an email to the admin, asking to be reactivated
        Mail::send('emails.reactivate', ['user' => $user], function ($mail) use ($user) {
            $mail->to('members@volitioncap.com')
                ->subject('User asked to be re-activated');
        });

        Session::flash('success','Your account re-activation request has been sent to Admin. You will be notified once approved.');

        return redirect('/login');

    }
}