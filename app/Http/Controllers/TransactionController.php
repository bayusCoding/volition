<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Transaction;
use App\User;
use App\Subscription;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Collection;
use League\Csv\Writer;
use Schema;
use SplTempFileObject;

class TransactionController extends Controller
{
    
    /**
     * Get Blue Pool Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata = [
            'title' => 'Transaction History',
            'menu_active' => 'transactions',
        ];

        if (Auth::user()->isAdmin) {
            //Get all transactions
            $transactions = Transaction::orderBy('id', 'desc')->get();
        } else {
            //Get all user's transactions
            $transactions = Transaction::where('user_id', '=',Auth::user()->id)->orderBy('id', 'desc')->get();
        }

        return view('transactions.index', compact('transactions', 'metadata'));
    }


    /**
     * Store Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $metadata = [
            'title' => 'Transaction History',
            'menu_active' => 'transactions',
        ];
        $subscriptions = Subscription::select('id', DB::raw("concat(subscription_number, ' - ',subscription_fee) as merger"))
            //->with('user')
            //->select('sub_id', DB::raw("concat(merge, ' - ', fullname) as merger"))
            ->pluck('merger', 'sub_id');
        $users = User::all()->pluck('fullname', 'id');
        return view('transactions.create', compact('users', 'subscriptions', 'metadata'));
    }

    /**
     * Store Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $metadata = [
            'title' => 'Transaction History',
            'menu_active' => 'transactions',
        ];
        $trans_id = $request->transaction;
        $transaction = Transaction::findOrFail($trans_id);
        return view('transactions.edit', compact('transaction', 'metadata'));
    }

    /**
     * Store Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $trans_id = $request->transaction;
        $transaction = Transaction::findOrFail($trans_id);
        $transaction->trans_desc = $request->trans_desc;
        $transaction->amount = $request->amount;
        if($transaction->save()){
            return redirect()->back()->with('success', 'Transaction successfully updated');
        }
        return redirect()->back()->with('error', 'Unable to update transaction kindly contact administrator if error persist');
    }

    /**
     * Store Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reference = 'TRAN' . uniqid();
        $user = User::findOrFail($request->user);

        Transaction::unguard();
        Transaction::create([
            'user_id' => $request->user,
            'subscription_id' => 0,
            'amount' => $request->amount,
            'trans_ref' => $reference,
            'trans_desc' => $request->description,
            'trans_type' => $request->type
        ]);
        Transaction::reguard();

        //Log action/event
        Log::create(['user_id' => Auth::user()->id, 'log_desc' => 'created transaction for ' . $user->firstname . ' ' . $user->lastname]);

        return redirect()->back()->with('error', 'Unable to update transaction kindly contact administrator if error persist');

    }

    /**
     * Store Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $trans_id = $request->transaction;
        $transaction = Transaction::findOrFail($trans_id);
        $transaction->delete();
        return redirect()->route('transactions.index')->with('success', 'Transaction successfully deleted');
    }
    
    private function createCsv(Collection $modelCollection, $tableName){

        $csv = Writer::createFromFileObject(new SplTempFileObject());
    
        // This creates header columns in the CSV file - probably not needed in some cases.
        $csv->insertOne(Schema::getColumnListing($tableName));
    
        foreach ($modelCollection as $data){
            $csv->insertOne($data->toArray());
        }
    
        $csv->output($tableName . '.csv');
    
    }

    public function getMainTransactionData(){
        $user = Transaction::all();

        // Note: $mainMeta is a Collection object 
        //(returning a 'collection' of data from using 'all()' function), 
        //so can be passed in below.
        $this->createCsv($user, 'transaction');
    }
}
