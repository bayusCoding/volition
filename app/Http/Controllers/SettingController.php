<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Setting;
use App\Pool;
use App\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        //
    }
    
    /**
     * Get Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata = [
            'title' => 'Settings',
            'menu_active' => 'settings',
        ];

        $pools = Pool::pluck('name', 'id');
        $settings = Setting::all();

        return view('settings.index', compact('settings', 'pools', 'metadata'));
    }

    /**
     * Store Blue Pool Information.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    
    /**
     * Show .
     *
     * @return View
     */
    public function show(Request $request)
    {
        //
    }
    
    /**
     * Edit
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
    }
    
    /**
     * Update Settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $input = $request->all();

        foreach ($input as $key => $value) {
            $setting = Setting::where('key', '=', $key)->first();
            if (!is_null($setting)) {
                $setting->value = $value;
                $setting->save();
            }
        }
        
        return back()->with('success', 'General settings updated successfully');
    }

    /**
     * Update pool admin fee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request)
    {
        $validation = [
            'firstname' => 'required',
            'lastname' => 'required',
        ];

        $validator = Validator::make($request->all(), $validation);
        
        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        $user = User::findOrFail($request->user()->id);
        
        $data = $request->except(['_token']);
        
        $user->update($data);

        return back();
    }

    /**
     * Update user password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        $user = User::findOrFail($request->user()->id);

        if (Hash::check($request->old_password, $user->password)) {
            $validation = [
            'old_password' => 'required|string|different:password',
            'password' => 'required|string|min:6|confirmed',
            ];

            $validator = Validator::make($request->all(), $validation);
            
            if ($validator->fails()){
                return redirect(route('settings.index'))->withErrors($validator);
            }

            $data = ['password' => Hash::make($request->password)];
            
            $user->update($data);

            Auth::logout();

            Session::flash('success','Password changed! Please login again');

            return redirect('/login');
        } else {
            return redirect(route('settings.index'))->with('error','Password is incorrect');
        }
    }

    /**
     * Update pool admin fee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getPool(Request $request)
    {
        $pool = Pool::findOrFail($request->pool);
        
        if ($request->ajax()) {
            return $pool->toJson();
        }  

        return $pool; 
    }

    /**
     * Update pool admin fee.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePool(Request $request)
    {
        $pool = Pool::findOrFail($request->pool);
        
        $data = $request->except(['_token', 'pool']);

        $pool->update($data);

        return back()->with('success', $pool->name .' has been successfully updated');
    }

}
