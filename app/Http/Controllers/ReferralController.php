<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Referral;
use App\Http\Controllers\TokenController as Token;
use Mail;
use App\Wallet;
use App\Log;
use App\Noreferral;

class ReferralController extends Controller
{
    
    /**
     * Get Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $metadata = [
            'title' => 'Referrals',
            'menu_active' => 'referrals',
        ];

        if($request->user()->isAdmin) {
            $noreferrals = Noreferral::limit(5)->orderBy('id', 'desc')->get();
            $referrals = Referral::limit(5)->orderBy('id', 'desc')->get();

            $referrals_count = Referral::all()->count();
            $noreferrals_count = Noreferral::all()->count();

            return view('referrals.index', compact('noreferrals','referrals', 'referrals_count', 'noreferrals_count', 'metadata'));
        } else {
            $referrals = Referral::where('user_id', '=', $request->user()->id)->orderBy('id', 'desc')->paginate($this->page_limit);
            $referrals_count = Referral::where('user_id', '=', $request->user()->id)->count();
            $wallet = Wallet::where('user_id', '=', $request->user()->id)->firstOrFail();
            return view('referrals.index', compact('referrals', 'referrals_count', 'wallet', 'metadata'));
        }
        
    }

    /**
     * Get Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function history(Request $request)
    {
        $metadata = [
            'title' => 'Referrals',
            'menu_active' => 'referrals',
        ];

        $referrals = Referral::orderBy('id', 'desc')->paginate($this->page_limit);
        return view('referrals.history', compact('referrals', 'metadata'));
    }

    /**
     * Get Index.
     *
     * @return \Illuminate\Http\Response
     */
    public function noreferral(Request $request)
    {
        $metadata = [
            'title' => 'Referrals',
            'menu_active' => 'referrals',
        ];

        $noreferrals = Noreferral::orderBy('id', 'desc')->paginate($this->page_limit);
        return view('referrals.request', compact('noreferrals', 'metadata'));
    }

    /**
     * Send referral invite.
     *
     * @return \Illuminate\Http\Response
     */
    public function invite(Request $request)
    {
        $emails = explode(',', $request->email);

        $validation = [
            //check if invite is registered user
        ];
        $messages = [
            'unique'    => ':input is already registered on Volition.',
        ];
        $limit = count($emails) - 1;
        foreach(range(0, $limit) as $index) {
            $validation[$index] = 'bail|required|email|unique:users,email|max:255';
        }
        $validator = Validator::make($emails, $validation, $messages);
        
        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        $status = 'You have successfully sent an invite to';

        foreach($emails as $email) {
            $token = $this->generateToken();
            $code = $this->generateReferralCode();

            $data = [
                'user_id' => $request->user()->id,
                'referral_code' => $code,
                'email' => $email,
            ];
            
            //Store referal
            $referral = Referral::create($data);
            Token::store($referral->id, $token);

            $mail_data = ['token' => $token, 'code' => $code, 'referrer_email' => $request->user()->email,'referrer_name' => $request->user()->firstname . ' ' . $request->user()->lastname];

            //Send Email 
            Mail::send('emails.referral', $mail_data, function($message) use ($request, $email) {
                $message->to($email)
                ->subject('You are invited by ' . $request->user()->firstname . ' ' . $request->user()->lastname . ' to join Volition');
            });

            //Log action/event
            Log::create(['user_id' => $request->user()->id, 'log_desc' => 'sent a referral to '. $email]);

            $status = $status . ' ' . $email;
        }   
        return back()->with('success', $status);
    }

    /**
     * Send referral invite.
     *
     * @return \Illuminate\Http\Response
     */
    public function noreferralInvite(Request $request)
    {
        $emails = explode(',', $request->email);

        $validation = [
            //check if invite is registered user
        ];
        $messages = [
            'unique'    => ':input is already registered on Volition.',
        ];
        $limit = count($emails) - 1;
        foreach(range(0, $limit) as $index) {
            $validation[$index] = 'bail|required|email|unique:users,email|max:255';
        }
        $validator = Validator::make($emails, $validation, $messages);
        
        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        $status = 'You have successfully sent an invite to';

        foreach($emails as $email) {
            $token = $this->generateToken();
            $code = $this->generateReferralCode();

            $data = [
                'user_id' => $request->user()->id,
                'referral_code' => $code,
                'email' => $email,
            ];
            
            //Store referal
            $referral = Referral::create($data);
            Token::store($referral->id, $token);

            $mail_data = ['token' => $token, 'code' => $code, 'referrer_email' => $request->user()->email,'referrer_name' => $request->user()->firstname . ' ' . $request->user()->lastname];

            //Send Email 
            Mail::send('emails.referral', $mail_data, function($message) use ($request, $email) {
                $message->to($email)
                ->subject('You are invited by ' . $request->user()->firstname . ' ' . $request->user()->lastname . ' to join Volition');
            });

            //Log action/event
            Log::create(['user_id' => $request->user()->id, 'log_desc' => 'sent a referral to '. $email]);

            //check if email is valid noreferral request made and link referral sent out to it
            $noreferral = Noreferral::where('email', $email)->first();
            if(!is_null($noreferral)){
                $noreferral->referral_id = $referral->id;
                $noreferral->save();
            }

            $status = $status . ' ' . $email;
        }   
        return back()->with('success', $status);
    }

    /**
     * Store user requesting for a referral.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validation());
        
        if ($validator->fails()) return back()->withInput()->withErrors($validator);

        $data = $request->except(['_token']);

        //Store referal
        $noreferral = Noreferral::create($data);

        if($noreferral) {
            //Log action/event
            Log::create(['user_id' => 1, 'log_desc' => $request->firstname . ' ' . $request->lastname . ' with Email '. $request->email . ' requested for a referral code']);

            //Send Email Notification
            $mail_data = [
                'title' => $request->firstname . ' ' . $request->lastname . ' requested for a referral code on Volition', 
                'body' => $request->firstname . ' ' . $request->lastname . ' with email address ' . $request->email . ' requested for a referral code <br> <br> <br> <a href="' . route("referrals.index") . '" style="padding: 14px 50px;   border: 1px solid #A0830B;  border-radius: 3px; background-color: #A0830B; color: #fff;  box-shadow: 0 3px 12px 2px rgba(160,131,11,0.3);font-size: 18px;   font-weight: 500; font-family: Helvetica;   line-height: 22px;text-decoration: none">Send referral</a><br><br>'
            ];
            Mail::send('emails.general', $mail_data, function($message) {
                $message->to(['members@volitioncap.com'])
                ->subject('New Referral Code Request');
            });

            return back()->with('success', 'Thank you for requesting to join Volition Capital. We would be in touch with you for further verification');
        }
    }


    /**
     * Generate User Referring ID.
     *
     * @return \Illuminate\Http\Response
     */
    public static function generateReferralCode()
    {
        return uniqid();
    }

    /**
     * Generate token.
     *
     * @return \Illuminate\Http\Response
     */
    public static function generateToken()
    {
        return uniqid(true);
    }


    /**
     * Get validation rule.
     *
     * @return array
     */
    protected function validation()
    {
        return [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ];
    }
}
