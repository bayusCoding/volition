<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rave;

class PaymentController extends Controller
{
    /**
     * Redirect User to Paystack Payment Gateway.
     *
     * @return \Illuminate\Http\Response
     */
    public static function paystackPay()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/initialize",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode([
                'amount' => Request('amount') * 100,
                'email' => Request('email'),
                'metadata' => Request('metadata')
            ]),
            CURLOPT_HTTPHEADER => [
                "authorization: Bearer ".env('PAYSTACK_SECRET_KEY')."",
                "content-type: application/json",
                "cache-control: no-cache"
            ],
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if($err){
            // there was an error contacting the Paystack API
            die('Curl returned error: ' . $err);
        }

        $tranx = json_decode($response);

        if(!$tranx->status){
            // there was an error from the API
            die('API returned error: ' . $tranx->message);
        }

        // store transaction reference so we can query in case user never comes back
        // perhaps due to network issue
        //save_last_transaction_reference($tranx->data->reference);

        // redirect to page so User can pay
        header('Location: ' . $tranx->data->authorization_url);
        exit;
    }

    /**
     * Process Paystack Payment Gateway Callback.
     *
     * @return \Illuminate\Http\Response
     */
    public static function paystackPayCallback()
    {
        $curl = curl_init();
        $reference = isset($_GET['reference']) ? $_GET['reference'] : '';
        if(!$reference){
            die('No reference supplied');
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/verify/" . rawurlencode($reference),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                "accept: application/json",
                "authorization: Bearer ".env('PAYSTACK_SECRET_KEY')."",
                "cache-control: no-cache"
            ],
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        if($err){
            // there was an error contacting the Paystack API
            die('Curl returned error: ' . $err);
        }

        $tranx = json_decode($response);

        if(!$tranx->status){
            // there was an error from the API
            die('API returned error: ' . $tranx->message);
        }

        if('success' == $tranx->data->status){

            return $tranx->data;
        }

    }

    /**
     * Process Ravepay payment.
     *
     * @return \Illuminate\Http\Response
     */
    public static function ravePay($ref, $callback_route) {
        Rave::setPrefix($ref, true)->initialize(route($callback_route));
    }

    /**
     * Process Ravepay Payment Gateway Callback.
     *
     * @return \Illuminate\Http\Response
     */
    public static function ravePayCallback() {
        if (request()->cancelled && request()->txref){
            $data = Rave::requeryTransaction(request()->txref)->paymentCanceled(request()->txref);
            return $data;
        } elseif(request()->txref){
            $data = Rave::requeryTransaction(request()->txref);
            return $data;
        } else {
            echo 'Stop!!! Please pass the txref parameter!';
        }
    }

}
