<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdminFees;
use Carbon\Carbon;

use App\Pool;
use App\Log;
use App\Http\Controllers\PaymentController as Pay;
use App\Transaction;

class AdminFeesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public static function store($user_id, $pool_id, $subscription_id)
    {
        $selected = Pool::findOrFail($pool_id);

        $data = [
            'user_id' => $user_id,
            'pool_id' => $pool_id,
            'subscription_id' => $subscription_id,
            'expiry' => Carbon::now()->addDays($selected->expiry),
        ];

        //create admin fee record for user
        $admin_fees = AdminFees::create($data);

        Log::create(['user_id' => $user_id, 'log_desc' => 'paid for ' . $selected->name . ' administration fee']);

        return $admin_fees;
    }

    /** Renew Admin Fee
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public static function renew(Request $request)
    {
        $pool = Pool::where('name', '=', $request->pool)->firstOrFail();
        $payment_ref = uniqid();

        $admin_fees = AdminFees::where([['user_id', '=', $request->user()->id], ['pool_id', '=', $pool->id]])->whereDate('expiry', '>', Carbon::now())->first();

        $adminfee_subscription_days_left = 0;

        if (!is_null($admin_fees)) {
             $adminfee_subscription_days_left = $admin_fees->expiry->diffInDays(Carbon::now(), false);
         }

         //check of posititve (this will indicate that payment is being made before and existing subscription as expreied) 
         //or negative (this will indicate that payment is made after an existing subscription has expired)
         if ($adminfee_subscription_days_left < 0) {
            $adminfee_subscription_days_left = 0;
         }

        $data = [
            'user_id' => $request->user()->id,
            'pool_id' => $pool->id,
            'payment_ref' => $payment_ref,
            'expiry' => Carbon::now()->addDays($pool->expiry + $adminfee_subscription_days_left),
        ];


        $request->request->add([
            //metadata for RavePay
            'firstname' => $request->user()->firstname,
            'lastname' => $request->user()->lastname,
            'email' => $request->user()->email,
            'phonenumber' => $request->user()->phone,
            'amount' => $pool->admin_fees,
            'country' => 'NG',
            'currency' => 'NGN'
        ]);

        //create admin fee record for user
        AdminFees::unguard();
        $admin_fees = AdminFees::create($data);
        AdminFees::reguard();

        Pay::ravePay($payment_ref, 'adminfees.renew.callback');
    }

    /** Renew Admin Fee
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public static function renewCallback(Request $request)
    {
        //process the callback from Ravepay
        $callback = Pay::ravePayCallback();

        if (!$request->cancelled) {
            $payment_ref = $callback->txref; 

            $admin_fees = AdminFees::withoutGlobalScopes()->where('payment_ref', $payment_ref)->firstOrFail(); 

            //update admin_fee to has paid
            $admin_fees->has_paid = 1;
            $admin_fees->save();
            $amount = $callback->amount; 

            $transaction_data = [
                'user_id' => $request->user->id,
                'amount' => $amount,
                'trans_ref' => $payment_ref,
                'trans_desc' => 'Admin Fee Renewal',
                'trans_type' => 0
            ];

            Transaction::unguard();
            Transaction::create($transaction_data);
            Transaction::reguard();

            Log::create(['user_id' => $request->user->id, 'log_desc' => 'paid for ' . $pool->name . ' administration fee']);

            return redirect(route('users.home'));
        } else {
            return redirect(route('users.home'));
        }
    }

}
