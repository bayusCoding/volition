<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BlueCategory;

use Validator;
use App\Log;

class BlueCategoryController extends Controller 
{
	
	/*
	 * Get Index.
	 *
	 * @return View
	 */
	public function index()
	{
		$metadata = [
            'title' => 'Blue Categories',
            'menu_active' => 'pool',
        ];

		$categories = BlueCategory::orderBy('created_at', 'asc')->paginate($this->page_limit);

		return view('pools.blue.categories.index', compact('categories', 'metadata'));
	}
	
	/*
	 * Create.
	 *
	 * @return View
	 */
	public function create()
	{
		$metadata = [
            'title' => 'New Category',
            'menu_active' => 'pool',
        ];

		$categories = BlueCategory::pluck('title', 'id');
		
		return view('pools.blue.categories.create', compact('categories', 'metadata'));
	}
	
	/*
	 * Get Index.
	 *
	 * @return View
	 */
	public function store(Request $request)
	{
		$validation = array (
            'title' => 'required|unique:blue_categories|max:150',
        );

		$validator = Validator::make($request->all(), $validation);
		
		if ($validator->fails()) return back()->withInput()->withErrors($validator);
		
		$data = $request->except(['_token']);
        
        //Store new category
        $category = BlueCategory::create($data);

        //Log action/event
        Log::create(['user_id' => $request->user()->id, 'log_desc' => 'created ' . $category->title . ' category']);
        
        return redirect()->route('categories.index');
	}
	
	/*
	 * Get Index.
	 *
	 * @return View
	 */
	public function show()
	{
		//
	}
	
	/*
	 * Get Index.
	 *
	 * @return View
	 */
	public function edit(Request $request)
	{
		$metadata = [
            'title' => 'Blue Categories',
            'menu_active' => 'pool',
        ];

		$category_id = $request->category;
		
		$category = BlueCategory::findOrFail($category_id);
		
		return view('pools.blue.categories.edit', compact('category', 'metadata'));
	}
	
	/*
	 * Update Category.
	 *
	 * @return Redirect
	 */
	public function update(Request $request)
	{
		$category_id = $request->category;
		
		$category = BlueCategory::findOrFail($category_id);
		
		$validation = array (
            'title' => 'bail|required|max:150',
        );
		$validator = Validator::make($request->all(), $validation);
		
		if ($validator->fails()) return back()->withInput()->withErrors($validator);
		
		$data = $request->except(['_token']);
        
        //Update Category
        $category->update($data);

        //Log action/event
        Log::create(['user_id' => $request->user()->id, 'log_desc' => 'updated ' . $category->title . ' category']);
        
        return back()->with('success', 'Category updated');
	}
	
	/*
	 * Delete Post.
	 *
	 * @return View
	 */
	public function destroy(Request $request)
	{
		$category_id = $request->category;
		
		BlueCategory::findOrFail($category_id)->delete();

		//Log action/event
		Log::create(['user_id' => $request->user()->id, 'log_desc' => 'deleted category with id ' . $category_id]);
        
        return redirect()->route('pools.blue.categories.index');
	}

}
