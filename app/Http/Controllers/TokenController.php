<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Token;
use Carbon\Carbon;

class TokenController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function store($referral_id, $token)
    {

        $data = [
            'referral_id' => $referral_id,
            'token' => $token,
            'expiry' => Carbon::now()->addDays(5),
        ];

        //create token
        Token::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($token)
    {
        return Token::where('token', $token)->get();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($token)
    {
     return Token::where('token', $token)->delete($token);
    }
}
