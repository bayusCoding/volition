<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Noreferral extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that are date mutable.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * Get related referral sent to user
     *
     * @return boolean
     */
    public function referral() {
        return $this->belongsTo('App\Referral');
    }

}
