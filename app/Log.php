<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    
    /**
     * The attributes that are date mutable.
     *
     * @var array
     */
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * Get using related to log activity
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user() {

        return $this->belongsTo('App\User');
    }
}
