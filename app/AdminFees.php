<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class AdminFees extends Model
{
    protected $table = 'admin_fees';

    /**
     * Date attribues
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'expiry'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('has_paid', function (Builder $builder) {
            $builder->where('has_paid', '=', 1);
        });
    }
}
