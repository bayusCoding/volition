@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Hello, {{ Auth::user()->firstname }}</h1>
    </div>
    @if(session()->has('success'))
        <div class="col-xs-12 col-md-6">
            <div class="notification-success">{{ session()->get('success') }}</div>
        </div>
    @endif
    @if(session()->has('warning'))
        <div class="col-xs-12 col-md-6">
            <div class="notification-warning">{{ session()->get('warning') }}</div>
        </div>
    @endif

    @if(!Auth::user()->is_Kyc && !Auth::user()->isAdmin)
        <div class="col-xs-12 col-md-12">
            <div class="notification-warning">Please fill in your KYC data, you will be unable to perform any operation without it. Click <a href="{{ url('settings') }}">here</a></div>
        </div>
    @endif

    <div class="col-xs-12"><span class="card-title">Current Estimated Portfolio Value</span></div>
    <!-- green pool card -->
    <div class="col-xs-12">
        <div class="row">
            <div id="greenpool-card" class="col-sm-6 col-md-4">
                <div class="box">
                    <div class="row">
                        <div class="col-xs-5 card-pool-left">
                            <span class="green-text"><h3>{{ $green_metrics['change'] }}%</h3></span>
                            <div class="greenpool-slot">
                                <h3>{{ $green_metrics['count'] }}</h3>
                                <p>Subscriptions</p>
                            </div>
                        </div>
                        <div class="col-xs-7 card-pool-right">
                            <h4>Green Pool</h4>
                            @if($green_metrics['count'] > 0)
                                <h5 class="pool-amount">₦{{ number_format($green_metrics['amount']) }}</h5>
                                <h5 class="pool-cycle">n/a</h5>
                                <a href="{{ route('pools.pool.show', 'Volition Green') }}" class="col-xs-9 btn-stroke-green">View Pool</a>
                            @else
                                <h5 class="pool-amount">No active investments </h5>
                                <h5 class="pool-cycle">n/a</h5> 
                                @if(Auth::user()->isAdmin)
                                <a href="{{ route('pools.pool.show', 'Volition Green') }}" class="col-xs-9 btn-stroke-green">View Pool</a>
                                @else 
                                <a href="{{ route('pools.create', 'Volition Green') }}" class="col-xs-9 btn-stroke-green">Invest</a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- black pool card -->
            <div id="blackpool-card" class="col-sm-6 col-md-4">
                <div class="box">
                    <div class="row">
                        <div class="col-xs-5 card-pool-left">
                            <span class="black-text">
                            <h3>{{ $black_metrics['change'] }}%</h3>
                        </span>
                            <div class="blackpool-slot">
                                <h3>{{ $black_metrics['count'] }}</h3>
                                <p>Subscriptions</p>
                            </div>
                        </div>
                        <div class="col-xs-7 card-pool-right">
                            <h4>Black Pool</h4>
                            @if($black_metrics['count'] > 0)
                                <h5 class="pool-amount">₦{{ number_format($black_metrics['amount']) }}</h5>
                                <h5 class="pool-cycle">n/a</h5>
                                <a href="{{ route('pools.pool.show', 'Volition Black') }}" class="col-xs-9 btn-stroke-black">View Pool</a>
                            @else
                                <h5 class="pool-amount">No active investments</h5>
                                <h5 class="pool-cycle">n/a</h5>
                                @if(Auth::user()->isAdmin)
                                <a href="{{ route('pools.pool.show', 'Volition Black') }}" class="col-xs-9 btn-stroke-black">View Pool</a>
                                @else 
                                <a href="{{ route('pools.create', 'Volition Black') }}" class="col-xs-9 btn-stroke-black">Invest</a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- red pool card -->
            <div id="redpool-card" class="col-sm-6 col-md-4">
                <div class="box">
                    <div class="row">
                        <div class="col-xs-5 card-pool-left">
                            <span class="red-text">
                            <h3>{{ $red_metrics['change'] }}%</h3>
                        </span>
                            <div class="redpool-slot">
                                <h3>{{ $red_metrics['count'] }}</h3>
                                <p>Subscriptions</p>
                            </div>
                        </div>
                        <div class="col-xs-7 card-pool-right">
                            <h4>Red Pool</h4>
                            @if($red_metrics['count'] > 0)
                                <h5 class="pool-amount">₦{{ number_format($red_metrics['amount']) }}</h5>
                                <h5 class="pool-cycle">n/a</h5>
                                <a href="{{ route('pools.pool.show', 'Volition Red') }}" class="col-xs-9 btn-stroke-red">View Pool</a>
                            @else
                                <h5 class="pool-amount">No active investments </h5>
                                <h5 class="pool-cycle">n/a</h5> 
                                @if(Auth::user()->isAdmin)
                                <a href="{{ route('pools.pool.show', 'Volition Red') }}" class="col-xs-9 btn-stroke-red">View Pool</a>
                                @else 
                                <a href="{{ route('pools.create', 'Volition Red') }}" class="col-xs-9 btn-stroke-red">Invest</a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- section 2 -->
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">Transaction History</span>
                <a href="{{ route('transactions.index') }}" class="btn-gold-xs pull-right">View more</a>
            </div>
        </div>
        <div class="col-md-12 table-box">
            <!-- table card -->
            <table>
                <thead>
                    <tr>
                        <th scope="col" aria-live="assertive" style="width: 30%;">Date</th>
                        <th scope="col" aria-live="assertive">Description</th>
                        <th scope="col" aria-live="assertive" style="width: 30%;">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($transactions) > 0)
                        @foreach($transactions as $transaction)
                            <tr>
                                <td data-label="Date">{{ $transaction->created_at->format('j M Y') }}</td>
                                <td data-label="Description">{{ $transaction->trans_desc }}</td>
                                <td data-label="amount" @if($transaction->trans_type == 0) class="green-text" @else class="red-text" @endif aria-live="assertive">₦{{ number_format($transaction->amount) }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td data-label="status" colspan="3">There are no transactions</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-xs-12">
                <span class="card-title">Recent Activity</span>
                <a href="{{ route('logs.index') }}" class="btn-gold-xs pull-right">View more</a>
            </div>
        </div>
        <div class="col-md-12 table-box">
            <!-- table card -->
            <table>
                <thead>
                    <tr>
                        <th scope="col" aria-live="assertive" style="width: 20%;">Date</th>
                        <th scope="col" aria-live="assertive">Description</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($logs) > 0)
                        @foreach($logs as $log)
                            <tr>
                                <td data-label="Date">{{ $log->created_at->format('j M Y') }}</td>
                                <td data-label="Description">
                                    @if(Auth::user()->isAdmin) {{ $log->user->firstname }} {{ $log->user->lastname }} @else You @endif
                                    {{ $log->log_desc }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td data-label="status" colspan="3">There are no activity log</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
