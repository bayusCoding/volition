<!DOCTYPE html>
<html lang="en" class="no-touch">

<head>
    <meta charset=" UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Get Financial Rewards. Joint Investment Opportunities. | Volition Capital</title>
    <!-- Search Engine -->
    <meta name="description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta name="image" content="www.volitioncap.com/opengraph.png">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Volition Capital">
    <meta itemprop="description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta itemprop="image" content="www.volitioncap.com/opengraph.png">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Volition Capital">
    <meta name="twitter:description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Volition Capital">
    <meta name="og:description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta name="og:image" content="www.volitioncap.com/opengraph.png">
    <meta name="og:url" content="www.volitioncap.com">
    <meta name="og:site_name" content="Volition Capital">
    <meta name="og:type" content="website">
    <!--Favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
    <link rel="manifest" href="img/favicons/manifest.json">
    <link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#a0830b">
    <meta name="theme-color" content="#ffffff">
    <!-- CSS stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/media.css" rel="stylesheet">
</head>

<body>
    <!--Header-->
    <header class="header--banner" style="height: 25rem;">
        <div class="mobile--header--wrap">
            <header class="mobile--nav">
                <a href="{{ url('/') }}" aria-label="homepage" aria-live="assertive">
                    <img src="img/Volition-logo.svg" alt="Volition-logo" class="header--logo">
                </a>
                <div class="hamburger-wrapper">
                    <span class="hamburger"></span>
                </div>
                <div class="cross-wrapper">
                    <span class="cross"></span>
                </div>
            </header>
            <!-- mobile header -->
            <div class="menu--container">
                <ul class="mobile--nav--menu">
                    <li class="nav--links"><a href="#about" aria-label="About section" aria-live="assertive">About</a></li>
                    <li class="nav--links"><a href="#pools" aria-label="investment pools section" aria-live="assertive">Pools</a></li>
                    <li class="nav--links"><a href="#returns" aria-label="returns section" aria-live="assertive">Returns</a></li>
                    <li class="nav--links"><a href="#people" aria-label="people section" aria-live="assertive">People</a></li>
                    <li class="nav--links"><a href="#contact" aria-label="contact us section" aria-live="assertive">Contact</a></li>
                    @auth
                        <li class="nav--links"><a href="{{ url('/home') }}" class="btn--xs btn--stroke" style="background-color: none; border-color: none;">{{ Auth::user()->firstname }}</a></li>
                    @else
                        <li class="nav--links"><a href="{{ route('register') }}" aria-label="Sign up" aria-live="assertive" class="btn--xs btn--fill">Sign Up</a></li>
                        <li class="nav--links"><a href="{{ route('login') }}" aria-label="Log in" aria-live="assertive" class="btn--xs btn--stroke">Log In</a></li>
                    @endauth
                </ul>
            </div>

        </div>
        <div class="container">
            <!--navigation-->
            <nav class="landing nav--container middle-xs row">
                <a href="{{ url('/') }}" aria-label="homepage" aria-live="assertive">
                    <img src="img/Volition-logo.svg" alt="Volition-logo" class="header--logo">
                </a>
                <ul class="nav--menu ">
                    <li class="nav--links"><a href="#about" aria-label="About section" aria-live="assertive">About</a></li>
                    <li class="nav--links"><a href="#pools" aria-label="investment pools section" aria-live="assertive">Pools</a></li>
                    <li class="nav--links"><a href="#returns" aria-label="returns section" aria-live="assertive">Returns</a></li>
                    <li class="nav--links"><a href="#people" aria-label="people section" aria-live="assertive">People</a></li>
                    <li class="nav--links"><a href="#contact" aria-label="contact us section" aria-live="assertive">Contact</a></li>
                    @auth
                        <li class="nav--links"><a href="{{ url('/home') }}" class="btn--xs btn--stroke" style="background-color: none; border-color: none;">{{ Auth::user()->firstname }}</a></li>
                    @else
                        <li class="nav--links"><a href="{{ route('register') }}" aria-label="Sign up" aria-live="assertive" class="btn--xs btn--fill">Sign Up</a></li>
                        <li class="nav--links"><a href="{{ route('login') }}" aria-label="Log in" aria-live="assertive" class="btn--xs btn--stroke" style="margin-left:-10px;">Log In</a></li>
                    @endauth
                </ul>
            </nav>

            <!-- header-content-->
            <div class="col-md-6 col-xs-12 header--content" style="transform: translateY(180%);">
                <h2 class="header--title">Cooperative Bye-Laws</h2>
            </div>
            <div class="col-md-6 col-xs-12">
            </div>
        </div>
    </header>
    <!-- The body Content -->
    <main>
        <!-- About section -->
        <a id="about" class="in-page-link" tabindex="-1"></a>
        <section id="section--grey--bg">
            <div class="container">
                <div class="row middle-xs">
                    <div class="col-md-12 col-xs-12">
                        {!! $page_content !!}
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer -->
        <a id="contact" class="in-page-link" tabindex="-1"></a>
        <footer class="footer--black--color">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-8 text--left">
                        <img src="img/volition-footer-logo.svg" alt="Volition-footer-logo">
                        <p>Volition functions as a “family and friends” investment platform. We collectively invest to earn higher returns.</p>
                    </div>
                    <div class="col-md-6 col-sm-4 col-xs-6">
                    </div>
                    <div class="col-sm-5 col-md-3 text--left">
                        <h6>Contact Us</h6>
                        <p>iLX Centre, 8 Providence Street, Lekki Phase One, Lagos
                        </p>
                        <ul class="list">
                            <li>
                                <a href="tel:+234 705 651 1542" aria-label="phone number: 07056511542" aria-live="assertive">
                                    <span aria-label="+234 705 651 1542">+234 705 651 1542 (Whatsapp Only)</span>
                                </a>
                            </li>
                            <li>
                                <a href="mailto:ask@volitioncap.com" aria-label="email for support" aria-live="assertive">
                                    <span>ask@volitioncap.com</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row footer--copy">
                    <div class="col-sm-7 col-xs-12">
                        <span class="type--fine-print">
                            Volition Capital ©
                            <span>2018.</span> All Rights Reserved
                        </span>
                    </div>
                    <div class="col-sm-5 col-xs-12 text--right">
                        <a class="type--fine-print" href="#" aria-label="privacy policy" aria-live="assertive">Privacy Policy</a> &nbsp; &nbsp; &nbsp;
                        <a class="type--fine-print" href="{{ url('terms-and-conditions') }}" aria-label="terms of use" aria-live="assertive">Terms & Conditions</a> &middot; &middot;
                        <a class="type--fine-print" href="{{ url('corporate-bylaw') }}" aria-label="cooperative bye-laws" aria-live="assertive">Cooperative Bye-Laws</a>
                    </div>
                </div>
            </div>

        </footer>
    </main>

    <!--js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113412016-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-113412016-1');
    </script>


    <script>
        $(document).ready(function() {

            $(".cross ").hide();
            $(".menu--container ").hide();
            $(".hamburger ").click(function() {
                $(".menu--container ").slideToggle("slow ", function() {
                    $(".hamburger ").hide();
                    $(".cross ").show();
                });
            });

            $(".cross ").click(function() {
                $(".menu--container ").slideToggle("slow ", function() {
                    $(".cross ").hide();
                    $(".hamburger ").show();
                });
            });

        });
    </script>
</body>

</html>