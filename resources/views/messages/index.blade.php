@extends('layouts.app')
@section('content')
<style>
    .size{
        height: 130px
    }
</style>
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Messages</h1>

        {!! Form::open(['route' => ['message.broadcast'], 'method' => 'POST', 'id' => 'form-id']) !!}
        <div class="row">
            <div class="col-md-5">
                <h3 class="card-title">Send Broadcast Messages</h3>


                @if(session()->has('success'))
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="notification-success">{{ session()->get('success') }}</div>
                        </div>
                    </div>
                @endif
                <div style="background-color: #ffffff" class="form-group form-group-default">
                    <label>Receipient:</label>
                    <select required name="receipient" id="receipient" class="form-control">
                        <option selected="selected" value="all">All Members</option>
                        <option value="subscribers">Subscribed Members Only</option>
                    </select>
                </div>


                <div style="background-color: #ffffff" class="form-group form-group-default">
                    <label>Subject:</label>
                    <input type="text" name="subject" class="form-control" required />
                </div>



                <div style="background-color: #ffffff" class="form-group form-group-default size">
                    <label>Message:</label>
                    <textarea
                        height="100"
                        required
                        rows="40"
                        cols="50"
                        aria-label="enter message"
                        name="msg"
                        id="msg"
                        class="form-control"></textarea>
                </div>

                <p><button type="submit" form="form-id" class="btn btn-gold" aria-label="send">Send message</button></p>
            
            </div>
        </div>
        
        {!! Form::close() !!}
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">View All Messages</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table class="datatable">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive" style="width: 15%;">Date</th>
                            <th scope="col" aria-live="assertive">User</th>
                            <th scope="col" aria-live="assertive">Message</th>
                            <th scope="col" aria-live="assertive" style="width: 15%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($messages) > 0)
                            @foreach($messages as $message)
                                <tr>
                                    <td data-label="Date">{{ Carbon\Carbon::parse($message->created_at)->format('j M Y') }}<br><small>{{ Carbon\Carbon::parse($message->created_at)->diffForHumans() }}</small></td>
                                    <td data-label="User">{{ $message->firstname }} {{ $message->lastname }}</td>
                                    <td data-label="Message">{{ substr($message->body,0,50).'...' }}</td>
                                    <td data-label="Action" aria-live="assertive"><a href="{{ route('messages.show', ['to' => $message->user_id]) }}" class="btn btn-xs btn-stroke-gold">Reply</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">No messages</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        {{ $messages->links() }}
    </div>
</div>
@else
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Get in Touch</h1>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">Message Support</span>
            </div>
        </div>
        <div class="box">
            <div class="row">
                <div class="col-xs-12 col-md-12" aria-live="assertive">
                    @if(count($messages) > 0)
                        @foreach($messages as $message)
                            @if($message->type == 0)
                            <div class="chat-col">
                                <div class="customer-chat-info">
                                    <span class="customer">You</span>&nbsp;&nbsp;<span class="date-time">{{ $message->created_at->diffForHumans() }}</span>
                                </div>
                                <div class="chat-content">{{ $message->body }}</div>
                            </div>
                            @else
                                <div class="support-chat-col">
                                    <div class="customer-chat-info">
                                        <span class="support">Support</span>&nbsp;&nbsp;<span class="date-time">{{ $message->created_at->diffForHumans() }}</span>
                                    </div>
                                    <div class="chat-content">{{ $message->body }}</div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                    {!! Form::open(['route' => ['users.message'], 'method' => 'POST', 'id' => 'contact-form']) !!}
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group form-group-default">
                                    <label>Type your message</label>
                                    <textarea name="body" class="form-control" rows="20" cols="50" aria-label="enter message"></textarea>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-12 text-right">
                                <button type="submit" form="contact-form" class="btn btn-gold" aria-label="proceed">Send message</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">More Questions?</span>
            </div>
        </div>
        <div class="table-box">
            <dl class="horizontal-dl-2" style="padding-bottom:0px; padding-top: 20px;">
                <dt>Email Address</dt>
                <dd><strong><a href="mailto://ask@volitioncap.com" class="gold-text">ask@volitioncap.com</a></strong></dd>
                <dt style="border-bottom: 0px;">Phone</dt>
                <dd style="border-bottom: 0px;"><strong><a href="tel://ask@volitioncap.com" class="gold-text">+234 705 651 1542 (Whatsapp Only)</a></strong></dd>
            </dl>
        </div>
    </div>
</div>
@endif
@endsection