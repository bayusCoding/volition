@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Messages</h1>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">{{ $metadata['name'] }}</span>
            </div>
        </div>
        <div class="box">
            <div class="row">
                <div class="chat-form">
                    <div class="col-xs-12 col-md-12" aria-live="assertive">

                        {!! Form::open(['route' => ['users.message'], 'method' => 'POST', 'id' => 'contact-form']) !!}
                        <input type="hidden" name="message_to" value="{{ $metadata['to'] }}">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="form-group form-group-default size">
                                    <label>Type your message</label>
                                    <textarea name="body" class="form-control" rows="20" cols="50" aria-label="enter message"></textarea>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-12 text-right">
                                <button type="submit" form="contact-form" class="btn btn-gold" aria-label="proceed">Send message</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                        @if(count($messages) > 0)
                            @foreach($messages as $message)
                                @if($message->type == 0)
                                <div class="chat-col">
                                    <div class="customer-chat-info">
                                        <span class="customer">{{ $metadata['name'] }}</span>&nbsp;&nbsp;<span class="date-time">{{ $message->created_at->diffForHumans() }}</span>
                                    </div>
                                    <div class="chat-content">{{ $message->body }}</div>
                                </div>
                                @else
                                    <div class="support-chat-col">
                                        <div class="customer-chat-info">
                                            <span class="support">Support</span>&nbsp;&nbsp;<span class="date-time">{{ $message->created_at->diffForHumans() }}</span>
                                        </div>
                                        <div class="chat-content">{{ $message->body }}
                                        <div style="margin-top:8px;"><a onclick="return ConfirmDelete()" href="{{ route('message.delete', ['id' => $message->id]) }}">Delete message</a></div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>
    function ConfirmDelete() {
        return confirm("Are you sure you want to delete this message?");
    }
</script>

