@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Referrals - Request</h1>
    </div>

    <div class="col-xs-12"><span class="card-title">View all Referrals Request</span></div>
</div>    
<!-- Second section -->
<div class="row">
    <div class="col-md-12">
        <!-- table card -->
        <table class="datatable">
            <thead>
                <tr>
                    <th scope="col" aria-live="assertive">Date</th>
                    <th scope="col" aria-live="assertive">Name</th>
                    <th scope="col" aria-live="assertive">Email</th>
                    <th scope="col" aria-live="assertive">Phone</th>
                    <th scope="col" aria-live="assertive">How</th>
                    <th scope="col" aria-live="assertive">Referral Code</th>
                </tr>
            </thead>
            <tbody>
                @if(count($noreferrals) > 0)
                    @foreach($noreferrals as $noreferral)
                        <tr>
                            <td data-label="Date">{{ $noreferral->created_at->toDateString() }}</td>
                            <td data-label="Name">{{ $noreferral->firstname }} {{ $noreferral->lastname }}</td>
                            <td data-label="Email">{{ $noreferral->email }}</td>
                            <td data-label="Phone">{{ $noreferral->phone }}</td>
                            <td data-label="How">{{ $noreferral->how }}</td>
                            <td data-label="Referral Code" aria-live="assertive">
                                @php
                                    if(!is_null($noreferral->referral)) {
                                        echo $noreferral->referral->referral_code;
                                    } 
                                @endphp
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td data-label="Status" colspan="3">No referrals have been made</td></tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
{{ $noreferrals->links() }}
@endif
@endsection
