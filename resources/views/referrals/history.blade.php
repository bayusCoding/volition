@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Referrals</h1>
    </div>

    <div class="col-xs-12"><span class="card-title">View all Referrals Made</span></div>
</div>    
<!-- Second section -->
<div class="row">
    <div class="col-md-12">
        <!-- table card -->
        <table class="datatable">
            <thead>
                <tr>
                    <th scope="col" aria-live="assertive">Date</th>
                    <th scope="col" aria-live="assertive">Email</th>
                    <th scope="col" aria-live="assertive">Referrer</th>
                    <th scope="col" aria-live="assertive">Referral Code</th>
                </tr>
            </thead>
            <tbody>
                @if(count($referrals) > 0)
                    @foreach($referrals as $referral)
                        <tr>
                            <td data-label="Date">{{ $referral->created_at->format('j M Y') }}</td>
                            <td data-label="Email">{{ $referral->email }}</td>
                            <td data-label="Referrer">{{ $referral->user->firstname }} {{ $referral->user->lastname }}</td>
                            <td data-label="Referral Code" aria-live="assertive">{{ $referral->referral_code }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr><td data-label="Status" colspan="3">No referrals have been made</td></tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
{{ $referrals->links() }}
@endif
@endsection
