@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Referrals</h1>
    </div>

    <div class="col-xs-12"><span class="card-title">View Referrals</span></div>

    @if(session()->has('success'))
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 col-md-6">
                    <div class="notification-success">{{ session()->get('success') }}</div>
                </div>
            </div>
        </div>
    @endif

    @if(session()->has('errors'))
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 col-md-6">
                    <div class="notification-warning">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="col-xs-12 col-md-6">
        <div class="box">
            {!! Form::open(['route' => ['referrals.noreferral.invite'], 'method' => 'POST', 'accept-charset' => 'UTF-8', '' => '']) !!}
                <div class="row">
                    <p class="desc-text2 text-left">Send referral code to verified request.</p>
                    <div class="col-md-12">
                        <div class="form-group form-group-default{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>Email Address</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" aria-label="email" multiple="multiple">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-gold" aria-label="Invite">Send</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    <!-- summary card -->
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>{{ $noreferrals_count }}</h1>
                    <p class="desc-text">Referrals Request</p>
                </div>
            </div>
        </div>
    </div>
    <!-- summary card -->
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>{{ $referrals_count }}</h1>
                    <p class="desc-text">Referrals Made</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Second section -->
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <span class="card-title">Referral Request</span>
                        <a href="{{ route('referrals.noreferral') }}" class="btn-gold-xs pull-right">View more</a>
                    </div>
                </div>
                <div class="col-md-12 table-box">
                    <!-- table card -->
                    <table>
                        <thead>
                            <tr>
                                <th scope="col" aria-live="assertive" width="20%">Date</th>
                                <th scope="col" aria-live="assertive" width="50%">Email</th>
                                <th scope="col" aria-live="assertive">Referral Code</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($noreferrals) > 0)
                                @foreach($noreferrals as $noreferral)
                                    <tr>
                                        <td data-label="Date">{{ $noreferral->created_at->format('j M Y') }}</td>
                                        <td data-label="Email">{{ $noreferral->email }}</td>
                                        <td data-label="Referral Code" aria-live="assertive">
                                            @php
                                                if(!is_null($noreferral->referral)) {
                                                    echo $noreferral->referral->referral_code;
                                                } 
                                            @endphp
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td data-label="Status" colspan="3">No referral request</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <!-- Third section -->
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <span class="card-title">Referral History</span>
                        <a href="{{ route('referrals.history') }}" class="btn-gold-xs pull-right">View more</a>
                    </div>
                </div>
                <div class="col-md-12 table-box">
                    <!-- table card -->
                    <table>
                        <thead>
                            <tr>
                                <th scope="col" aria-live="assertive" width="20%">Date</th>
                                <th scope="col" aria-live="assertive" width="50%">Email</th>
                                <th scope="col" aria-live="assertive">Referral Code</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($referrals) > 0)
                                @foreach($referrals as $referral)
                                    <tr>
                                        <td data-label="Date">{{ $referral->created_at->format('j M Y') }}</td>
                                        <td data-label="Email">{{ $referral->email }}</td>
                                        <td data-label="Referral Code" aria-live="assertive">{{ $referral->referral_code }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr><td data-label="Status" colspan="3">No referrals have been made</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Referrals</h1>
    </div>

    <div class="col-xs-12"><span class="card-title">Make a New Referral</span></div>

    @if(session()->has('success'))
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 col-md-6">
                    <div class="notification-success">{{ session()->get('success') }}</div>
                </div>
            </div>
        </div>
    @endif

    @if(session()->has('errors'))
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 col-md-6">
                    <div class="notification-warning">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="col-xs-12 col-md-6">
        <div class="box">
            {!! Form::open(['route' => ['referrals.invite'], 'method' => 'POST', 'accept-charset' => 'UTF-8', '' => '']) !!}
                <div class="row">
                    <p class="desc-text2 text-left">Members earn 20% of the first admin fee of a new member, in each of the pools the member signs up for.</p>
                    <div class="col-md-12">
                        <div class="form-group form-group-default{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label>Email Address</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" aria-label="email" multiple="multiple">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-gold" aria-label="Invite">Invite</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    <!-- summary card -->
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>{{ $referrals_count }}</h1>
                    <p class="desc-text">Referrals Made</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>₦{{ number_format($wallet->balance) }}</h1>
                    <p class="desc-text">Total Referral Income</p>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Second section -->
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">Referral History</span>
            </div>
        </div>
        <div class="col-md-12 table-box">
            <!-- table card -->
            <table class="datatables">
                <thead>
                    <tr>
                        <th scope="col" aria-live="assertive">Date</th>
                        <th scope="col" aria-live="assertive">Email</th>
                        <th scope="col" aria-live="assertive">Referral Code</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($referrals) > 0)
                        @foreach($referrals as $referral)
                            <tr>
                                <td data-label="Date">{{ $referral->created_at->format('j M Y') }}</td>
                                <td data-label="Email">{{ $referral->email }}</td>
                                <td data-label="Referral Code" aria-live="assertive">{{ $referral->referral_code }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr><td data-label="Status" colspan="3">You've have made no referrals</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    {{ $referrals->links() }}
</div>
@endif
@endsection
