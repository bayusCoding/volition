<!DOCTYPE html>
<html lang="en" class="no-touch">

<head>
    <meta charset=" UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Get Financial Rewards. Joint Investment Opportunities. | Volition Capital</title>
    <!-- Search Engine -->
    <meta name="description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta name="image" content="www.volitioncap.com/opengraph.png">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Volition Capital">
    <meta itemprop="description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta itemprop="image" content="www.volitioncap.com/opengraph.png">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Volition Capital">
    <meta name="twitter:description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Volition Capital">
    <meta name="og:description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta name="og:image" content="www.volitioncap.com/opengraph.png">
    <meta name="og:url" content="www.volitioncap.com">
    <meta name="og:site_name" content="Volition Capital">
    <meta name="og:type" content="website">
    <!--Favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png">
    <link rel="manifest" href="img/favicons/manifest.json">
    <link rel="mask-icon" href="img/favicons/safari-pinned-tab.svg" color="#a0830b">
    <meta name="theme-color" content="#ffffff">
    <!-- CSS stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/media.css" rel="stylesheet">
</head>

<body>
    <!--Header-->
    <header class="header--banner">
        <div class="header--globe"></div>
        <span class="pulse pulse-1"></span>
        <span class="pulse pulse-2"></span>
        <span class="pulse pulse-3"></span>
        <span class="pulse pulse-4"></span>
        <div class="mobile--header--wrap">
            <header class="mobile--nav">
                <a href="{{ url('/') }}" aria-label="homepage" aria-live="assertive">
                    <img src="img/Volition-logo.svg" alt="Volition-logo" class="landing header--logo">
                </a>
                <div class="hamburger-wrapper">
                    <span class="hamburger"></span>
                </div>
                <div class="cross-wrapper">
                    <span class="cross" style="display: none;"></span>
                </div>
            </header>
            <!-- mobile header -->
            <div class="menu--container">
                <ul class="mobile--nav--menu">
                    <li class="nav--links"><a href="#about" aria-label="About section" aria-live="assertive">About</a></li>
                    <li class="nav--links"><a href="#pools" aria-label="investment pools section" aria-live="assertive">Pools</a></li>
                    <li class="nav--links"><a href="#returns" aria-label="returns section" aria-live="assertive">Returns</a></li>
                    <li class="nav--links"><a href="#people" aria-label="people section" aria-live="assertive">People</a></li>
                    <li class="nav--links"><a href="#contact" aria-label="contact us section" aria-live="assertive">Contact</a></li>
                    @auth
                        <li class="nav--links"><a href="{{ url('/home') }}" class="btn--xs btn--stroke" style="background-color: none; border-color: none;">{{ Auth::user()->firstname }}</a></li>
                    @else
                        <li class="nav--links"><a href="{{ route('register') }}" aria-label="Sign up" aria-live="assertive" class="btn--xs btn--fill">Sign Up</a></li>
                        <li class="nav--links"><a href="{{ route('login') }}" aria-label="Log in" aria-live="assertive" class="btn--xs btn--stroke">Log In</a></li>
                    @endauth
                </ul>
            </div>

        </div>
        <div class="container">
            <!--navigation-->
            <nav class="landing nav--container middle-xs row">
                <a href="{{ url('/') }}" aria-label="homepage" aria-live="assertive">
                    <img src="img/Volition-logo.svg" alt="Volition-logo" class="header--logo">
                </a>
                <ul class="nav--menu ">
                    <li class="nav--links"><a href="#about" aria-label="About section" aria-live="assertive">About</a></li>
                    <li class="nav--links"><a href="#pools" aria-label="investment pools section" aria-live="assertive">Pools</a></li>
                    <li class="nav--links"><a href="#returns" aria-label="returns section" aria-live="assertive">Returns</a></li>
                    <li class="nav--links"><a href="#people" aria-label="people section" aria-live="assertive">People</a></li>
                    <li class="nav--links"><a href="#contact" aria-label="contact us section" aria-live="assertive">Contact</a></li>
                    @auth
                        <li class="nav--links"><a href="{{ url('/home') }}" class="btn--xs btn--stroke" style="background-color: none; border-color: none;">{{ Auth::user()->firstname }}</a></li>
                    @else
                        <li class="nav--links"><a href="{{ route('register') }}" aria-label="Sign up" aria-live="assertive" class="btn--xs btn--fill">Sign Up</a></li>
                        <li class="nav--links"><a href="{{ route('login') }}" aria-label="Log in" aria-live="assertive" class="btn--xs btn--stroke" style="margin-left:15px;">Log In</a></li>
                    @endauth
                </ul>
            </nav>

            <!-- header-content-->
            <div class="col-md-6 col-xs-12 header--content">
                <h2 class="header--title">Welcome.</h2>
                <p>We help the honest & hardworking to earn financial rewards, through joint investment opportunities.</p>
                <a href="{{ route('register') }}" aria-label="Sign up" aria-live="assertive" class="btn btn--fill">Sign Up</a>
                <a href="{{ url('login') }}" aria-label="Log in" aria-live="assertive" class="btn btn--stroke">Log in</a>
            </div>
            <div class="col-md-6 col-xs-12">
            </div>
        </div>
    </header>
    <!-- The body Content -->
    <main>
        <!-- About section -->
        <a id="about" class="in-page-link" tabindex="-1"></a>
        <section id="section--grey--bg">
            <div class="container">
                <div class="row middle-xs">
                    <div class="col-md-5 col-xs-12">
                        <h3>About Volition</h3>
                        <p>Have you ever felt like only high net-worth individuals (HNIs) and large institutions have access to great financial deals?</p>
                        <p>This is because, many individuals have limited capacity, expertise and time to invest. A group definitely has more leverage and capital. </p>
                        <p>At Volition, we manage a “family and friends” platform that combines our members’ resources, so we can collectively invest and earn higher returns.</p>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <img src="img/v-about.png" alt="about-volition">
                    </div>
                </div>
            </div>
        </section>
        <!-- Investment pools section -->
        <a id="pools" class="in-page-link" tabindex="-1"></a>
        <section class="section--goldtint--color">
            <div class="container">
                <div class="col-xs-12 col-sm-10 col-md-10">
                    <h3 class="section--title">Investment Pools</h3>
                    <!--Green Pool -->
                    <div class="white--card">
                        <div class="row center-xs">
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <img src="img/volition-green-logo.svg" alt="Volition-green-logo" class="pool--logo">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <p>Low to medium risk and principal guaranteed. We invest in Money Market Funds, Agriculture and Real Estate.</p>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <a href="{{ route('register') }}" aria-label="Join this pool" aria-live="assertive" class="btn btn--xs btn--green"> Join Pool </a>
                            </div>
                        </div>
                    </div>
                    <!-- Black pool-->
                    <div class="white--card">
                        <div class="row center-xs">
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <img src="img/volition-black-logo.svg" alt="Volition-black-logo" class="pool--logo">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <p>Medium risk and non-principal guaranteed. We invest in the Capital Markets.</p>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <a href="{{ route('register') }}" aria-label="Join this pool" aria-live="assertive" class="btn btn--xs btn--black"> Join Pool </a>
                            </div>
                        </div>
                    </div>
                    <!-- Red pool -->
                    <div class="white--card">
                        <div class="row center-xs">
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <img src="img/volition-red-logo.svg" alt="Volition-red-logo" class="pool--logo">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <p>High risk and non-principal guaranteed. We invest in Blockchain Applications.</p>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <a href="{{ route('register') }}" aria-label="Join this pool" aria-live="assertive" class="btn btn--xs btn--red"> Join Pool </a>
                            </div>
                        </div>
                    </div>
                    <!-- Blue pool -->
                    <div class="white--card">
                        <div class="row center-xs">
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <img src="img/volition-blue-logo.svg" alt="Volition-blue-logo" class="pool--logo">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <p class="vl--b">We scouts for projects members may independently invest in. From opportunities to buy land to the option of funding a farm, we constantly search for the best deals for members, at exclusive returns.</p>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <a href="{{ route('register') }}" aria-label="Join this pool" aria-live="assertive" class="btn btn--xs btn--blue"> Join Pool </a>
                            </div>
                        </div>
                    </div>
                    <!-- Priviledge pool -->
                    <div class="white--card">
                        <div class="row center-xs">
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <img src="img/volition-privilege-logo.svg" alt="Volition-privilege-logo" class="pool--logo">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <p>A private portfolio where we structure custom investment plans, based on a member's investment objectives and risk profile. </p>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <a href="{{ route('register') }}" aria-label="Join this pool" aria-live="assertive" class="btn btn--xs btn--purple"> Join Pool </a>
                            </div>
                        </div>
                    </div>
                    <!--Business pool -->
                    <div class="white--card">
                        <div class="row center-xs">
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <img src="img/volition-business-logo.svg" alt="Volition-business-logo" class="pool--logo">
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <p>A custom portfolio for businesses.</p>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <a href="{{ route('register') }}" aria-label="Join this pool" aria-live="assertive" class="btn btn--xs btn--grey"> Join Pool </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- returns section -->
        <a id="returns" class="in-page-link" tabindex="-1"></a>
        <section id="section--returns">
            <div class="container">
                <div class="white--card">
                    <div class="row">
                        <div class="col-md-5 col-xs-12">
                            <h1>100</h1>
                            <p>Verified Members</p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <h4>We have subscribers in these countries:</h4>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <ul>
                                        <li>Nigeria</li>
                                        <li>Germany</li>
                                        <li>France</li>
                                        <li>United Kingdom</li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <ul>
                                        <li>USA</li>
                                        <li>South Africa</li>
                                        <li>Czech Republic</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row between-xs">
                    <h3 class="section--title">Returns</h3>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="green--card">
                            <h1>30%</h1>
                            <h6>per annum</h6>
                            <p>Green Pool</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="black--card">
                            <h1>40%</h1>
                            <h6>per annum</h6>
                            <p>Black Pool</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="red--card">
                            <h1>60%</h1>
                            <h6>per annum</h6>
                            <p>Red Pool</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Our people section -->
        <a id="people" class="in-page-link" tabindex="-1"></a>
        <section id="section--people">
            <div class="container">
                <h3 class="section--title">Our People</h3>
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="person--profile--container">
                            <div class="person--img" style="background-image: url(./img/Kola-Oyeneyin.jpg); box-shadow: 0 10px 30px 3px rgba(85,85,85,0.2);">
                                <div class="person--img--name shrt--text">Kola Oyeneyin</div>
                                <span class="image--title--bg"></span>
                                <div class="person--textbox">
                                    <a href="javascript:void(0)"><img src="img/close_volition.svg" alt="close" class="close--btn"></a>
                                    <div>

                                        <p class="person--textbox--text text--body">
                                            Kola Oyeneyin is the Founder/CEO of Venia Group, a venture creation and development company, with investments cutting across Coworking spaces, Digital Insurance and Human Capital Development Kola is also the Founder of AutoGenius, Nigeria’s first digital
                                            auto insurance platform; and The iLx Center - a learning and development company, with emphasis on building capacity within SMEs/Entrepreneurs and Young Professionals.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="person--profile--container">
                            <div class="person--img " style="background-image: url(./img/Subomi-Plumptre.jpg); box-shadow: 0 10px 30px 3px rgba(85,85,85,0.2);">
                                <div class="person--img--name lng--text">Subomi Plumptre</div>
                                <span class="image--title--bg"></span>
                                <div class="person--textbox">
                                    <a href="javascript:void(0)"><img src="img/close_volition.svg" alt="close" class="close--btn"></a>
                                    <div>
                                        <p class="person--textbox--text text--body">
                                            Subomi Plumptre is an investor, investment educator, strategist, cultural trendspotter and social media denizen. Her affiliation with Volition Capital, is drawn from a desire to help the honest & hardworking to earn financial rewards. She is a Director
                                            at Alder Consulting, a leading brand strategy firm.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="person--profile--container">
                            <div class="person--img" style="background-image: url(./img/Zeal-Akaraiwe.jpg); box-shadow: 0 10px 30px 3px rgba(85,85,85,0.2);">
                                <div class="person--img--name shrt--text">Zeal Akaraiwe</div>
                                <span class="image--title--bg"></span>
                                <div class="person--textbox">
                                    <a href="javascript:void(0)"><img src="img/close_volition.svg" alt="close" class="close--btn"></a>
                                    <div>
                                        <p class="person--textbox--text text--body">
                                            Zeal Akaraiwe currently has over 17 years experience in the financial sector. He joined Standard Chartered treasury in 2004 prior to which he had spent over 5 years working in 3 other banks spanning HR and Admin to Corporate Banking. He was a key member
                                            of the Wholesale Bank Management Committee in Standard Chartered Nigeria and effective 2012, for a 2year term, Zeal sat on the governing council of the FMDA Nigeria (Financial Markets Dealers Association).
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- CTA -->
        <section id="section--gold">
            <div class="container">
                <h3 class="section--title">Join Us</h3>
                <div class="row center-xs">
                    <div class="col-md-7 col-xs-12">
                        <p>Membership is by invitation only. We are a trusted network of family & friends, you must be referred by an existing member, or vouched for by a member our team. If you have a referral code, please click on the link sent to your email. If you don't have one, click on the button below to request. Your request will be treated in 48 hours. </p>
                    </div>
                    <div class="col-xs-12 col-sm-7">
                        <a href="{{ url('/register') }}" class="btn--xs btn--stroke">Request Referral</a>
                        <!-- Begin MailChimp Signup Form -->
                        <!-- <form action="https://volitioncap.us17.list-manage.com/subscribe/post?u=6f605c8fe57a4db5afbb67009&amp;id=aa13493414" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div class="input--email--bg ">
                                <input type="email" name="EMAIL" class="email" aria-label="enter your email address" aria-live="assertive" placeholder="Email address">
                                <input type="submit" value="Sign Up" aria-label="Sign Up" class="cta--btn">
                            </div>
                        </form> -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer -->
        <a id="contact" class="in-page-link" tabindex="-1"></a>
        <footer class="footer--black--color">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-8 text--left">
                        <img src="img/volition-footer-logo.svg" alt="Volition-footer-logo">
                        <p>Volition functions as a “family and friends” investment platform. We collectively invest to earn higher returns.</p>
                    </div>
                    <div class="col-md-6 col-sm-4 col-xs-6">
                    </div>
                    <div class="col-sm-5 col-md-3 text--left">
                        <h6>Contact Us</h6>
                        <p>iLX Centre, 8 Providence Street, Lekki Phase One, Lagos
                        </p>
                        <ul class="list">
                            <li>
                                <a href="tel:+234 705 651 1542" aria-label="phone number: 07056511542" aria-live="assertive">
                                    <span aria-label="+234 705 651 1542">+234 705 651 1542 (Whatsapp Only)</span>
                                </a>
                            </li>
                            <li>
                                <a href="mailto:ask@volitioncap.com" aria-label="email for support" aria-live="assertive">
                                    <span>ask@volitioncap.com</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="row footer--copy">
                    <div class="col-sm-7 col-xs-12">
                        <span class="type--fine-print">
                            Volition Capital ©
                            <span>2018.</span> All Rights Reserved
                        </span>
                    </div>
                    <div class="col-sm-5 col-xs-12 text--right">
                        <a class="type--fine-print" href="#" aria-label="privacy policy" aria-live="assertive">Privacy Policy</a> &nbsp; &nbsp; &nbsp;
                        <a class="type--fine-print" href="{{ url('terms-and-conditions') }}" aria-label="terms of use" aria-live="assertive">Terms & Conditions</a> &middot; &middot;
                        <a class="type--fine-print" href="{{ url('corporate-bylaw') }}" aria-label="cooperative bye-laws" aria-live="assertive">Cooperative Bye-Laws</a>
                    </div>
                </div>
            </div>

        </footer>
    </main>

    <!--js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js "></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113412016-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-113412016-1');
    </script>


    <script>
        $(document).ready(function() {

            $(".cross ").hide();
            $(".menu--container ").hide();
            $(".hamburger ").click(function() {
                $(".menu--container ").slideToggle("slow ", function() {
                    $(".hamburger ").hide();
                    $(".cross ").show();
                });
            });

            $(".cross ").click(function() {
                $(".menu--container ").slideToggle("slow ", function() {
                    $(".cross ").hide();
                    $(".hamburger ").show();
                });
            });


            //$('.person--profile--container').on('click', function() {$('person--textbox').css('opacity','1')});
            // $('.person--profile--container').on('click', function() {$(this).$('.person--textbox').show()});

            $('.person--profile--container').click(function(e) {

                e.preventDefault();
                // hide all span
                var $this = $(this).parent().find('.person--textbox');

                $this.toggle();

            });

        });
    </script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.5/require.js"></script> -->
    <script type="text/javascript" src="https://s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script>
    <script>
        function showSignUpPopUp(e) {
            e.preventDefault();
            require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us17.list-manage.com","uuid":"6f605c8fe57a4db5afbb67009","lid":"aa13493414"})
            });
            document.cookie = "MCPopupClosed=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        }
        $(".signup--popup").click(function(e) {showSignUpPopUp(e)});
    </script>
</body>

</html>
