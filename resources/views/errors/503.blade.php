<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset=" UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Get Financial Rewards. Joint Investment Opportunities. | Volition Capital Dashboard</title>
    <!-- Search Engine -->
    <meta name="description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta name="image" content="www.volitioncap.com/opengraph.png">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Volition Capital">
    <meta itemprop="description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta itemprop="image" content="www.volitioncap.com/opengraph.png">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Volition Capital">
    <meta name="twitter:description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Volition Capital">
    <meta name="og:description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta name="og:image" content="www.volitioncap.com/opengraph.png">
    <meta name="og:url" content="www.volitioncap.com">
    <meta name="og:site_name" content="Volition Capital">
    <meta name="og:type" content="website">
    <!--Favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('img/favicons/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('img/favicons/safari-pinned-tab.svg') }}" color="#a0830b">
    <meta name="theme-color" content="#ffffff">
    <!-- stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>

<body class="error-page">
    <div class="container-fluid">
        <nav class="nav-404">
            <a href="{{ url('/') }}">
                <img src="{{ asset('img/Volition-logo.svg') }}" alt="Volition logo" class="header--logo">
            </a>
        </nav>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 box-404">
                <h2>Oops!</h2>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-9">
                        <p>We are so embarrassed, something went wrong. Check back later.</p>
                    </div>
                </div>
                <br>
                <a href="{{ url('/home') }}" class="btn btn-gold">Back to Home</a>
            </div>
        </div>
    </div>
</body>

</html>