@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Getting Started</h1>
    </div>
    <div class="col-xs-12 col-md-12">
        <div class="box">
            {!! $page_content !!}
        </div>
    </div>
</div>
@endsection
