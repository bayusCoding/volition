@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-md-12">
    <h1 class="sub-header">Portfolio Management</h1>
</div>
<div class="col-xs-12"><span class="card-title">Update Pool ROI Percentages Monthly</span></div>

@if(session()->has('error'))
    <div class="col-xs-12 col-md-6">
        <div class="notification-warning">{{ session()->get('error') }}</div>
    </div>
@endif
<div class="col-xs-12 col-md-12">
    <div class="row">
        <div class="box">
            {!! Form::open(['route' => ['portfolio.store'], 'method' => 'POST', 'accept-charset' => 'UTF-8']) !!}
            <div class="row parent-card-pool">
                <div class="col-xs-6 card-title-pool">
                    <div class="col-xs-12 text-left">
                        <div class="form-group form-group-default">
                            <label>Month</label>
                            @php
                                $month = \Carbon\Carbon::now()->month
                            @endphp
                            <select name="month" class="form-control" aria-label="select month">
                                <option value="1" @if($month == 1) selected @endif>January</option>
                                <option value="2" @if($month == 2) selected @endif>February</option>
                                <option value="3" @if($month == 3) selected @endif>March</option>
                                <option value="4" @if($month == 4) selected @endif>April</option>
                                <option value="5" @if($month == 5) selected @endif>May</option>
                                <option value="6" @if($month == 6) selected @endif>June</option>
                                <option value="7" @if($month == 7) selected @endif>July</option>
                                <option value="8" @if($month == 8) selected @endif>August</option>
                                <option value="9" @if($month == 9) selected @endif>September</option>
                                <option value="10" @if($month == 10) selected @endif>October</option>
                                <option value="11" @if($month == 11) selected @endif>November</option>
                                <option value="12" @if($month == 12) selected @endif>December</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 card-title-pool">
                    <div class="col-xs-12 text-left">
                        <div class="form-group form-group-default">
                            <label>Pool</label>
                            {{ Form::select('pool_id', $pools, '', ['class' => 'form-control', 'placeholder' => '', 'aria-label' => 'Select investment pool']) }}
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 card-title-pool">
                    <div class="col-xs-12 text-left">
                        <div class="form-group form-group-default">
                            <label>Change %</label>
                            <input type="text" name="change" class="form-control" aria-label="enter pool percentage growth or decline for month">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-2">
                    <div class="text-left">
                        <button type="submit" class="btn btn-gold update" aria-label="update">Update</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table class="table-pool datatable">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive">Month</th>
                            <th scope="col" aria-live="assertive">Year</th>
                            <th scope="col" aria-live="assertive">Pool</th>
                            <th scope="col" aria-live="assertive">Percentage Change</th>
                            <th scope="col" aria-live="assertive">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($portfolios) > 0)
                            @foreach($portfolios as $portfolio)
                            <tr>
                                <td data-label="Month">
                                    @php
                                        $month_no  = $portfolio->month;
                                        $date   = DateTime::createFromFormat('!m', $month_no);
                                        $month_name = $date->format('F');
                                        echo $month_name;
                                    @endphp
                                </td>
                                <td data-label="Year">{{ $portfolio->year }}</td>
                                <td data-label="Pool" aria-live="assertive">{{ $portfolio->pool->name }}</td>
                                <td data-label="Percentage Change">{{ $portfolio->change }}</td>
                                <td data-label="Action" aria-live="assertive"><a href="{{ route('portfolio.edit', $portfolio->id) }}" class="btn btn-xs btn-stroke-gold">Edit</a></td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="Status" colspan="5">No Portfolio Record</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
