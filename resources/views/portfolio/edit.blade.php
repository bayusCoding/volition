@extends('layouts.app')

@section('content')
<div class="col-sm-9 col-md-12">
    <h1 class="sub-header">Portfolio Management</h1>
</div>
<div class="col-xs-12"><span class="card-title">Update Pool ROI Percentages Monthly</span></div>

@if(session()->has('error'))
    <div class="col-xs-12 col-md-6">
        <div class="notification-warning">{{ session()->get('error') }}</div>
    </div>
@endif
<div class="col-xs-12 col-md-12">
    <div class="row">
        <div class="box">
            {!! Form::open(['route' => ['portfolio.update', $portfolio->id], 'method' => 'PUT', 'accept-charset' => 'UTF-8']) !!}
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group form-group-default">
                        <label>Month</label>
                        <select name="month" class="form-control" aria-label="select month">
                            <option value="1" @if($portfolio->month == 1) selected @endif>January</option>
                            <option value="2" @if($portfolio->month == 2) selected @endif>February</option>
                            <option value="3" @if($portfolio->month == 3) selected @endif>March</option>
                            <option value="4" @if($portfolio->month == 4) selected @endif>April</option>
                            <option value="5" @if($portfolio->month == 5) selected @endif>May</option>
                            <option value="6" @if($portfolio->month == 6) selected @endif>June</option>
                            <option value="7" @if($portfolio->month == 7) selected @endif>July</option>
                            <option value="8" @if($portfolio->month == 8) selected @endif>August</option>
                            <option value="9" @if($portfolio->month == 9) selected @endif>September</option>
                            <option value="10" @if($portfolio->month == 10) selected @endif>October</option>
                            <option value="11" @if($portfolio->month == 11) selected @endif>November</option>
                            <option value="12" @if($portfolio->month == 12) selected @endif>December</option>
                        </select>
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="form-group form-group-default">
                        <label>Pool</label>
                        {{ Form::select('pool_id', $pools, $portfolio->pool_id, ['class' => 'form-control', 'placeholder' => '', 'aria-label' => 'Select investment pool']) }}
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="form-group form-group-default">
                        <label>Change %</label>
                        <input type="text" name="change" class="form-control" value="{{ $portfolio->change or old('change') }}" aria-label="enter pool percentage growth or decline for month">
                    </div>
                </div>

                <div class="col-xs-12">
                    <button type="submit" class="btn btn-gold update" aria-label="update">Update</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
