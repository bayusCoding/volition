@extends('layouts.app')

@section('content')

@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-xs-12">
        <h1 class="sub-header">Settings</h1>
    </div>
</div>

@if(session()->has('success'))
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="notification-success">{{ session()->get('success') }}</div>
    </div>
</div>
@endif
@if(session()->has('error'))
<div class="row">
    <div class="col-xs-12 col-md-6">
        <div class="notification-warning">{{ session()->get('error') }}</div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-xs-12 col-md-6">
        <span class="card-title">Account</span>
        <div class="box">
            {!! Form::open(['route' => ['settings.password'], 'method' => 'POST']) !!}
                <div class="form-group form-group-default">
                    <label>Old Password</label>
                    <input type="password" name="old_password" class="form-control" aria-label="enter your password">
                </div>
                <div class="form-group form-group-default">
                    <label>New Password</label>
                    <input type="password" name="password" class="form-control" aria-label="enter your password">
                </div>
                <div class="form-group form-group-default">
                    <label>Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" aria-label="confirm your password">
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-gold center-block" aria-label="save changes">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <span class="card-title">Pool</span>
        <div class="box">
            {!! Form::open(['route' => ['settings.pool'], 'method' => 'POST']) !!}
                <div class="form-group form-group-default">
                    <label>Select Investment Pool<sup>*</sup></label>
                    {{ Form::select('pool', $pools, null,['id' => 'pool', 'placeholder' => '','class' => 'form-control', 'aria-label' => 'Select investment pool']) }}
                </div>
                <div class="form-group form-group-default">
                    <label>Admin Fee</label>
                    <input class="form-control" aria-label="enter admin fee for selected pool" name="admin_fees" id="adminFees" type="text">
                </div>
                <div class="form-group form-group-default">
                    <label>Minimum amount per slot<sup>*</sup></label>
                    <input class="form-control" aria-label="enter minimum amount per slot for selected pool" name="upgrade" id="upgrade" type="text">
                </div>
                <div class="form-group form-group-default">
                    <label>Duration<sup>*</sup></label>
                    <input class="form-control" aria-label="enter durationor selected pool" name="expiry" id="expiry" type="text">
                </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-gold center-block" aria-label="save changes">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <span class="card-title">General</span>
        <div class="box">
            {!! Form::open(['route' => ['settings.update'], 'method' => 'POST']) !!}
                <div class="form-group form-group-default">
                    <label>Page Limit</label>
                    {{ Form::select('page_limit', ['5'=>'5', '10'=>'10', '15'=>'15', '25'=>'25', '50'=>'50', '100'=>'100' ], null,['class' => 'form-control', 'aria-label' => 'Select page limit']) }}
                </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-gold center-block" aria-label="save changes">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Account</h1>
    </div>

    <div class="col-xs-12"><span class="card-title">View and Edit Your Profile</span></div>

    <!-- Notification -->
    @if(session()->has('success'))
        <div class="col-xs-12 col-md-6">
            <div class="notification-success">{{ session()->get('success') }}</div>
        </div>
    @endif
    @if(session()->has('error'))
        <div class="col-xs-12 col-md-6">
            <div class="notification-warning">{{ session()->get('error') }}</div>
        </div>
    @endif

    <!-- User Profile -->
    <div id="user-details-info" class="col-xs-12 col-md-6">
        <div class="table-box">
            <dl class="horizontal-dl-2">
                <dt>First Name</dt>
                <dd>{{ auth()->user()->firstname }}</dd>
                <dt>Last name</dt>
                <dd>{{ auth()->user()->lastname }}</dd>
                <dt>Phone number</dt>
                <dd>{{ auth()->user()->phone }}</dd>
                <dt>email</dt>
                <dd class="email-dd">{{ auth()->user()->email }}</dd>
                <dt>password</dt>
                <dd class="password-dd">password</dd>
            </dl>
            <div class="row">
                <!-- <div class="col-md-6">
                    <div class="form-group">
                        <button id="deleteText" type="button" class="btn btn-stroke-gold center-block" aria-label="Edit Profile">Delete Account</button>
                    </div>
                </div> -->
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="button" id="edit-profile" class="btn btn-stroke-gold center-block" aria-label="Edit Profile">Edit Profile</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::model(auth()->user(), ['id' => 'user-details-form', 'route' => ['settings.user'], 'method' => 'POST', 'style' => 'display:none;']) !!}
    <div class="col-xs-12 col-md-6">
        <div class="box">
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>First Name</label>
                    <input type="text" class="form-control" aria-label="enter your First Name" name="firstname" value="{{ auth()->user()->firstname }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Last Name</label>
                    <input type="text" class="form-control" aria-label="enter your Last Name" name="lastname" value="{{ auth()->user()->lastname }}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group form-group-default">
                    <label>Phone number</label>
                    <input type="text" class="form-control" aria-label="enter your Phone number" name="phone" value="{{ auth()->user()->phone }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Gender</label>
                    {{ Form::select('gender', ['male' => 'Male', 'female' => 'Female'], null, ['class' => 'form-control', 'placeholder' => '', 'aria-label' => 'select your gender']) }}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Date of Birth</label>
                    <input type="date" class="form-control" aria-label="enter your date of birth" name="dob" value="{{ auth()->user()->dob }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Address</label>
                    <input type="text" class="form-control" aria-label="enter your address" name="city" value="{{ auth()->user()->city }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Bank Name</label>
                    <input type="text" class="form-control" aria-label="confirm your password" name="account_bank" value="{{ auth()->user()->account_bank }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Bank Account Number</label>
                    <input type="text" class="form-control" aria-label="bank account number" name="account_number" value="{{ auth()->user()->account_number }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Bank Account Name</label>
                    <input type="text" class="form-control" aria-label="bank account name" name="account_name" value="{{ auth()->user()->account_name }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>BVN</label>
                    <input type="text" class="form-control" aria-label="confirm your password" name="account_bvn" value="{{ auth()->user()->account_bvn }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Occupation</label>
                    <input type="text" class="form-control" aria-label="occupation" name="occupation" value="{{ auth()->user()->occupation }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Industry</label>
                    <input type="text" class="form-control" aria-label="occupation industry" name="industry" value="{{ auth()->user()->industry }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Next of Kin Name</label>
                    <input type="text" class="form-control" aria-label="enter your next of kin name" name="nok_name" value="{{ auth()->user()->nok_name }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Next of Kin Email Address</label>
                    <input type="email" class="form-control" aria-label="enter your next of kin email" name="nok_email" value="{{ auth()->user()->nok_email }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Next of Kin Mobile Number</label>
                    <input type="text" class="form-control" aria-label="enter your next of kin mobile number" name="nok_phone" value="{{ auth()->user()->nok_phone }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group form-group-default">
                    <label>Next of Kin Address</label>
                    <input type="text" class="form-control" aria-label="enter your next of kin address" name="nok_address" value="{{ auth()->user()->nok_address }}">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-gold center-block" aria-label="save changes">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <!-- User password update -->
    <div class="col-xs-12 col-md-6">
        <div class="box">
            {!! Form::open(['route' => ['settings.password'], 'method' => 'POST']) !!}
                <div class="form-group form-group-default">
                    @if ($errors->has('old_password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('old_password') }}</strong>
                        </span>
                    @endif
                    <label>Old Password</label>
                    <input type="password" name="old_password" class="form-control" aria-label="enter your password">
                </div>
                <div class="form-group form-group-default">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    <label>New Password</label>
                    <input type="password" name="password" class="form-control" aria-label="enter your password">
                </div>
                <div class="form-group form-group-default">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                    <label>Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" aria-label="confirm your password">
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-gold center-block" aria-label="save changes">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif

@endsection
