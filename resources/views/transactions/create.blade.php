@extends('layouts.app')

@section('content')
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Transaction</h1>
    </div>
    <div class="col-xs-12"> <span class="card-title">Manage Transaction Details</span></div>

    @include('partials.notification')

    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="box">
                {!! Form::open(['route' => 'transactions.store', 'method' => 'POST', 'accept-charset' => 'UTF-8']) !!}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group form-group-default">
                            <label>Amount</label>
                            <input type="text" name="amount" class="form-control" value="{{ old('amount') }}" aria-label="enter pnew transaction amount">
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group form-group-default">
                            <label>User<sub>*</sub></label>
                            {{ Form::select('user', $users, ' ', ['id' => 'users', 'class' => 'form-control', 'placeholder' => ' ', 'aria-label' => 'select user to create transaction for']) }}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group form-group-default">
                            <label>Type</label>
                            {{ Form::select('type', [1 => 'Credit', 0 => 'Debit'], '', ['id' => 'type', 'class' => 'form-control', 'placeholder' => '', 'aria-label' => 'select transaction type']) }}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group form-group-default">
                            <label>Subscription</label>
                            {{ Form::select('subscription_id', $subscriptions, ' ', ['id' => 'subscriptionId', 'class' => 'form-control', 'placeholder' => ' ', 'aria-label' => 'select subscription']) }}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group form-group-default">
                            <label>Description</label>
                            <textarea name="trans_desc" class="form-control" aria-label="enter transaction description">{{ old('trans_desc') }}</textarea>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-gold update" aria-label="update">Save</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
