@extends('layouts.app')

@section('content')
<style>
      #dataTable span{
        display:none;
      }
    </style>
<div class="row">
    <div class="col-sm-12 col-md-4">
        <h1 class="sub-header">Transaction History</h1>
    </div>
    <div class="col-xs-12 col-sm-3 col-md-2">@if(Auth::user()->isAdmin)<a href="{{ route('transactions.create') }}" class="btn btn-lg btn-gold" style="margin-top:12px;">New Transaction</a>@endif</div>
</div>
<!-- Second section -->
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">Monitor Your Transactions</span>
                <br>
                <a 
                  href="{{ URL::route('data/download/transaction_data') }}"
                  style=" margin-bottom:-10px">
                    <button style="padding:7px; margin-bottom:-10px" class="btn btn-gold"> CSV </button>
                </a>
            </div>
        </div>

        @include('partials.notification')

        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table id="table">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive">Date</th>
                            <th scope="col" aria-live="assertive">Reference</th>
                            <th scope="col" aria-live="assertive">Description</th>
                            <th scope="col" aria-live="assertive">Amount</th>
                            @if(Auth::user()->isAdmin)
                            <th scope="col" aria-live="assertive">Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($transactions))
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td data-label="Date"><p>{{ $transaction->created_at->format('j M Y') }}</p></td>
                                    <td data-label="Reference">{{ $transaction->trans_ref }}</td>
                                    <td data-label="Description">{{ $transaction->trans_desc }}</td>
                                    <td data-label="amount" @if($transaction->trans_type == 0) class="green-text" @else class="red-text" @endif aria-live="assertive">₦{{ number_format($transaction->amount) }}</td>
                                    @if(Auth::user()->isAdmin)
                                    <td data-label="Action" aria-live="assertive">
                                        <a href="{{ route('transactions.edit', $transaction->id) }}" class="btn btn-xs btn-stroke-gold">Edit</a>
                                        <form action="{{ route('transactions.destroy', $transaction->id) }}" method="post" id="deleteTransactionForm{{ $transaction->id }}" style="display:inline-block;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <button onclick="return ConfirmDelete()" type="submit" form="deleteTransactionForm{{ $transaction->id }}" class="btn btn-xs btn-stroke-red" style="margin-top:0">Delete</button>
                                        </form>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="Status" colspan="4">You've no transactions to view</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function ConfirmDelete() {
            return confirm("Are you sure you want to delete?");
        }

    $(document).ready(function() {
        $('#table').DataTable({
            "pageLength": 15,
            'columnDefs': [
              { type: 'html-num', targets: 0},
              { type: 'currency', targets: 3} 
            ]
        });
    });
    </script>
</div>

@endsection
