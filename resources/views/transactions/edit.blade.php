@extends('layouts.app')

@section('content')
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Transaction</h1>
    </div>
    <div class="col-xs-12"><span class="card-title">Manage Transaction Details</span></div>

    @include('partials.notification')

    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="box">
                {!! Form::open(['route' => ['transactions.update', $transaction->id], 'method' => 'PUT', 'accept-charset' => 'UTF-8']) !!}
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group form-group-default">
                            <label>Amount</label>
                            <input type="text" name="amount" class="form-control" value="{{ $transaction->amount or old('amount') }}" aria-label="enter pnew transaction amount">
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group form-group-default">
                            <label>Description</label>
                            <textarea name="trans_desc" class="form-control" aria-label="enter transaction description">{{ $transaction->trans_desc or old('trans_desc') }}</textarea>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-gold update" aria-label="update">Update</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
