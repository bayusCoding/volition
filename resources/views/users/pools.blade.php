@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $user->firstname }} {{ $user->lastname }} - Pool Subscriptions</h1>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">View all pools</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table class="datatable">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive" width="15%">Date</th>
                            <th scope="col" aria-live="assertive">User</th>
                            <th scope="col" aria-live="assertive">Pool</th>
                            <th scope="col" aria-live="assertive">Amount</th>
                            <th scope="col" aria-live="assertive" style="width: 30%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($subscriptions) > 0)
                            @foreach($subscriptions as $subscription)
                                <tr>
                                    <td data-label="Date">{{ $subscription->created_at->toDateString() }}</td>
                                    <td data-label="User">{{ $subscription->user->firstname }} {{ $subscription->user->lastname }}</td>
                                    <td data-label="Pool">{{ $subscription->pool->name }}</td>
                                    <td data-label="Amount">
                                        @php if(!is_null($subscription->subscription_fee)) { echo '₦'.number_format($subscription->subscription_fee); } else { echo 'n/a'; } @endphp
                                    </td>
                                    <td data-label="Action" aria-live="assertive">
                                        <a href="{{ route('investment.show', $subscription->subscription_number) }}" class="btn btn-xs btn-stroke-gold">View</a>
                                        <a href="{{ route('users.pools.edit', $subscription->id) }}" class="btn btn-xs btn-stroke-gold">Edit</a>
                                        <form action="{{ route('users.pools.destroy', $subscription->id) }}" method="post" id="deleteSubscriptionForm{{ $subscription->id }}" style="display:inline-block;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="user" value="{{ $user->id }}">
                                            <button onclick="return ConfirmDelete()" type="submit" form="deleteSubscriptionForm{{ $subscription->id }}" class="btn btn-xs btn-stroke-red" style="margin-top:0">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="status" colspan="5">No pool subscription</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        {{ $subscriptions->links() }}
    </div>
</div>
<script>
    function ConfirmDelete() {
        return confirm("Are you sure you want to delete?");
    }
</script>
@endsection

