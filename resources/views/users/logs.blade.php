@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $user->firstname }} {{ $user->lastname }} - Logs</h1>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">Track all actions</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table class="datatable">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive" width="20%">Date</th>
                            <th scope="col" aria-live="assertive" width="20%">Time</th>
                            <th scope="col" aria-live="assertive">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($logs) > 0)
                            @foreach($logs as $log)
                                <tr>
                                    <td data-label="Date">{{ $log->created_at->format('j M Y') }}</td>
                                    <td data-label="time">{{ $log->created_at->toTimeString() }}</td>
                                    <td data-label="description" aria-live="assertive">
                                        @if(Auth::user()->isAdmin) {{ $log->user->firstname }} {{ $log->user->lastname }} @else You @endif
                                        {{ $log->log_desc }}
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="status" colspan="3">No activity log</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        {{ $logs->links() }}
    </div>
</div>
@endsection
