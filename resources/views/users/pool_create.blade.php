@extends('layouts.app')

@section('content')
<style>
    #money{
        display: none;
    }
</style>
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $user->firstname }} {{ $user->lastname }} - Subscription </h1>
    </div>
</div>

<div class="row">
    {!! Form::open(['route' => ['users.pools.store'], 'method' => 'POST', 'accept-charset' => 'UTF-8', 'id' => 'poolAdditionForm']) !!}
    <input type="hidden" name="user" value="{{ $user->id }}">
    <div class="col-xs-12 col-md-6">
        <div class="box">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-default">
                        <label>Select Investment Pool</label>
                        {{ Form::select('pool_id', $pools, null, ['id' => 'pool', 'class' => 'form-control', 'required' => 'required', 'aria-label' => 'Select investment pool']) }}
                    </div>
                </div>
                <div id="let" class="col-md-6">
                    <div class="form-group form-group-default">
                        <label>Number of slots<sub>*</sub></label>
                        <select name="slots" class="form-control" id="slots" aria-label="enter number of slots">
                            <option selected value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                        </select>
                    </div>
                </div>
            </div>



            <div id="money">
                <div class="row" id="black">
                    <div class="col-md-6">
                        <div class="form-group form-group-default">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Green: ₦</span>
                            </div>
                            <input name="{{ $green_plus->slug }}" type="number" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="ecol-md-1"> </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-default">
                            <label for="green_month">Months</label>
                            <select name="green_month" class="form-control" id="green_month" aria-label="enter number of months">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option selected="selected" value="12">12</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-default">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Black: ₦</span>
                            </div>
                            <input name="{{ $black_plus->slug }}" type="number" class="form-control" />
                        </div>

                    </div>
                    <div class="ecol-md-1"> </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-default">
                            <label for="black_month">Months</label>
                            <select name="black_month" class="form-control" id="black_month" aria-label="enter number of months">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option selected="selected" value="12">12</option>
                            </select>
                        </div>
                    </div>
                </div>


                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-default">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Red: ₦</span>
                            </div>
                            <input name="{{ $red_plus->slug }}" type="number" class="form-control" />
                        </div>
                    </div>

                    <div class="ecol-md-1"> </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-default">
                            <label for="red_month">Months</label>
                            <select name="red_month" class="form-control" id="red_month" aria-label="enter number of months">
                                <option value="0"></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option selected="selected" value="12">12</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>




            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-default">
                        <label>Start</label>
                        {{ Form::date('start_date', \Carbon\Carbon::now(), ['class' => 'form-control']) }} 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-default">
                        <label>End</label>
                       {{ Form::date('end_date', \Carbon\Carbon::now(), ['class' => 'form-control']) }} 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="radio">
                        <input id="adminFeeCheck" name="admin_fees_inc"  value="1" type="checkbox">
                        <label for="adminFeeCheck" class="radio-label">Admin Fee Included in Payment</label>
                    </div>
                    <div id="hidden_form"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button id="pay" type="submit" class="btn btn-gold center-block" aria-label="save changes">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<script>
    $('select').first().change(function(){
        if($('select').first().val() == 5 || $('select').first().val() == 6 ){
            $('#let').hide();
            $('#money').show();
        } else {
            $('#let').show();
            $('#money').hide();
        }
    });

    $("select").first().children('option').eq(6).hide();
    $("select").first().children('option').eq(7).hide();
    $("select").first().children('option').eq(8).hide();
    

</script>

<script>
    $(document).ready(function() {
        var green_amount = <?php echo $green_plus->upgrade; ?>;
        var red_amount = <?php echo $red_plus->upgrade; ?>;
        var black_amount = <?php echo $black_plus->upgrade; ?>;
        var green_value = 0;
        var red_value = 0;
        var black_value = 0;
        var total = 0;

        (function () {
            document.getElementById('green').addEventListener('change', () => {
                green_value = document.getElementById('green').value;
                console.log(green_value);
                compute();
            }, false);

            document.getElementById('red').addEventListener('change', () => {
                red_value = document.getElementById('red').value;
                console.log(red_value);
                compute();
            }, false);

            document.getElementById('black').addEventListener('change', () => {
                black_value = document.getElementById('black').value;
                console.log(black_value);
                compute();
            }, false);


            function compute () {
                var green = green_amount * green_value;
                var red = red_amount * red_value;
                var black = black_amount * black_value;

                total = green + red + black;

                console.log(total);
                createHiddenInput(total);
            }

            function createHiddenInput (total) {
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("id", "hidden");
                input.setAttribute("name", "upgrade");
                input.setAttribute("value", total);

                $("#hidden_form").html(input);
            }
                                      
        })();
    });
</script>

@stop
