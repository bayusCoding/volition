@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-9 col-md-12">
            <h1 class="sub-header">{{ $user->firstname }} {{ $user->lastname }}</h1>
            <form action="{{ route('users.destroy', $user->id) }}" method="post" id="deleteUserForm">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
            </form>
            <button onclick="return ConfirmDelete()" type="submit" form="deleteUserForm" class="btn-gold-xs pull-right" style="margin-right:10px;">Archive User</button>
        </div>
    </div>

    <!-- User Profile -->
    <div class="row"><div class="col-xs-12"><span class="card-title">User Profile</span></div></div>
    <div class="row">
        <div id="user-details-info" class="col-xs-12 col-md-6">
            <div class="table-box">
                <dl class="horizontal-dl-2">
                    <dt>First Name</dt>
                    <dd>{{ $user->firstname }}</dd>
                    <dt>Last name</dt>
                    <dd>{{ $user->lastname }}</dd>
                    <dt>Gender</dt>
                    <dd>{{ $user->gender }}</dd>
                    <dt>Occupation</dt>
                    <dd>{{ $user->occupation }}</dd>
                    <dt>Industry</dt>
                    <dd>{{ $user->industry }}</dd>
                    <dt>Phone number</dt>
                    <dd>{{ $user->phone }}</dd>
                    <dt>Email</dt>
                    <dd class="email-dd">{{ $user->email }}</dd>
                    <dt>Address</dt>
                    <dd>{{ $user->city }}</dd>
                    @if($user->isReferred)
                        <dt>Referred by</dt>
                        <dd><a href="{{ url('users/'.$ref_user_id) }}">{{ $ref_full_name }}</a></dd>
                    @endif
                </dl>
            </div>
        </div>
        <div id="user-details-info" class="col-xs-12 col-md-6">
            <div class="table-box">
                <dl class="horizontal-dl-2">
                    <dt>Bank Name</dt>
                    <dd>{{ $user->account_bank }}</dd>
                    <dt>Account Number</dt>
                    <dd>{{ $user->account_number }}</dd>
                    <dt>Account Name</dt>
                    <dd>{{ $user->account_name }}</dd>
                    <dt>BVN</dt>
                    <dd>{{ $user->account_bvn }}</dd>
                    <dt>Next of Kin Name</dt>
                    <dd>{{ $user->nok_name }}</dd>
                    <dt>Next of Kin Email Address</dt>
                    <dd class="email-dd">{{ $user->nok_email }}</dd>
                    <dt>Next of Kin Phone Number</dt>
                    <dd>{{ $user->nok_phone }}</dd>
                    <dt>Next of Kin Address</dt>
                    <dd>{{ $user->nok_address }}</dd>
                </dl>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <span class="card-title">Pool</span>
            <a href="{{ route('users.pools', $user->id) }}" class="btn-gold-xs pull-right">View more</a>
            <a href="{{ route('users.pools.create', $user->id) }}" class="btn-gold-xs pull-right" style="margin-right:10px;">Add Subscription</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p class="desc-text">Volition Green</p>
                        <h1 class="green-text">{{ $metadata['green_count'] }}</h1>
                        <p class="desc-text">Total subscriptions</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p class="desc-text">Volition Red</p>
                        <h1 class="red-text">{{ $metadata['red_count'] }}</h1>
                        <p class="desc-text">Total subscriptions</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p class="desc-text">Volition Black</p>
                        <h1 class="black-text">{{ $metadata['black_count'] }}</h1>
                        <p class="desc-text">Total subscriptions</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-3">
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p class="desc-text">Priviledge/Business</p>
                        <h1 class="black-text">{{ $metadata['privilege_count'] }}/{{ $metadata['business_count'] }}</h1>
                        <p class="desc-text">Total subscriptions</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <span class="card-title">Transaction History</span>
                    <a href="{{ route('users.transactions', $user->id) }}" class="btn-gold-xs pull-right">View more</a>
                </div>
            </div>
            <div class="col-md-12 table-box">
                <table>
                    <thead>
                    <tr>
                        <th scope="col" aria-live="assertive" style="width: 30%;">Date</th>
                        <th scope="col" aria-live="assertive">Description</th>
                        <th scope="col" aria-live="assertive" style="width: 30%;">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($transactions) > 0)
                        @foreach($transactions as $transaction)
                            <tr>
                                <td data-label="Date">{{ $transaction->created_at->format('j M Y') }}</td>
                                <td data-label="Description">{{ $transaction->trans_desc }}</td>
                                <td data-label="amount" @if($transaction->trans_type == 0) class="green-text" @else class="red-text" @endif aria-live="assertive">₦{{ number_format($transaction->amount) }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td data-label="status" colspan="3">There are no transactions</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="row">
                <div class="col-xs-12">
                    <span class="card-title">Recent Activity</span>
                    <a href="{{ route('users.logs', $user->id) }}" class="btn-gold-xs pull-right">View more</a>
                </div>
            </div>
            <div class="col-md-12 table-box">
                <table>
                    <thead>
                    <tr>
                        <th scope="col" aria-live="assertive" style="width: 20%;">Date</th>
                        <th scope="col" aria-live="assertive">Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($logs) > 0)
                        @foreach($logs as $log)
                            <tr>
                                <td data-label="Date">{{ $log->created_at->format('j M Y') }}</td>
                                <td data-label="Description">
                                    @if(Auth::user()->isAdmin) {{ $log->user->firstname }} {{ $log->user->lastname }} @else You @endif
                                    {{ $log->log_desc }}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td data-label="status" colspan="3">There are no activity log</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row"><div class="col-xs-12"><span class="card-title">Referral</span></div></div>
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="black-text">{{ $metadata['referral_count'] }}</h1>
                        <p class="desc-text">Referral Made</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h1 class="black-text">₦{{ $wallet->balance }}</h1>
                        <p class="desc-text">Total Referral Income</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <span class="card-title">Referral</span>
            <a href="{{ route('users.referrals', $user->id) }}" class="btn-gold-xs pull-right">View more</a>
        </div>
    </div>
    <div class="col-md-12 table-box">
        <table>
            <thead>
            <tr>
                <th scope="col" aria-live="assertive">Date</th>
                <th scope="col" aria-live="assertive">Email</th>
                <th scope="col" aria-live="assertive">Referrer</th>
                <th scope="col" aria-live="assertive">Referral Code</th>
            </tr>
            </thead>
            <tbody>
            @if(count($referrals) > 0)
                @foreach($referrals as $referral)
                    <tr>
                        <td data-label="Date">{{ $referral->created_at->format('j M Y') }}</td>
                        <td data-label="Email">{{ $referral->email }}</td>
                        <td data-label="Referrer">{{ $referral->user->firstname }} {{ $referral->user->lastname }}</td>
                        <td data-label="Referral Code" aria-live="assertive">{{ $referral->referral_code }}</td>
                    </tr>
                @endforeach
            @else
                <tr><td data-label="Status" colspan="3">No referrals have been made</td></tr>
            @endif
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="row"><div class="col-md-12"><span class="card-title">Recent Messages</span></div></div>
            <div class="box">
                <div class="row">
                    <div class="col-xs-12 col-md-12" aria-live="assertive">
                        @if(count($messages) > 0)
                            @foreach($messages as $message)
                                @if($message->type == 0)
                                    <div class="chat-col">
                                        <div class="customer-chat-info">
                                            <span class="customer">You</span>&nbsp;&nbsp;<span class="date-time">{{ $message->created_at->diffForHumans() }}</span>
                                        </div>
                                        <div class="chat-content">{{ $message->body }}</div>
                                    </div>
                                @else
                                    <div class="support-chat-col">
                                        <div class="customer-chat-info">
                                            <span class="support">Support</span>&nbsp;&nbsp;<span class="date-time">{{ $message->created_at->diffForHumans() }}</span>
                                        </div>
                                        <div class="chat-content">{{ $message->body }}</div>
                                    </div>
                                @endif
                            @endforeach
                        @endif

                        <div class="row">
                            <div class="col-xs-12 col-md-12 text-center">
                                <a href="{{ route('messages.show', ['to' => $user->id]) }}" class="btn btn-gold" aria-label="view all user message">View All</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function ConfirmDelete() {
                return confirm("Are you sure you want to archive this user?");
            }
        </script>
@endsection
