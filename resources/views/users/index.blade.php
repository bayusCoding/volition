@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Users</h1>
    </div>
</div>
<!-- Second section -->
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">View list of all registered users</span>
                <br>
                <a 
                  href="{{ URL::route('data/download/user_data') }}"
                  style=" margin-bottom:-10px">
                    <button style="padding:7px; margin-bottom:-10px" class="btn btn-gold"> CSV </button>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table id="table" style="margin-top: 10px">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive">Joined</th>
                            <th scope="col" aria-live="assertive">Name</th>
                            <th scope="col" aria-live="assertive">Email</th>
                            <th scope="col" aria-live="assertive">Mobile No</th>
                            <th scope="col" aria-live="assertive">Subscription</th>
                            <th scope="col" aria-live="assertive">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($users))
                            @foreach($users as $user)
                                <tr>
                                    <td data-label="Joined"><p>{{ $user->created_at->format('j M Y') }}</p></td>
                                    <td data-label="Name">{{ $user->firstname }} {{ $user->lastname }}</td>
                                    <td data-label="Email">{{ $user->email }}</td>
                                    <td data-label="Mobile No" aria-live="assertive">{{ $user->phone }}</td>
                                    <td data-label="Subscription" aria-live="assertive">{{ $user->activeSubscription }}</td>
                                    <td data-label="Action" aria-live="assertive"><a href="{{ route('users.show', $user->id) }}" class="btn btn-xs btn-stroke-gold">View details</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="Status" colspan="5">No registered users found.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div id="search" class="search-wrapper">
                <form action="{{ route('users.index') }}" method="GET">
                    <label>
                        <input type="search" name="q" placeholder="Search Users" aria-controls="datatable">
                    </label>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                "pageLength": 15,
                'columnDefs': [
                  { type: 'html-num', targets: 0}
                ]
            });
        });
    </script>
</div>
@endsection
