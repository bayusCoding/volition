@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $user->firstname }} {{ $user->lastname }} - Transactions</h1>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">View all Transactions</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table class="datatable">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive">Date</th>
                            <th scope="col" aria-live="assertive">Reference</th>
                            <th scope="col" aria-live="assertive">Description</th>
                            <th scope="col" aria-live="assertive">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($transactions) > 0)
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td data-label="Date">{{ $transaction->created_at->format('j M Y') }}</td>
                                    <td data-label="Reference">{{ $transaction->trans_ref }}</td>
                                    <td data-label="Description">{{ $transaction->trans_desc }}</td>
                                    <td data-label="amount" @if($transaction->trans_type == 0) class="green-text" @else class="red-text" @endif aria-live="assertive">₦{{ number_format($transaction->amount) }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="Status" colspan="4">You've no transactions to view</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        {{ $transactions->links() }}
    </div>
</div>
@endsection
