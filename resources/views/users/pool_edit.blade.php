@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $user->firstname }} {{ $user->lastname }} - Subscription </h1>
    </div>
</div>

<div class="row">
    {!! Form::open(['route' => ['users.pools.update', $subscription->id], 'method' => 'PUT', 'accept-charset' => 'UTF-8', 'id' => 'poolUpdateForm']) !!}
    <input type="hidden" name="user" value="{{ $subscription->user_id }}">
    <div class="col-xs-12 col-md-6">
        <div class="box">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-default">
                        <label>Select Investment Pool</label>
                        {{ Form::select('pool_id', $pools, $subscription->pool_id, ['id' => 'pool', 'class' => 'form-control', 'required' => 'required', 'aria-label' => 'Select investment pool']) }}
                    </div>
                </div>
            </div>

                @if($subscription->pool_id == 5 || $subscription->pool_id == 6)

                @php
                    $child_pool_slot = explode('+', $subscription->child_pool_slot);
                    $child_pool_duration = explode('+', $subscription->child_pool_slot_duration);

                @endphp

                    <div id="money">
                        <div class="row" id="black">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Green: ₦</span>
                                    </div>
                                    <input value="{{ $child_pool_slot[0] * $green_plus->upgrade }}" name="{{ $green_plus->slug }}" type="number" class="form-control" />
                                </div>
                            </div>

                            <div class="ecol-md-1"> </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="green_month">Months</label>
                                    <input value="{{ $child_pool_duration[0] }}" name="green_month" type="number" class="form-control" />
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Black: ₦</span>
                                    </div>
                                    <input value="{{ $child_pool_slot[2] * $black_plus->upgrade }}" name="{{ $black_plus->slug }}" type="number" class="form-control" />
                                </div>

                            </div>
                            <div class="ecol-md-1"> </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="black_month">Months</label>
                                    <input value="{{ $child_pool_duration[2] }}" name="black_month" type="number" class="form-control" />
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Red: ₦</span>
                                    </div>
                                    <input value="{{ $child_pool_slot[1] * $red_plus->upgrade }}" name="{{ $red_plus->slug }}" type="number" class="form-control" />
                                </div>
                            </div>

                            <div class="ecol-md-1"> </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="red_month">Months</label>
                                    <input value="{{ $child_pool_duration[1] }}" name="red_month" type="number" class="form-control" />
                                </div>
                            </div>
                        </div>
                    </div>

                    @else

                <div class="col-md-6">
                    <div class="form-group form-group-default">
                        <label>Number of slots<sub>*</sub></label>
                        <select name="slots" class="form-control" id="slots" aria-label="enter number of slots">
                            <option value="1" @if($subscription->slots == 1) selected @endif>1</option>
                            <option value="2" @if($subscription->slots == 2) selected @endif>2</option>
                            <option value="3" @if($subscription->slots == 3) selected @endif>3</option>
                            <option value="4" @if($subscription->slots == 4) selected @endif>4</option>
                            <option value="5" @if($subscription->slots == 5) selected @endif>5</option>
                            <option value="6" @if($subscription->slots == 6) selected @endif>6</option>
                            <option value="7" @if($subscription->slots == 7) selected @endif>7</option>
                            <option value="8" @if($subscription->slots == 8) selected @endif>8</option>
                            <option value="9" @if($subscription->slots == 9) selected @endif>9</option>
                            <option value="10" @if($subscription->slots == 10) selected @endif>10</option>
                            <option value="11" @if($subscription->slots == 11) selected @endif>11</option>
                            <option value="12" @if($subscription->slots == 12) selected @endif>12</option>
                            <option value="13" @if($subscription->slots == 13) selected @endif>13</option>
                            <option value="14" @if($subscription->slots == 14) selected @endif>14</option>
                            <option value="15" @if($subscription->slots == 15) selected @endif>15</option>
                            <option value="16" @if($subscription->slots == 16) selected @endif>16</option>
                            <option value="17" @if($subscription->slots == 17) selected @endif>17</option>
                            <option value="18" @if($subscription->slots == 18) selected @endif>18</option>
                            <option value="19" @if($subscription->slots == 19) selected @endif>19</option>
                            <option value="20" @if($subscription->slots == 20) selected @endif>20</option>
                        </select>
                    </div>
                </div>

                    @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group form-group-default">
                        <label>Start</label>
                        {{ Form::date('start_date', $subscription->start_date, ['class' => 'form-control']) }} 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-default">
                        <label>End</label>
                       {{ Form::date('end_date', $subscription->end_date, ['class' => 'form-control']) }} 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-gold center-block" aria-label="save changes">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@stop
