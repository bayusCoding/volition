@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $user->firstname }} {{ $user->lastname }} - Referrals</h1>
    </div>

    <!-- summary card -->
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>{{ count($referrals) }}</h1>
                    <p class="desc-text">Referrals Made</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1>₦{{ number_format($wallet->balance) }}</h1>
                    <p class="desc-text">Total Referral Income</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">Referral History</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table class="datatable">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive">Date</th>
                            <th scope="col" aria-live="assertive">Email</th>
                            <th scope="col" aria-live="assertive">Referral Code</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($referrals) > 0)
                            @foreach($referrals as $referral)
                                <tr>
                                    <td data-label="Date">{{ $referral->created_at->format('j M Y') }}</td>
                                    <td data-label="Email">{{ $referral->email }}</td>
                                    <td data-label="Referral Code" aria-live="assertive">{{ $referral->referral_code }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr><td data-label="Status" colspan="3">You've have made no referrals</td></tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
