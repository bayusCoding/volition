@extends('layouts.auth')

{{-- This is the page for which user with referral code to verify it and then sign up --}}

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 signup--box">
            
            @include('partials.notification')

            <h4>sign up</h4>
            <p>Enter your referral code</p>
            <form method="POST" action="{{ route('referral.verify') }}" accept-charset="UTF-8">
                {{ csrf_field() }}
                <input type="hidden" value="{{ $token or old('token') }}">
                <div class="form-group form-group-default{{ $errors->has('referral_code') ? ' has-error' : '' }}">
                    <label for="referral_code">Referral Code</label>
                    <input type="text" name="referral_code" class="form-control" value="{{ old('referral_code') }}" autofocus="autofocus" required="required" aria-label="enter your referral code">
                    @if ($errors->has('referral_code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('referral_code') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn--block--lg btn--fill" aria-label="Log in">Verify</button>
                </div>
                <p>Don't have a Referral Code? <a href="{{ route('register.request') }}">Request one</a></p>
            </form>
        </div>
    </div>
</div>
@endsection
