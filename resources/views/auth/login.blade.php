@extends('layouts.auth')

{{-- This is the page for users who are fully signed up and need to login --}}

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 signup--box">
            
            @include('partials.notification')

            <h4>Login</h4>
            <form method="POST" action="{{ url('/login') }}" accept-charset="UTF-8">
                {{ csrf_field() }}

                <div class="form-group form-group-default{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Email Address</label>
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}" id="" placeholder="" autofocus="" required="required" aria-label="enter your Email address">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group form-group-default{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="email">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="" autofocus="" required="required" aria-label="enter your Password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn--block--lg btn--fill" aria-label="Log in">Sign In</button>
                </div>
                <p>Forgotten password? <a href="{{ url('/password/reset') }}">Reset</a></p>
            </form>
        </div>
    </div>
</div>
@endsection
