@extends('layouts.auth')

{{-- This is the page for users who have received password reset token and have come to update the password --}}

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-4 signup--box">
            
            @include('partials.notification')
            
            <h4>Reset password</h4>

            <form method="POST" action="{{ route('password.request') }}" accept-charset="UTF-8"><input name="" type="hidden" value="">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group form-group-default{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="ref-no">Email Address</label>
                    <input type="email" name="email" class="form-control" id="ref-no" value="{{ $email or old('email') }}" autofocus required="require" aria-label="enter your Email address">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group form-group-default{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="email">Password</label>
                    <input type="password" name="password" class="form-control" required="required" aria-label="enter your Password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group form-group-default{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="email">Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" required="required" aria-label="confirm your Password">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn--block--lg btn--fill" aria-label="Log in">Reset Password</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
