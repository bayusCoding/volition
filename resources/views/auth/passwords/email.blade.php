@extends('layouts.auth')

{{-- This is the page for users who have forgotten their password and need to reset it. This page sends a reset token to the inputted email --}}

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-4 signup--box">
            
            @include('partials.notification')
            
            <h4>Reset password</h4>

            <form method="POST" action="{{ route('password.email') }}" accept-charset="UTF-8">
                {{ csrf_field() }}
                <div class="form-group form-group-default{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="ref-no">Email Address</label>
                    <input type="email" name="email" class="form-control" id="email" value="{{ old('email') }}" placeholder="" autofocus="" required="required" aria-label="enter your Email address">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn--block--lg btn--fill" aria-label="Log in">Request Reset</button>
                </div>
                <p>Remember your password? <a href="{{ url('/login') }}">Login</a></p>
            </form>
        </div>
    </div>
</div>
@endsection
