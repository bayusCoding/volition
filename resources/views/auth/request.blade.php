@extends('layouts.auth')

{{-- This is the page for which user who don't have a referral can request for one from admin --}}

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 signup--box">
            
            @include('partials.notification')

            <h4>Referral Request</h4>
            <p>Fill the form to request for a code</p>
            <form method="POST" action="{{ route('request.store') }}" accept-charset="UTF-8">
                {{ csrf_field() }}

                <div class="form-group form-group-default{{ $errors->has('firstname') ? ' has-error' : '' }}">
                    <label for="firstname">First Name</label>
                    <input type="text" name="firstname" class="form-control" id="firstname" value="{{ old('firstname') }}" autofocus="autofocus" required="required" aria-label="enter your firstname">
                    @if ($errors->has('firstname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group form-group-default{{ $errors->has('lastname') ? ' has-error' : '' }}">
                    <label for="lastname">Last Name</label>
                    <input type="text" name="lastname" class="form-control" id="lastname" value="{{ old('lastname') }}" required="required" aria-label="enter your lastname">
                    @if ($errors->has('lastname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group form-group-default{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Email Address</label>
                    <input type="email" name="email" class="form-control" id="email" value="{{ old('email') }}" required="required" aria-label="enter your email address">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group form-group-default{{ $errors->has('phone') ? ' has-error' : '' }}">
                    <label for="phone">Phone Number</label>
                    <input type="text" name="phone" class="form-control" id="phone" value="{{ old('phone') }}" required="required" aria-label="enter your phone number">
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group form-group-default{{ $errors->has('how') ? ' has-error' : '' }}">
                    <label for="how">How did you learn about Volition?</label>
                    <input type="text" name="how" class="form-control" id="how" value="{{ old('how') }}" required="required" aria-label="how did you get to learn about Volition">
                    @if ($errors->has('how'))
                        <span class="help-block">
                            <strong>{{ $errors->first('how') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn--block--lg btn--fill" aria-label="Submit Request">Submit Request</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
