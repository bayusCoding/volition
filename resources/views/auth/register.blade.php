@extends('layouts.auth')

{{-- This is the page for which user with refferal code that have been verified come to sign up --}}

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 signup--box">

            @include('partials.notification')

            <h4>sign up</h4>
            <form method="POST" action="{{ route('register') }}" accept-charset="UTF-8">
                <input name="referral_code" type="hidden" value="{{ $referral_code or old('referral_code') }}">
                {{ csrf_field() }}

                <div class="form-group form-group-default{{ $errors->has('firstname') ? ' has-error' : '' }}">
                    <label for="email">First Name</label>
                    <input type="text" name="firstname" class="form-control" id="firstname" value="{{ old('firstname') }}" required="required" aria-label="enter your First Name">
                    @if ($errors->has('firstname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('firstname') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group form-group-default{{ $errors->has('lastname') ? ' has-error' : '' }}">
                    <label for="email">Last Name</label>
                    <input type="text" name="lastname" class="form-control" id="lastname" value="{{ old('lastname') }}" required="required" aria-label="enter your Last Name">
                    @if ($errors->has('lastname'))
                        <span class="help-block">
                            <strong>{{ $errors->first('lastname') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group form-group-default{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Email Address</label>
                    <input type="email" name="email" class="form-control" id="email" value="{{ old('email') }}" required="required" aria-label="enter your Email address">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group form-group-default{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="email">Password</label>
                    <input type="password" name="password" class="form-control" id="password" required="required" aria-label="enter your Password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group form-group-default{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="email">Confirm Password</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password-confirmation" required="required" aria-label="confirm your Password">
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="text-left" style="padding:0px 32px;">
                    <input id="agree" type="checkbox">
                    <label class="checkbox-label">Agree to our <a href="{{ url('terms-and-conditions') }}">Terms and Conditions</a></label>
                </div>
                <div class="form-group">
                    <button type="submit" id="pay" class="btn btn--block--lg btn--fill" disabled="disabled" aria-label="Sign up">Sign Up</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
