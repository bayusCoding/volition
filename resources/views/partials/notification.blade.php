@if(Session::has('success'))
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="notification-success">{!! session('success') !!}</div>
        </div>
    </div>
@elseif(Session::has('warning'))
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="notification-warning">{!! session('warning') !!}</div>
        </div>
    </div>
@elseif(Session::has('error'))
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="notification-error">{!! session('error') !!}</div>
        </div>
    </div>
@endif