<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset=" UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Get Financial Rewards. Joint Investment Opportunities. | Volition Capital Dashboard</title>
    <!-- Search Engine -->
    <meta name="description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta name="image" content="www.volitioncap.com/opengraph.png">
    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Volition Capital">
    <meta itemprop="description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta itemprop="image" content="www.volitioncap.com/opengraph.png">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Volition Capital">
    <meta name="twitter:description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Volition Capital">
    <meta name="og:description" content="Volition is an investment platform that provides joint investment opportunities for well deserved financial rewards.">
    <meta name="og:image" content="www.volitioncap.com/opengraph.png">
    <meta name="og:url" content="www.volitioncap.com">
    <meta name="og:site_name" content="Volition Capital">
    <meta name="og:type" content="website">
    <!--Favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('img/favicons/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('img/favicons/safari-pinned-tab.svg') }}" color="#a0830b">
    <meta name="theme-color" content="#ffffff">
    <!-- stylesheet -->

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
    <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css"> -->

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/buttons.datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/additionalStyle.css') }}">
    <!--wysiwyg editor"-->
    <script src="https://cdn.ckeditor.com/ckeditor5/1.0.0-alpha.2/classic/ckeditor.js"></script>
    <script src="{{ asset('js/Chartjs/Chart.js') }}"></script>


    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/stringMonthYear.js"></script>
    <!-- <script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script> -->
    
    <script src="{{ asset('js/currency.js') }}"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.16/type-detection/numeric-comma.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.16/type-detection/formatted-num.js"></script>

</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" id="mainNav">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ url('/home') }}" class="navbar-brand">
                    <img src="{{ asset('img/volition-mobile.svg') }}"> 
                </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right" style="display: none;">
                <li class="@if($metadata['menu_active'] == 'dashboard') active @endif"><a href="{{ route('users.home') }}">Dashboard</a></li>
                <li class="@if($metadata['menu_active'] == 'started') active @endif"><a href="{{ url('getting-started') }}">Getting Started</a></li>
                <li class="@if($metadata['menu_active'] == 'pool') active @endif">
                    <a class="accordion-toggle toggle-switch collapsed" data-toggle="collapse" href="#submenu-3" aria-expanded="false">
                        <span class="sidebar-title">Investment Pools</span>
                        <b class="caret"></b>
                    </a>
                    <ul id="submenu-3" class="panel-collapse panel-switch collapse @if($metadata['menu_active'] == 'pool') show @endif" role="menu" aria-expanded="false">
                        <li><a href="{{ route('pools.pool.show', 'Volition Green') }}"><i class="fa fa-caret-right"></i>Volition Green</a></li>
                        <li><a href="{{ route('pools.pool.show', 'Volition Black') }}"><i class="fa fa-caret-right"></i>Volition Black</a></li>
                        <li><a href="{{ route('pools.pool.show', 'Volition Red') }}"><i class="fa fa-caret-right"></i>Volition Red</a></li>
                        <li><a href="{{ route('blue.index') }}"><i class="fa fa-caret-right"></i>Volition Blue</a></li>
                        <li><a href="{{ route('pools.pool.show', 'Volition Privilege') }}"><i class="fa fa-caret-right"></i>Volition Privilege </a></li>
                        <li><a href="{{ route('pools.pool.show', 'Volition Business') }}"><i class="fa fa-caret-right"></i>Volition Business </a></li>
                    </ul>
                </li>
                <li class="@if($metadata['menu_active'] == 'investments') active @endif"><a href="{{ route('pools.show') }}">Investment History</a></li>
                <li class="@if($metadata['menu_active'] == 'transactions') active @endif"><a href="{{ route('transactions.index') }}">Transaction History</a></li>
                <li class="@if($metadata['menu_active'] == 'referrals') active @endif"><a href="{{ route('referrals.index') }}">Referrals</a></li>
                @if(Auth::user()->isAdmin)
                <li class="@if($metadata['menu_active'] == 'portfolio') active @endif"><a href="{{ route('portfolio.index') }}">Portfolio Management</a></li>
                <li class="@if($metadata['menu_active'] == 'messages') active @endif"><a href="{{ route('messages.index') }}">Messages</a></li>
                <li class="@if($metadata['menu_active'] == 'users') active @endif"><a href="{{ route('users.index') }}">Users</a></li>
                    <li class="@if($metadata['menu_active'] == 'archived') active @endif"><a href="{{ route('users.archived') }}">Archived Users</a></li>
                @endif
                <li class="@if($metadata['menu_active'] == 'logs') active @endif"><a href="{{ route('logs.index') }}">Activity Log</a></li>
                <li class="@if($metadata['menu_active'] == 'settings') active @endif"><a href="{{ route('settings.index') }}">@if(Auth::user()->isAdmin) Settings @else Account @endif</a></li>

                @if(!Auth::user()->isAdmin)
                <li class="@if($metadata['menu_active'] == 'help') active @endif"><a href="{{ route('users.help') }}"><small>Help</small></a></li>
                <li class="@if($metadata['menu_active'] == 'messages') active @endif"><a href="{{ route('users.contact') }}"><small>Contact</small></a></li>
                @endif
                <li><a href="{{ url('/logout') }}"><small>Log Out</small></a></li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="sidebar">
            <div class="row">
                <div class="sidebar-logo col-md-6">
                    <a href="{{ url('/home') }}">
                    <img src="{{ asset('img/Volition-logo.svg') }}" class="sidebar-logo-img" alt="Volition logo">
                    </a>
                </div>
            </div>
            <ul class="nav nav-sidebar">
                <li class="@if($metadata['menu_active'] == 'dashboard') active @endif"><a href="{{ route('users.home') }}">Dashboard</a></li>
                <li class="@if($metadata['menu_active'] == 'started') active @endif"><a href="{{ url('getting-started') }}">Getting Started</a></li>
                <li class="@if($metadata['menu_active'] == 'pool') active @endif">
                    <a class="accordion-toggle toggle-switch collapsed" data-toggle="collapse" href="#submenu-2" aria-expanded="false">
                        <span class="sidebar-title">Investment Pools</span>
                        <b class="caret"></b>
                    </a>
                    <ul id="submenu-2" class="panel-collapse panel-switch collapse @if($metadata['menu_active'] == 'pool') show @endif" role="menu" aria-expanded="false">
                        <li><a href="{{ route('pools.pool.show', 'Volition Green') }}"><i class="fa fa-caret-right"></i>Volition Green</a></li>
                        <li><a href="{{ route('pools.pool.show', 'Volition Black') }}"><i class="fa fa-caret-right"></i>Volition Black</a></li>
                        <li><a href="{{ route('pools.pool.show', 'Volition Red') }}"><i class="fa fa-caret-right"></i>Volition Red</a></li>
                        <li><a href="{{ route('blue.index') }}"><i class="fa fa-caret-right"></i>Volition Blue</a></li>
                        <li><a href="{{ route('pools.pool.show', 'Volition Privilege') }}"><i class="fa fa-caret-right"></i>Volition Privilege </a></li>
                        <li><a href="{{ route('pools.pool.show', 'Volition Business') }}"><i class="fa fa-caret-right"></i>Volition Business </a></li>
                    </ul>
                </li>
                <li class="@if($metadata['menu_active'] == 'investments') active @endif"><a href="{{ route('pools.show') }}">Investment History</a></li>
                <li class="@if($metadata['menu_active'] == 'transactions') active @endif"><a href="{{ route('transactions.index') }}">Transaction History</a></li>
                <li class="@if($metadata['menu_active'] == 'referrals') active @endif"><a href="{{ route('referrals.index') }}">Referrals</a></li>
                @if(Auth::user()->isAdmin)
                <li class="@if($metadata['menu_active'] == 'portfolio') active @endif"><a href="{{ route('portfolio.index') }}">Portfolio Management</a></li>
                <li class="@if($metadata['menu_active'] == 'messages') active @endif"><a href="{{ route('messages.index') }}">Messages</a></li>
                <li class="@if($metadata['menu_active'] == 'users') active @endif"><a href="{{ route('users.index') }}">Users</a></li>
                    <li class="@if($metadata['menu_active'] == 'archived') active @endif"><a href="{{ route('users.archived') }}">Archived Users</a></li>
                @endif
                <li class="@if($metadata['menu_active'] == 'logs') active @endif"><a href="{{ route('logs.index') }}">Activity Log</a></li>
                <li class="@if($metadata['menu_active'] == 'settings') active @endif"><a href="{{ route('settings.index') }}">@if(Auth::user()->isAdmin) Settings @else Account @endif</a></li>
            </ul>
            <ul class="nav nav-sidebar">
                @if(!Auth::user()->isAdmin)
                <li class="@if($metadata['menu_active'] == 'help') active @endif"><a href="{{ route('users.help') }}"><small>Help</small></a></li>
                <li class="@if($metadata['menu_active'] == 'messages') active @endif"><a href="{{ route('users.contact') }}"><small>Contact</small></a></li>
                @endif
                <li><a href="{{ url('/logout') }}"><small>Log Out</small></a></li>
            </ul>
        </div>
        <main class="main-content">
