@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Help</h1>
    </div>

    <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">FAQ</span>
            </div>
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            What is Volition?
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        Volition is a private cooperative. It was set up to help the honest & hardworking to earn financial rewards, through joint investments. The company's Directors are <a href="https://www.linkedin.com/in/kolaoyeneyin/" target="_blank">Kola Oyeneyin</a>, <a href="https://www.linkedin.com/in/subomiplumptre/" target="_blank">Subomi Plumptre</a> and <a href="https://www.linkedin.com/in/zeal-akaraiwe-0535a712/" target="_blank">Zeal Akaraiwe</a>.
                        <br><br>
                        To invite a friend to join the cooperative, kindly use the Referrals section of your dashboard. Please provide their email addresses, separated by commas. They will receive a referral link and code. 
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            How does Volition Capital work?
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        Volition's Cooperative currently runs 6 investment pools and has a private option for customised portfolios. Please review the Getting Started section on your dashboard before subscribing to a pool in the Investment Pools section.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            How do I subscribe and pay?
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        You may select an investment pool on the dashboard and then pay with your Debit or Credit Card via our payment portal. If you prefer a direct bank account transfer, please use our bank details in the Getting Started section.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                            Completing your profile
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                    <div class="panel-body">
                        It is important for you to complete your profile on your dashboard. This is because remittances cannot be made to members with incomplete profiles, who do not supply their bank account details.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">More Questions?</span>
            </div>
        </div>
        <div class="table-box">
            <dl class="horizontal-dl-2" style="padding-bottom:0px; padding-top: 20px;">
                <dt>Email Address</dt>
                <dd><strong><a href="mailto://ask@volitioncap.com" class="gold-text">ask@volitioncap.com</a></strong></dd>
                <dt style="border-bottom: 0px;">Phone Number</dt>
                <dd style="border-bottom: 0px;"><strong><a href="tel://ask@volitioncap.com" class="gold-text">+234 705 651 1542 (Whatsapp Only)</a></strong></dd>
            </dl>
        </div>
    </div>
</div>
@endsection
