@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">New Investment</h1>
    </div>
    
    <div class="col-xs-12"><span class="card-title">Make a new investment in one of our pools</span></div>

    @if(session()->has('error'))
        <div class="col-xs-12 col-md-6">
            <div class="notification-error">{{ session()->get('error') }}</div>
        </div>
    @endif
    
    <div class="col-xs-12 col-md-7">
        <div class="box">
            {!! Form::open(['route' => ['pools.store'], 'method' => 'POST', 'accept-charset' => 'UTF-8', 'id' => 'poolInvestForm']) !!}
            @if(($selected->id == 5 || $selected->id == 6) && $p_b_admin_fee_exist > 0)
                <input type="hidden" name="admin_fees" value="0" id="adminFees">
            @else
                <input type="hidden" name="admin_fees" value="{{ $selected->admin_fees }}" id="adminFees">
                @endif


                <div class="row">
                    @if($selected->id == 5 || $selected->id == 6)
                    <div class="col-md-12">
                        <div class="form-group form-group-default">
                            <label>Select Investment Pool<sub>*</sub></label>
                            {{ Form::select('pool', $pools, $selected->id, ['id' => 'pool', 'class' => 'form-control', 'placeholder' => '', 'aria-label' => 'Select investment pool', 'disabled' => 'disabled']) }}
                            <input type="hidden" name="pool" value="{{ $selected->id }}">
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="green">{{ $green_plus->name }} (₦{{ number_format($green_plus->upgrade) }})</label>
                                    <select name="{{ $green_plus->slug }}" class="form-control" id="green" aria-label="enter number of slots">
                                        <option value="0"></option>
                                        <option selected value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                            </div>
                            <div class="ecol-md-1"> </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="green_month">Months</label>
                                    <select name="green_month" class="form-control" id="green_month" aria-label="enter number of slots">
                                        <option value="0"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option selected="selected" value="12">12</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="red">{{ $red_plus->name }} (₦{{ number_format($red_plus->upgrade) }})</label>

                                    <select name="{{ $red_plus->slug }}" class="form-control" id="red" aria-label="enter number of slots">
                                        <option value="0"></option>
                                        <option selected value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                            </div>
                            <div class="ecol-md-1"> </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="red_month">Months</label>
                                    <select name="red_month" class="form-control" id="red_month" aria-label="enter number of month">
                                        <option value="0"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option selected="selected" value="12">12</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="black">{{ $black_plus->name }} (₦{{ number_format($black_plus->upgrade) }})</label>
                                    <select name="{{ $black_plus->slug }}" class="form-control" id="black" aria-label="enter number of slots">
                                        <option value="0"></option>
                                        <option selected value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                            </div>
                            <div class="ecol-md-1"> </div>
                            <div class="col-md-6">
                                <div class="form-group form-group-default">
                                    <label for="black_month">Months</label>
                                    <select name="black_month" class="form-control" id="black_month" aria-label="enter number of months">
                                        <option value="0"></option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option selected="selected" value="12">12</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div id="hidden_form"></div>
                    </div>
                    @else

                    <div class="col-md-6">
                        <div class="form-group form-group-default">
                            <label>Select Investment Pool<sub>*</sub></label>
                            {{ Form::select('pool', $pools, $selected->id, ['id' => 'pool', 'class' => 'form-control', 'placeholder' => '', 'aria-label' => 'Select investment pool', 'disabled' => 'disabled']) }}
                            <input type="hidden" name="pool" value="{{ $selected->id }}">
                        </div>
                    </div>
                    @endif
                    <div class="col-md-6">
                        <div>
                            @if($selected->id == 5 || $selected->id == 6)

                                <!-- <label for="upgrade">Amount to Invest (Min: 5million naira)</label> -->
                                <!-- <input type="number" min="5000000" name="upgrade" class="form-control" id="custom-investment" placeholder="" required="required" aria-label="enter your investment amount"> -->

                                <!-- <input type="hidden" name="slots" value="1" id="slots"> -->                            

                        </div>
                        <p style="font-size:12px;">For custom investments, kindly send a mail to <a href="mailto:members@volitioncap.com" style="text-decoration:none;">members@volitioncap.com</a></p>
                    </div>
                            @else

                                <input type="hidden" name="upgrade" value="{{ $selected->upgrade }}" id="upgrade">
                                <label>Number of slots<sub>*</sub></label>
                                <select name="slots" class="form-control" id="slots" aria-label="enter number of slots">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                </select>

                        </div>
                        <p style="font-size:12px;">To subscribe to over 20 slots, kindly send a mail to <a href="mailto:members@volitioncap.com" style="text-decoration:none;">members@volitioncap.com</a></p>
                    </div>

                                @endif

                </div>

    @if(($selected->id == 5 || $selected->id == 6) && $p_b_admin_fee_exist > 0)

        @else
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-group-default" style="margin-top:0;">
                            <label>Month of commencement<sub>*</sub></label>
                            @php
                                $month = \Carbon\Carbon::now()->addMonth(1)->month
                            @endphp
                            <select name="start_month" id="startMonth" class="form-control" aria-label="Month of commencement" disabled="disabled">
                                <option value="1" @if($month == 1) selected @endif>January</option>
                                <option value="2" @if($month == 2) selected @endif>February</option>
                                <option value="3" @if($month == 3) selected @endif>March</option>
                                <option value="4" @if($month == 4) selected @endif>April</option>
                                <option value="5" @if($month == 5) selected @endif>May</option>
                                <option value="6" @if($month == 6) selected @endif>June</option>
                                <option value="7" @if($month == 7) selected @endif>July</option>
                                <option value="8" @if($month == 8) selected @endif>August</option>
                                <option value="9" @if($month == 9) selected @endif>September</option>
                                <option value="10" @if($month == 10) selected @endif>October</option>
                                <option value="11" @if($month == 11) selected @endif>November</option>
                                <option value="12" @if($month == 12) selected @endif>December</option>
                            </select>
                        </div>
                    </div>
                    @php
                        $expiry = round($month + (($selected->expiry / 30) - 1), 0);

                    //get next year month from the difference
                    if($expiry > 12) $expiry = $expiry - 12;

                    @endphp
                    <div class="col-md-6">
                        <div class="form-group form-group-default" style="margin-top:0;">
                            <label>Month of conclusion</label>
                            <select name="end_month" id="endMonth" class="form-control" aria-label="Month of ROI" disabled="disabled">
                                <option value="1" @if($expiry == 1) selected @endif>January</option>
                                <option value="2" @if($expiry == 2) selected @endif>February</option>
                                <option value="3" @if($expiry == 3) selected @endif>March</option>
                                <option value="4" @if($expiry == 4) selected @endif>April</option>
                                <option value="5" @if($expiry == 5) selected @endif>May</option>
                                <option value="6" @if($expiry == 6) selected @endif>June</option>
                                <option value="7" @if($expiry == 7) selected @endif>July</option>
                                <option value="8" @if($expiry == 8) selected @endif>August</option>
                                <option value="9" @if($expiry == 9) selected @endif>September</option>
                                <option value="10" @if($expiry == 10) selected @endif>October</option>
                                <option value="11" @if($expiry == 11) selected @endif>November</option>
                                <option value="12" @if($expiry == 12) selected @endif>December</option>
                            </select>
                        </div>
                    </div>
                </div>
    @endif
                @if($selected->id == 3)
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="radio" style="display: none">
                            <input id="returnTwo" name="return_period"  value="0" type="radio">
                            <label for="returnTwo" class="radio-label">Receive returns every 2 months</label>
                        </div>
                        <div class="radio">
                            <input id="returnEnd" name="return_period" value="1" type="radio" checked="checked">
                            <label for="returnEnd" class="radio-label">Compound returns until end of tenure</label>
                        </div>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <p class="desc-text2 text-left">
                            We can only receive funds for next month's cycle by 11:59pm WAT on the last day of the
                            previous month. Funds received after the last day of the previous month will be treated
                            the subsequent month.
                        </p>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    
    <!-- summary card -->
    <div class="col-xs-12 col-md-5">
        <div class="table-box">
            <dl class="horizontal-dl">

                @if($selected->id == 5 || $selected->id == 6)
                    <dt>Subtotal </dt>
                    <dd>₦<span id="custom-subscription-subtotal">0</span></dd>

                    @else
                <dt>Subtotal (<span id="slots-no">1</span> slots of ₦{{ number_format($selected->upgrade) }} each) </dt>
                <dd>₦<span id="subscription-subtotal">{{ number_format($selected->upgrade) }}</span></dd>
                @endif
                
                @if($admin_fees)
                <dt>Administrative Fee</dt>

                    @if(($selected->id == 5 || $selected->id == 6) && $p_b_admin_fee_exist > 0)
                        <dd>₦0</dd>
                    @else
                        <dd>₦{{ number_format($selected->admin_fees) }}</dd>
                    @endif

                @endif

                    @if($selected->id == 5 || $selected->id == 6)
                        <dt>Total</dt>
                        <dd><b>₦<span id="custom-subscription-total">@if($p_b_admin_fee_exist == 0) {{ number_format($selected->admin_fees) }} @else {{ number_format(0) }} @endif</span></b></dd>

                    @else
                        <dt>Total</dt>
                        <dd><b>₦<span id="subscription-total">@if($admin_fees) {{ number_format($selected->upgrade + $selected->admin_fees) }} @else {{ number_format($selected->upgrade) }} @endif</span></b></dd>

                    @endif


            </dl>

            <p class="desc-text2 text-left" style="padding:0px 32px;">On investment maturity, 1% of withdrawals will be deducted for 3rd party charges and taxes.</p>

            <div class="text-left" style="padding:0px 32px;">
                <input id="agree" type="checkbox">
                <label class="checkbox-label">Accept <a href="{{ url('terms-and-conditions') }}">Terms and Conditions</a> and <a href="{{ url('corporate-bylaw') }}">Bye-Laws</a></label>
                
            </div>
            <div class="text-left" style="padding:0px 32px; padding-top:28px" >
                Kindly use a local card as "pay with bank" & "foreign cards" attract charges. 
                To do a direct bank transfer, please check the <a href="{{ url('getting-started') }}">Getting Started</a> section.
            </div>

            <div class="row">
                <div class="col-sm-5 col-lg-5 center-block">
                    @php
                    if(env('PAYMENT_METHOD') == 'paystack'){
                    @endphp
                    <img src="{{ asset('img/paystack-badge.png') }}" class="payment-badge"></div>
                @php
                }else{
                @endphp
                <img src="{{ asset('img/ravepay-badge.png') }}" class="payment-badge"></div>
                @php
                }
                @endphp

            </div>
            <div class="form-group">
                @php
                    $today = \Carbon\Carbon::now();
                    if($today->diffInSeconds($today->copy()->endOfMonth()) < 0) {
                        echo '<button type="submit" id="pay" form="poolInvestForm" class="btn btn-gold center-block" aria-label="Pay securely">Pay Securely</button>';
                    }
                @endphp
                <button type="submit" form="poolInvestForm" id="pay" class="btn btn-gold center-block" disabled="disabled" aria-label="Pay securely">Pay Securely</button>
                
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var green_amount = <?php echo $green_plus->upgrade; ?>;
        var red_amount = <?php echo $red_plus->upgrade; ?>;
        var black_amount = <?php echo $black_plus->upgrade; ?>;
        var green_value = 0;
        var red_value = 0;
        var black_value = 0;
        var total = 0;

        (function () {
            document.getElementById('green').addEventListener('change', () => {
                green_value = document.getElementById('green').value;
                console.log(green_value);
                compute();
            }, false);

            document.getElementById('red').addEventListener('change', () => {
                red_value = document.getElementById('red').value;
                console.log(red_value);
                compute();
            }, false);

            document.getElementById('black').addEventListener('change', () => {
                black_value = document.getElementById('black').value;
                console.log(black_value);
                compute();
            }, false);
            
            document.getElementById('pay').addEventListener('click', (event) => {
                if(total < 5000000) {
                    event.preventDefault();
                    alert("This pool investment should NOT be less than 5 Million Naira");
                    return false;
                }
            }, false);

            function compute () {
                var green = green_amount * green_value;
                var red = red_amount * red_value;
                var black = black_amount * black_value;

                total = green + red + black;

                    $('#custom-subscription-subtotal').html(parseInt(total));
                    $('#custom-subscription-total').html( ( parseInt(total) ) + (parseInt($('#adminFees').val())) );

                console.log(total);
                createHiddenInput(total);
            }

            function createHiddenInput (total) {
                var input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("id", "hidden");
                input.setAttribute("name", "upgrade");
                input.setAttribute("value", total);

                $("#hidden_form").html(input);
            }
                                      
        })();
    });
</script>

@endsection
