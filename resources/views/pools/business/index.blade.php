@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $metadata['title'] }}</h1>
    </div>
    
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table class="datatable">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive" style="width: 30%;">Date</th>
                            <th scope="col" aria-live="assertive">Pool</th>
                            <th scope="col" aria-live="assertive" style="width: 30%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($subscriptions) > 0)
                            @foreach($subscriptions as $subscription)
                                <tr>
                                    <td data-label="Date">{{ $subscription->created_at->format('j M Y') }}</td>
                                    <td data-label="Pool">{{ $subscription->pool->name }}</td>
                                    <td data-label="Action" aria-live="assertive"><a href="{{ route('investment.show', $subscription->subscription_number) }}" class="btn btn-xs btn-stroke-gold">View details</a></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="Status" colspan="3">No pool subscription</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-sm-9 col-md-6">
        <h1 class="sub-header">{{ $metadata['title'] }}</h1>
    </div>
</div>
@if(session()->has('success'))
    <div class="row">
        <div class="col-xs-12 col-md-6">
            <div class="notification-success">{{ session()->get('success') }}</div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-7">
        <div class="row"><div class="col-xs-12"><span class="card-title">About</span></div></div>
        <div class="box">
            <p class="text-left">Volition Business is a private portfolio. We structure custom investment plans, based on a your investment objectives and risk profile. You’ll receive a more hands-on portfolio management and advisory services. You’ll also have
                access to physical meetings and phone calls at your expense. Volition Business is for those who want to subscribe with a lump sum and have their portfolio personally managed for them. The minimum tenure is 12 months, with pre-agreed
                windows for withdrawals. Monthly reports are provided.</p>
            <br>
            <p>The minimum subscription in Volition Business is N5,000,000. An admin fee of N500,000 is paid once every 12 months. 0.5% will be deducted from all deposits and all withdrawals to provide for bank charges.
            </p>
            <br>
            <p>To contact us directly please call
                <a href="tel:+234 705 651 1542">
                    <span class="text-link" aria-label="+234 705 651 1542">+234 705 651 1542</span>
                    <span class="sr-only">(support phone number)</span>
                </a> or
                <a href="mailto:ask@volitioncap.com" aria-label="email for support">
                    <span class="text-link">ask@volitioncap.com</span>
                </a>.</p>
        </div>
    </div>
    <div class="col-xs-12 col-md-5">
        <div class="row">
            <div class="col-xs-12">
                <span class="card-title">Sign up</span>
            </div>
        </div>
        <div class="box">
            {!! Form::open(['route' => ['business.register'], 'method' => 'POST', 'accept-charset' => 'UTF-8']) !!}
                <div class="col-md-12">
                    <p>
                        Get in touch with us and we'll contact you as soon as possible.
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="form-group form-group-default">
                        <label>Company Name</label>
                        <input type="text" class="form-control" aria-label="enter your Company's Name" value="{{ auth()->user()->firstname }} {{ auth()->user()->lastname }}" disabled="disabled">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group form-group-default">
                        <label>Company Phone number</label>
                        <input type="text" class="form-control" aria-label="enter your Company's Phone number" value="{{ auth()->user()->phone }}" disabled="disabled">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group form-group-default">
                        <label>Company Email</label>
                        <input type="email" class="form-control" aria-label="enter your company's email address" value="{{ auth()->user()->email }}" disabled="disabled">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-gold pull-right" aria-label="save changes">Send</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>

    </div>
</div>
@endif
@endsection