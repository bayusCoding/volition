@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $metadata['title'] }}</h1>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">View All Investments for {{ $metadata['title'] }}</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table id="table">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive" style="width: 30%;">Date</th>
                            <th scope="col" aria-live="assertive">User</th>
                            <th scope="col" aria-live="assertive">Pool</th>
                            <th scope="col" aria-live="assertive" style="width: 30%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($subscriptions))
                            @foreach($subscriptions as $subscription)
                                <tr>
                                    <td data-label="Date">{{ $subscription->created_at->format('j M Y') }}</td>
                                    <td data-label="User">{{ $subscription->user->firstname }} {{ $subscription->user->lastname }}</td>
                                    <td data-label="Pool">{{ $subscription->pool->name }}</td>
                                    <td data-label="Action" aria-live="assertive">
                                        <a href="{{ route('investment.show', $subscription->subscription_number) }}" class="btn btn-xs btn-stroke-gold">View</a>
                                        <a href="{{ route('users.pools.edit', $subscription->id) }}" class="btn btn-xs btn-stroke-gold">Edit</a>
                                        <form action="{{ route('users.pools.destroy', $subscription->id) }}" method="post" id="deleteSubscriptionForm{{ $subscription->id }}" style="display:inline-block;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="user" value="{{ $subscription->user_id }}">
                                            <button onclick="return ConfirmDelete()" type="submit" form="deleteSubscriptionForm{{ $subscription->id }}" class="btn btn-xs btn-stroke-red" style="margin-top:0">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="status" colspan="4">No pool subscription</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@else
<div class="row">
    <div class="col-sm-9 col-md-12">
        <div class="row">
            <div class="col-sm-4"><h1 class="sub-header">{{ $metadata['title'] }}</h1></div>
            <div class="col-xs-12 col-sm-2"><a href="{{ route('pools.create', $metadata['title']) }}" class="btn btn-lg btn-gold" style="margin-top:12px;">Invest</a></div>
        </div>
    </div>
    <div class="col-xs-12"><span class="card-title">Current Estimated Portfolio Value</span></div>
    <!-- 1 card -->
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1 class="{{ $metadata['slug'] }}-text">{{ count($subscriptions) }}</h1>
                    <p class="desc-text">Running investments</p>
                </div>
            </div>
        </div>
    </div>
    <!-- 2 card -->
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1 class="{{ $metadata['slug'] }}-text">{{ $metadata['pool_completed'] }}</h1>
                    <p class="desc-text">Completed investments</p>
                </div>
            </div>
        </div>
    </div>
    <!-- 3 card -->
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <h1 class="{{ $metadata['slug'] }}-text">{{ $metadata['current_pool_change'] }}%</h1>
                    <p class="desc-text">Current Pool ROI</p>
                </div>
            </div>
        </div>
    </div>
    <!-- 4 Card -->
    <div class="col-xs-12 col-md-3">
        <div class="box">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p class="desc-text text-left">
                        @if($admin_fees_expiry)
                            Administration fee for this pool expires in
                            {{ $admin_fees_expiry->diffForHumans() }} on the {{ $admin_fees_expiry->format('j M Y') }}
                        @else
                            No current active administration fee for this pool
                        @endif
                    </p>
                    @if($admin_fees_expiry) 
                        <a href="{{ route('adminfees.renew', $metadata['title']) }}" class="btn-gold">Renew</a>
                    @else
                        <a href="{{ route('pools.create', $metadata['title']) }}" class="btn-gold">Join Pool</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @if(!empty($subscriptions))
        @foreach($subscriptions as $subscription)
            <div class="row">
                <!-- second section -->
                <div class="col-xs-12 col-md-7">
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="card-title">{{ $subscription->pool->name }} investment {{ $subscription->start_date->format('F Y') }}</span>
                        </div>
                    </div>
                    <div class="box">
                        <canvas id="{{ $subscription->subscription_number }}" width="400" height="400"></canvas>
                    </div>
                </div>

                <!-- summary card -->
                <div class="col-xs-12 col-md-4">
                    <div class="row">
                        <div class="col-xs-12">
                            <span class="card-title">Details</span>
                        </div>
                    </div>

                    @if($subscription->pool_id == 5 || $subscription->pool_id == 6)

                        @php
                        $child_pool_slot = explode('+', $subscription->child_pool_slot);
                        $child_pool_duration = explode('+', $subscription->child_pool_slot_duration);

                                @endphp

                        <div class="table-box">
                            <dl class="horizontal-dl">
                                <dt>Portfolio Value (%) </dt>
                                <dd class="green-text">{{ $metadata['current_pool_change'] }}%</dd>
                                <dt>Portfolio Value (₦)</dt>
                                <dd><b>
                                        ₦@php
                                            if ($metadata['current_pool_change'] != 0) {
                                                if ($subscription->start_date->month >= \Carbon\Carbon::now()->month) {
                                                    echo number_format($subscription->subscription_fee);
                                                } else {
                                                    echo number_format((($metadata['current_pool_change'] / 100) + 1) * $subscription->subscription_fee);
                                                }
                                            } else {
                                                echo number_format($subscription->subscription_fee);
                                            }
                                        @endphp
                                    </b></dd>
                                <dt>Total Principal</dt>
                                <dd>₦{{ number_format($subscription->subscription_fee) }}</dd>

                                <dt>Green Plus ({{ $child_pool_duration[0] }} months)</dt>
                                <dd>{{ $child_pool_slot[0] }} x {{ number_format($pool[6]->upgrade) }}</dd>
                                <dt>Red Plus ({{ $child_pool_duration[1] }} months)</dt>
                                <dd>{{ $child_pool_slot[1] }} x {{ number_format($pool[7]->upgrade) }}</dd>
                                <dt>Black Plus ({{ $child_pool_duration[2] }} months)</dt>
                                <dd>{{ $child_pool_slot[2] }} x {{ number_format($pool[8]->upgrade) }}</dd>

                                <dt>Duration</dt>
                                <dd>{{ $subscription->start_date->format('F Y') }} - {{ $subscription->end_date->format('F Y') }}</dd>
                                <dt>Status</dt>
                                <dd>
                                    @php
                                        if ($subscription->end_date <= \Carbon\Carbon::now()) {
                                            echo "Expired";
                                        } else {
                                            echo "Running";
                                        }
                                    @endphp

                                </dd>
                            </dl>
                        </div>


                        @else
                    <div class="table-box">
                        <dl class="horizontal-dl">
                            <dt>Portfolio Value (%) </dt>
                            <dd class="green-text">{{ $metadata['current_pool_change'] }}%</dd>
                            <dt>Portfolio Value (₦)</dt>
                            <dd><b>
                                ₦@php 
                                if ($metadata['current_pool_change'] != 0) {
                                    if ($subscription->start_date->month >= \Carbon\Carbon::now()->month) {
                                        echo number_format($subscription->subscription_fee);
                                    } else {
                                        echo number_format((($metadata['current_pool_change'] / 100) + 1) * $subscription->subscription_fee);
                                    } 
                                } else {
                                    echo number_format($subscription->subscription_fee);
                                }
                                @endphp
                            </b></dd>
                            <dt>Total Principal</dt>
                            <dd>₦{{ number_format($subscription->subscription_fee) }}</dd>
                            <dt>Number of Slots</dt>
                            <dd>{{ $subscription->slots }} x {{ $subscription->subscription_fee / $subscription->slots }}</dd>
                            <dt>Duration</dt>
                            <dd>{{ $subscription->start_date->format('F Y') }} - {{ $subscription->end_date->format('F Y') }}</dd>
                            <dt>Status</dt>
                            <dd>
                                @php 
                                if ($subscription->end_date <= \Carbon\Carbon::now()) {
                                    echo "Expired";
                                } else {
                                    echo "Running";
                                } 
                                @endphp

                            </dd>
                        </dl>
                    </div>
                        @endif


                </div>
                <script>
                    var ctx = document.getElementById("{{ $subscription->subscription_number }}");
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: @php print json_encode($subscription->current_date_range); @endphp,
                            datasets: [{
                                lineTension: 0,
                                bezierCurve: false,
                                responsive: true,
                                maintainAspectRatio: true,
                                tension: 0,
                                label: 'Percentage Growth of Investment',
                                data: @php print json_encode($subscription->portfolio_range); @endphp,
                                backgroundColor: [
                                    @if ($metadata['slug'] == 'green')
                                        'rgba(29, 147, 0, 0.1)'
                                    @elseif ($metadata['slug'] == 'red')
                                        'rgba(234, 6, 6, 0.1) '
                                    @elseif ($metadata['slug'] == 'black')
                                        'rgba(68, 68, 68, 0.1)'
                                    @endif
                                ],
                                borderColor: [
                                    @if ($metadata['slug'] == 'green')
                                        'rgba(29, 147, 0, 0.1)'
                                    @elseif ($metadata['slug'] == 'red')
                                        'rgba(234, 6, 6, 0.1) '
                                    @elseif ($metadata['slug'] == 'black')
                                        'rgba(68, 68, 68, 0.1)'
                                    @endif
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                xAxes: [{
                              gridLines: {
                                display: false,
                              },
                              ticks: {
                                fontSize: 14,
                                fontFamily: "Gotham",
                                padding: 10,
                              }
                            }],
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        fontSize: 14,
                                        fontFamily: "Gotham",
                                        padding: 10,
                                    },
                                    gridLines: {
                                    color: "#778F9B",
                                    drawBorder: false,
                                     borderDash: [2, 5],
                                        },
                                }],
                            }
                        }
                    });
                </script>
            </div>
        @endforeach
    @endif
</div>
@endif
<script>
    function ConfirmDelete() {
        return confirm("Are you sure you want to delete?");
    }
    
    $(document).ready(function() {
      $('#table').DataTable();
    });
</script>
@endsection
