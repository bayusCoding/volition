@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-4"><h1 class="sub-header">Blue Categories</h1></div>
            <div class="col-xs-12 col-sm-3">@if(Auth::user()->isAdmin)<a href="{{ route('categories.create') }}" class="btn btn-lg btn-gold" style="margin-top:12px;">New Category</a>@endif</div>
            <div class="col-sm-5">
                <div id="search_filter" class="pull-left search_input">
                    <label><input type="search" class="" placeholder="Search Categories"></label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="col-md-12 table-box">
            <!-- table card -->
            <table>
                <thead>
                    <tr>
                        <th scope="col" aria-live="assertive" style="width: 30%;">Date</th>
                        <th scope="col" aria-live="assertive">Title</th>
                        <th scope="col" aria-live="assertive" style="width: 30%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($categories) > 0)
                        @foreach($categories as $category)
                            <tr>
                                <td data-label="Date">{{ $category->created_at->format('j M Y') }}</small></td>
                                <td data-label="Title">{{ $category->title }}</td>
                                <td data-label="Action" aria-live="assertive"><a href="{{ route('categories.edit', $category->id) }}" class="btn btn-xs btn-stroke-gold">Edit</a></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
    @if (count($blues) > 0)
        @foreach($blues as $blue)
            <div class="col-xs-12 col-md-4">
                <div class="box">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="inner-topic">{{ $blue->title }}</h3>
                            <p class="text-left">{{ substr($blue->markdown,0,250).'...' }}</p>
                            <p class="blog-date">{{ $blue->created_at->toFormattedDateString() }}</p>

                            <div class="row">
                                <div class="col-xs-6"> 
                                    <a href="{{ route('blue.show', $blue->id) }}" class="btn btn-stroke-gold">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif

    {{ $blues->links() }}
@endif
@endsection
