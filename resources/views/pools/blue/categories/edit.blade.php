@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Edit Category</h1>
    </div>

    @if(session()->has('success'))
        <div class="col-xs-12 col-md-6">
            <div class="notification-success">{{ session()->get('success') }}</div>
        </div>
    @endif

    <div class="col-xs-12 col-md-6">
        <div class="box">
            {!! Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'POST', 'accept-charset' => 'UTF-8', 'id' => 'categoryUpdateForm']) !!}
            <div class="row">
                <div class="col-xs-12 col-md-12" aria-live="assertive">
                    <div class="form-group form-group-default{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label>Title</label>
                        <input name="title" class="form-control" value="{{ $category->title or old('title') }}" required aria-label="enter category title">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" aria-live="assertive">
                    <div class="form-group form-group-default">
                        <label>Description</label>
                        <textarea cols="80" rows="10" name="description" class="form-control" aria-label="enter category description">{{ $category->description or old('description') }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12 text-right">
                    <button type="submit" form="categoryUpdateForm" class="btn btn-lg btn-gold" aria-label="publish">Update</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
