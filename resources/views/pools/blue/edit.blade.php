@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Edit Post</h1>
    </div>

    @if(session()->has('success'))
        <div class="col-xs-12 col-md-6">
            <div class="notification-success">{{ session()->get('success') }}</div>
        </div>
    @endif

    <div class="col-xs-12 col-md-12">
        <div class="box">
            {!! Form::model($blue, ['route' => ['blue.update', $blue->id], 'method' => 'PUT', 'accept-charset' => 'UTF-8', 'id' => 'blueUpdateForm']) !!}
            <div class="row">
                <div class="col-xs-12 col-md-6" aria-live="assertive">
                    <div class="form-group form-group-default{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label>Post Title</label>
                        <input name="title" class="form-control" value="{{ $blue->title or old('title') }}" required aria-label="enter your post title">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group form-group-default">
                        <label>Categories</label>
                        {{ Form::select('blue_category_id', $category, $blue->blue_categories_id, ['class' => 'form-control', 'placeholder' => '', 'aria-label' => 'Select blue post categories']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" aria-live="assertive">
                    <textarea cols="80" rows="10" name="markdown" id="editor" aria-label="enter your post content">{{ $blue->markdown }}</textarea>
                </div>
            </div>
            {!! Form::close() !!}
            <form action="{{ route('blue.destroy', $blue->id) }}" method="POST" id="blueDeleteForm" class="needs-validation" novalidate="">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
            </form>
            <div class="row">
                <div class="col-xs-12 col-md-12 text-right">
                    <button onclick="return ConfirmDelete()" type="submit" form="blueDeleteForm" class="btn btn-lg btn-stroke-gold" aria-label="delete" style="margin-right: 50px;">Delete</button>
                    <button type="submit" form="blueUpdateForm" class="btn btn-lg btn-gold" aria-label="update">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
<script>
    function ConfirmDelete() {
        return confirm("Are you sure you want to delete?");
    }
</script>
