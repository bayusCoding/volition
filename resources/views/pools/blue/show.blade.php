@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-9 col-md-6">
        <h1 class="sub-header">{{ $blue->title }}</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-7">
        <div class="box">
            {!! $blue->markdown !!}
        </div>
        <a href="{{ URL::previous() }}" class="btn-stroke-gold">Back</a>
    </div>
</div>
@endsection
