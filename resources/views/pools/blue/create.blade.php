@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">Create Post</h1>
    </div>
    <div class="col-xs-12 col-md-12">
        <div class="box">
            {!! Form::open(['route' => ['blue.store'], 'method' => 'POST', 'accept-charset' => 'UTF-8', 'id' => 'blueCreateForm']) !!}
            <div class="row"> 
                <div class="col-xs-12 col-md-6" aria-live="assertive">
                    <div class="form-group form-group-default">
                        <label>Post Title</label>
                        <input name="title" class="form-control form-control-textarea"  required rows="20" cols="50" aria-label="enter your post title">
                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group form-group-default">
                        <label>Categories</label>
                        {{ Form::select('blue_category_id', $category, null, ['class' => 'form-control', 'placeholder' => '', 'aria-label' => 'Select blue post categories']) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-12" aria-live="assertive">
                    <textarea cols="80" rows="10" name="markdown" id="editor" aria-label="enter your post content"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12 text-right">
                    <button type="submit" form="blueCreateForm" class="btn btn-lg btn-gold" aria-label="publish">Publish</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endif
@endsection
