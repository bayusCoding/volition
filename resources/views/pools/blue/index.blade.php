@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-12 col-md-3"><h1 class="sub-header">Volition Blue</h1></div>
            <div class="col-xs-6 col-sm-3 col-md-2">@if(Auth::user()->isAdmin)<a href="{{ route('blue.create') }}" class="btn btn-lg btn-gold" style="margin-top:12px;">New Post</a>@endif</div>
            <div class="col-xs-6 col-sm-3 col-md-2 text-right">@if(Auth::user()->isAdmin)<a href="{{ route('categories.index') }}" class="btn btn-lg btn-stroke-gold" style="margin-top:12px;">Categories</a>@endif</div>
            <div class="col-sm-6 col-md-5">
                <div id="search_filter" class="pull-left search_input">
                    <form action="{{ route('blue.index') }}" method="GET">
                        <label><input type="search" name="q" placeholder="Search opportunities"></label>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12"><span class="card-title">Projects Members May Independently Invest In</span></div>
    @if (count($blues) > 0)
        @foreach($blues as $blue)
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="box">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="content-wrap">
                                <h3 class="inner-topic">{{ $blue->title }}</h3>
                                <p class="text-left">{!! substr($blue->markdown,0,250).'...' !!}</p>
                                <p class="blog-date">{{ $blue->created_at->toFormattedDateString() }}</p>
                            </div>
                            <div class="row">
                                <div class="col-xs-6"> 
                                    <a href="{{ route('blue.show', $blue->id) }}" class="btn btn-stroke-gold">Read more</a>
                                </div>
                                @if(Auth::user()->isAdmin)
                                <div class="col-xs-6 text-right">
                                    <a href="{{ route('blue.edit', $blue->id) }}" class="btn btn-lg btn-gold" aria-label="publish">Edit</a>
                                </div> 
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif

    <div class="col-xs-12">{{ $blues->links() }}</div>
</div>

@endsection