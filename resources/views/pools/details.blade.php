@extends('layouts.app')

@section('content')
    <div class="row">
        <!-- second section -->
        <div class="col-xs-12 col-md-7">
            <div class="row">
                <div class="col-xs-12">
                <span class="card-title">
                    {{ $subscription->pool->name }} investment {{ $subscription->start_date->format('F Y') }}
                    @if(Auth::user()->isAdmin) - {{ $subscription->user->firstname }} {{ $subscription->user->lastname }} @endif
                </span>
                </div>
            </div>
            <div class="box">
                <canvas id="{{ $subscription->subscription_number }}" width="400" height="400"></canvas>
            </div>
        </div>

        <!-- summary card -->
        <div class="col-xs-12 col-md-4">
            <div class="row">
                <div class="col-xs-12">
                    <span class="card-title">Details</span>
                </div>
            </div>

            @if($subscription->pool_id == 5 || $subscription->pool_id == 6)

                @php
                    $child_pool_slot = explode('+', $subscription->child_pool_slot);
                    $child_pool_duration = explode('+', $subscription->child_pool_slot_duration);

                @endphp

                <div class="table-box">
                    <dl class="horizontal-dl">

                        <dt>Total Principal</dt>
                        <dd>₦{{ number_format($subscription->subscription_fee) }}</dd>

                        @if (strpos($subscription->subscription_number, 'OFF') !== false)

                            <dt>Green Plus ({{ $child_pool_duration[0] }} months) <span style="color:green; font-weight: bold">{{ $metadata['current_pool_green_plus_change'] }}% &uarr;</span></dt>
                            <dd>₦{{ number_format((($metadata['current_pool_green_plus_change'] / 100) + 1) * $child_pool_slot[0]) }}</dd>
                            <dt>Red Plus ({{ $child_pool_duration[1] }} months) <span style="color:green; font-weight: bold">{{ $metadata['current_pool_red_plus_change'] }}% &uarr;</span></dt>
                            <dd>₦{{ number_format((($metadata['current_pool_red_plus_change'] / 100) + 1) * $child_pool_slot[1]) }}</dd>
                            <dt>Black Plus ({{ $child_pool_duration[2] }} months) <span style="color:green; font-weight: bold">{{ $metadata['current_pool_black_plus_change'] }}% &uarr;</span></dt>
                            <dd>₦{{ number_format((($metadata['current_pool_black_plus_change'] / 100) + 1) * $child_pool_slot[2]) }}</dd>

                        @else

                            <dt>Green Plus ({{ $child_pool_duration[0] }} months) <span style="color:green; font-weight: bold">{{ $metadata['current_pool_green_plus_change'] }}% &uarr;</span></dt>
                            <dd>₦{{ number_format((($metadata['current_pool_green_plus_change'] / 100) + 1) * ($child_pool_slot[0] * $pool[6]->upgrade)) }}</dd>
                            <dt>Red Plus ({{ $child_pool_duration[1] }} months) <span style="color:green; font-weight: bold">{{ $metadata['current_pool_red_plus_change'] }}% &uarr;</span></dt>
                            <dd>₦{{ number_format((($metadata['current_pool_red_plus_change'] / 100) + 1) * ($child_pool_slot[1] * $pool[7]->upgrade)) }}</dd>
                            <dt>Black Plus ({{ $child_pool_duration[2] }} months) <span style="color:green; font-weight: bold">{{ $metadata['current_pool_black_plus_change'] }}% &uarr;</span></dt>
                            <dd>₦{{ number_format((($metadata['current_pool_black_plus_change'] / 100) + 1) * ($child_pool_slot[2] * $pool[8]->upgrade)) }}</dd>

                        @endif

                        <dt>Duration</dt>
                        <dd>{{ $subscription->start_date->format('F Y') }} - {{ $subscription->end_date->format('F Y') }}</dd>
                        <dt>Status</dt>
                        <dd>
                            @php
                                if ($subscription->end_date <= \Carbon\Carbon::now()) {
                                    echo "Expired";
                                } else {
                                    echo "Running";
                                }
                            @endphp

                        </dd>
                    </dl>
                </div>

            @else

                <div class="table-box">
                    <dl class="horizontal-dl">
                        <dt>Portfolio Value (%) </dt>
                        <dd class="green-text">{{ $metadata['current_pool_change'] }}%</dd>
                        <dt>Portfolio Value (₦)</dt>
                        <dd><b>
                                ₦@php
                                    if ($metadata['current_pool_change'] != 0) {
                                        if ($subscription->start_date->month >= \Carbon\Carbon::now()->month) {
                                            echo number_format($subscription->subscription_fee);
                                        } else {
                                            echo number_format((($metadata['current_pool_change'] / 100) + 1) * $subscription->subscription_fee);
                                        }
                                    } else {
                                        echo number_format($subscription->subscription_fee);
                                    }
                                @endphp
                            </b></dd>
                        <dt>Total Principal</dt>
                        <dd>₦{{ number_format($subscription->subscription_fee) }}</dd>
                        <dt>Number of Slots</dt>
                        <dd>{{ $subscription->slots }} x {{ $subscription->subscription_fee / $subscription->slots }}</dd>
                        <dt>Duration</dt>
                        <dd>{{ $subscription->start_date->format('F Y') }} - {{ $subscription->end_date->format('F Y') }}</dd>
                        <dt>Status</dt>
                        <dd>
                            @php
                                if ($subscription->end_date <= \Carbon\Carbon::now()) {
                                    echo "Expired";
                                } else {
                                    echo "Running";
                                }
                            @endphp

                        </dd>
                    </dl>
                </div>
            @endif


        </div>
        <script>
            var ctx = document.getElementById("{{ $subscription->subscription_number }}");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: @php print json_encode($subscription->current_date_range); @endphp,
                    datasets: [{
                        lineTension: 0,
                        bezierCurve: false,
                        responsive: true,
                        maintainAspectRatio: true,
                        tension: 0,
                        label: 'Percentage Growth of Investment',
                        data: @php print json_encode($subscription->portfolio_range); @endphp,
                        backgroundColor: [
                            @if ($metadata['slug'] == 'green')
                                'rgba(29, 147, 0, 0.1)'
                            @elseif ($metadata['slug'] == 'red')
                                'rgba(234, 6, 6, 0.1) '
                            @elseif ($metadata['slug'] == 'black')
                                'rgba(68, 68, 68, 0.1)'
                            @endif
                        ],
                        borderColor: [
                            @if ($metadata['slug'] == 'green')
                                'rgba(29, 147, 0, 0.1)'
                            @elseif ($metadata['slug'] == 'red')
                                'rgba(234, 6, 6, 0.1) '
                            @elseif ($metadata['slug'] == 'black')
                                'rgba(68, 68, 68, 0.1)'
                            @endif
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false,
                            },
                            ticks: {
                                fontSize: 14,
                                fontFamily: "Gotham",
                                padding: 10,
                            }
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                fontSize: 14,
                                fontFamily: "Gotham",
                                padding: 10,
                            },
                            gridLines: {
                                color: "#778F9B",
                                drawBorder: false,
                                borderDash: [2, 5],
                            },
                        }],
                    }
                }
            });
        </script>
    </div>
@endsection