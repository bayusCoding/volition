@extends('layouts.app')

@section('content')
@if(Auth::user()->isAdmin)
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $metadata['title'] }}</h1>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">View All Investments</span>
                <br>
                <a 
                  href="{{ URL::route('data/download/investment_data') }}"
                  style="margin-bottom:-10px">
                    <button style="padding: 7px; margin-bottom:-10px" class="btn btn-gold"> CSV </button>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- table card -->
                <table id="table">
                    <thead>
                        <tr>
                            <th scope="col" aria-live="assertive" width="15%">Date</th>
                            <th scope="col" aria-live="assertive">User</th>
                            <th scope="col" aria-live="assertive">Pool</th>
                            <th scope="col" aria-live="assertive">Amount</th>
                            <th scope="col" aria-live="assertive" style="width: 30%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($subscriptions))
                            @foreach($subscriptions as $subscription)
                                <tr>
                                    <td data-label="Date">{{ $subscription->created_at->toDateString() }}</td>
                                    <td data-label="User">{{ $subscription->user->firstname }} {{ $subscription->user->lastname }}</td>
                                    <td data-label="Pool">{{ $subscription->pool->name }}</td>
                                    <td data-label="Amount">
                                        @php if(!is_null($subscription->subscription_fee)) { echo '₦'.number_format($subscription->subscription_fee); } else { echo 'n/a'; } @endphp
                                    </td>
                                    <td data-label="Action" aria-live="assertive">
                                        <a href="{{ route('investment.show', $subscription->subscription_number) }}" class="btn btn-xs btn-stroke-gold">View</a>
                                        <a href="{{ route('users.pools.edit', $subscription->id) }}" class="btn btn-xs btn-stroke-gold">Edit</a>
                                        <form action="{{ route('users.pools.destroy', $subscription->id) }}" method="post" id="deleteSubscriptionForm{{ $subscription->id }}" style="display:inline-block;">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE">
                                            <input type="hidden" name="user" value="{{ $subscription->user_id }}">
                                            <button onclick="return ConfirmDelete()" type="submit" form="deleteSubscriptionForm{{ $subscription->id }}" class="btn btn-xs btn-stroke-red" style="margin-top:0">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td data-label="status" colspan="5">No pool subscription</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>
@else
<div class="row">
    <div class="col-sm-9 col-md-12">
        <h1 class="sub-header">{{ $metadata['title'] }}</h1>
    </div>
    <div class="col-xs-12">
        <div class="row">
            <div class="col-md-12">
                <span class="card-title">View All Investments</span>
            </div>
        </div>
        <div class="col-md-12 table-box">
            <!-- table card -->
            <table class="datatable">
                <thead>
                    <tr>
                        <th scope="col" aria-live="assertive" width="15%">Date</th>
                        <th scope="col" aria-live="assertive">Pool</th>
                        <th scope="col" aria-live="assertive">Amount</th>
                        <th scope="col" aria-live="assertive" style="width: 30%;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($subscriptions))
                        @foreach($subscriptions as $subscription)
                            <tr>
                                <td data-label="Date">{{ $subscription->created_at->toDateString() }}</td>
                                <td data-label="Pool">{{ $subscription->pool->name }}</td>
                                <td data-label="Amount">
                                    @php if(!empty($subscription->subscription_fee)) { echo $subscription->subscription_fee; } else { echo 'n/a'; } @endphp
                                </td>
                                <td data-label="Action" aria-live="assertive"><a href="{{ route('investment.show', $subscription->subscription_number) }}" class="btn btn-xs btn-stroke-gold">View details</a></td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td data-label="status" colspan="3">No pool subscription</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>

    </div>
</div>
@endif
<script>
    function ConfirmDelete() {
        return confirm("Are you sure you want to delete?");
    }

    $(document).ready(function() {
        $('#table').DataTable({
            "pageLength": 15,
            'columnDefs': [
              { type: 'currency', targets: 3} 
            ]
        });
    });
</script>
@endsection