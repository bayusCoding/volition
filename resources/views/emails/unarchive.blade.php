<table style="width: 555px; margin: 0 auto;box-shadow: 0 10px 30px 3px rgba(85,85,85,0.08);">
    <thead>
    <tr>
        <th style="padding-top: 43px; padding-bottom: 30px"><img src="{{ url('img/volitiion-logo-mailchimp.jpg') }}" alt="Volition logo" style="margin: 0 auto; width: 110px; display: block"></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="padding-left: 55px; padding-right: 55px; padding-bottom: 20px">
            <h2 style="color: #444444; font-family: Helvetica; font-weight: 600;font-size: 35px; line-height: 44px; margin: 0">Hello {{ $user->firstname }}</h2>
        </td>
    </tr>
    <tr>
        <td style="padding-left: 55px; padding-right: 55px; padding-bottom: 20px">
            <p style="color: #444444; font-family: Helvetica; font-weight: 400;font-size: 15px; line-height: 23px; margin: 0">Your account has been unarchived.</p>

            </td>
    </tr>
    <tr>
        <td style="padding-left: 55px; padding-right: 55px; padding-top: 50px; padding-bottom: 72px;">
            <a href="http://volitioncap.com/" style="padding: 14px 50px;   border: 1px solid #A0830B;  border-radius: 3px; background-color: #A0830B; color: #fff;  box-shadow: 0 3px 12px 2px rgba(160,131,11,0.3);font-size: 18px;   font-weight: 500; font-family: Helvetica;   line-height: 22px;text-decoration: none">Log in now</a>
        </td>
    </tr>
    </tbody>
</table>