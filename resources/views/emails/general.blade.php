<table style="width: 555px; margin: 0 auto;box-shadow: 0 10px 30px 3px rgba(85,85,85,0.08);">
    <thead>
        <tr>
            <th style="padding-top: 43px; padding-bottom: 30px"><img src="{{ url('img/volitiion-logo-mailchimp.jpg') }}" alt="Volition logo" style="margin: 0 auto; width: 110px; display: block"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="padding-left: 55px; padding-right: 55px; padding-bottom: 20px">
                <h2 style="color: #444444; font-family: Helvetica; font-weight: 600;font-size: 35px; line-height: 44px; margin: 0">{!! $title !!}</h2>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 55px; padding-right: 55px; padding-bottom: 20px">
                <p style="color: #444444; font-family: Helvetica; font-weight: 400;font-size: 15px; line-height: 23px; margin: 0">{!! $body !!}</p>
            </td>
        </tr>
    </tbody>
</table>