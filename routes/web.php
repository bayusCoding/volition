<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Landing
 */

//payment.callbackRoute::get('/test', 'InvestController@testMailChimp');

Route::get('reactivate/{id}', 'UserController@reactivate');


Route::view('/', 'welcome');
Route::get('/terms-and-conditions', 'HomeController@tandc')->name('users.tandc');
Route::get('/corporate-bylaw', 'HomeController@bylaw')->name('users.bylaw');
Route::get('/getting-started', 'HomeController@gettingStarted')->name('users.getting.started')->middleware('auth');

/**
 * Login & Registration
 */
Route::get('/register/request', 'Auth\RegisterController@request')->name('register.request');
Route::post('/register/request', 'ReferralController@store')->name('request.store');
Route::get('/register/account', 'Auth\RegisterController@registerAccount')->name('register.account');
Route::post('/register/referral', 'Auth\RegisterController@referralVerification')->name('referral.verify');

Route::get('/logout', 'Auth\LoginController@logout');

Auth::routes();

Route::post('/register', 'Auth\RegisterController@create')->name('register');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

/**
 * Dashboard
 */
Route::get('/home', 'HomeController@index')->name('users.home')->middleware('auth');

/**
 * Help, Contact/Messaging
 */
Route::get('/contact', 'MessageController@index')->name('users.contact')->middleware('auth');

Route::get('/messages', 'MessageController@index')->name('messages.index')->middleware('admin', 'auth');
Route::get('/messages/show', 'MessageController@show')->name('messages.show')->middleware('admin', 'auth');
Route::post('/message', 'MessageController@store')->name('users.message')->middleware('auth');
Route::get('/message/delete/{id}', 'MessageController@delete')->name('message.delete')->middleware('auth');

Route::post('/message/broadcast', 'MessageController@broadcast')->name('message.broadcast')->middleware('auth');

Route::get('/help', 'HomeController@help')->name('users.help')->middleware('auth');

/**
 * Pools and history
 */
Route::get('/pools/{pool}/invest', 'InvestController@create')->name('pools.create')->middleware('auth');
Route::get('/pools/history', 'InvestController@show')->name('pools.show')->middleware('auth');
Route::get('/pools/{pool}/history', 'InvestController@show')->name('pools.pool.show')->middleware('auth');
Route::get('/investment/details/{number}', 'InvestController@investment')->name('investment.show')->middleware('auth');

/**
 * AdminFee
 */
Route::get('pools/adminfees/renew/{pool}', 'AdminFeesController@renew')->name('adminfees.renew')->middleware('auth');
Route::post('pools/adminfees/renew/callback', 'AdminFeesController@renewCallback')->name('adminfees.renew.callback')->middleware('auth');

/**
 * Payment Routes
 */
Route::post('/pools/invest', 'InvestController@store')->name('pools.store')->middleware('auth');
Route::post('pools/invest/payment/callback', 'InvestController@handlePaymentGatewayCallback')->name('payment.callback'); //method is POST for ravepay
Route::get('pools/invest/payment/callback', 'InvestController@handlePaymentGatewayCallback')->name('payment.callback'); //method is GET for paystack

/**
 * Referral
 */
Route::get('/referrals', 'ReferralController@index')->name('referrals.index')->middleware('auth');
Route::post('/referrals/invite', 'ReferralController@invite')->name('referrals.invite')->middleware('auth');
Route::get('/referrals/history', 'ReferralController@history')->name('referrals.history')->middleware('auth');
Route::get('/referrals/noreferral', 'ReferralController@noreferral')->name('referrals.noreferral')->middleware('admin', 'auth');
Route::post('/referrals/noreferral/invite', 'ReferralController@noreferralInvite')->name('referrals.noreferral.invite')->middleware('auth');

/**
 * Transactions
 */
Route::get('/transactions', 'TransactionController@index')->name('transactions.index')->middleware('auth');
Route::get('/transactions/create', 'TransactionController@create')->name('transactions.create')->middleware('admin', 'auth');
Route::post('/transactions', 'TransactionController@store')->name('transactions.store')->middleware('admin', 'auth');
Route::get('/transactions/{transaction}/edit', 'TransactionController@edit')->name('transactions.edit')->middleware('admin', 'auth');
Route::put('/transactions/{transaction}', 'TransactionController@update')->name('transactions.update')->middleware('admin', 'auth');
Route::delete('/transactions/{transaction}', 'TransactionController@destroy')->name('transactions.destroy')->middleware('admin', 'auth');

/**
 * Log
 */
Route::get('/logs', 'LogController@index')->name('logs.index')->middleware('auth');

/**
 * Volition Blue & Categories
 */
Route::resource('/pools/blue', 'BlueController')->middleware('auth');
Route::resource('blue/categories', 'BlueCategoryController')->middleware('admin', 'auth');

/**
 * Volition Privilege & Business
 */
Route::get('/pools/privilege', 'InvestController@privilege')->name('privilege')->middleware('auth');
Route::post('/pools/privilege', 'InvestController@privilegeRegister')->name('privilege.register')->middleware('auth');

Route::get('/pools/business', 'InvestController@business')->name('business')->middleware('auth');
Route::post('/pools/business', 'InvestController@businessRegister')->name('business.register')->middleware('auth');

/**
 * Portfolio
 */
Route::resource('/portfolio', 'PortfolioController')->middleware('admin', 'auth');

/**
 * Settings
 */
Route::get('/settings', 'SettingController@index')->name('settings.index')->middleware('auth');
Route::post('/settings/update', 'SettingController@update')->name('settings.update')->middleware('auth');
Route::post('/settings/user', 'SettingController@updateUser')->name('settings.user')->middleware('auth');
Route::post('/settings/password', 'SettingController@updatePassword')->name('settings.password')->middleware('auth');
Route::get('/settings/pool', 'SettingController@getPool')->middleware('auth');
Route::post('/settings/pool', 'SettingController@updatePool')->name('settings.pool')->middleware('auth');

/**
 * Users Management
 */
Route::get('/users/archived', 'UserController@trashed')->name('users.archived')->middleware('admin', 'auth');
Route::get('/users/unarchive/{id}', 'UserController@untrash')->name('users.unarchive')->middleware('admin', 'auth');
Route::resource('/users', 'UserController')->middleware('admin', 'auth');
Route::get('/users/transaction/{user}', 'UserController@transactions')->name('users.transactions')->middleware('admin', 'auth');
Route::get('/users/referrals/{user}', 'UserController@referrals')->name('users.referrals')->middleware('admin', 'auth');
Route::get('/users/logs/{user}', 'UserController@logs')->name('users.logs')->middleware('admin', 'auth');
Route::get('/users/pools/{user}', 'UserController@pools')->name('users.pools')->middleware('admin', 'auth');
Route::get('/users/pools/create/{user}', 'UserController@poolCreate')->name('users.pools.create')->middleware('admin', 'auth');
Route::get('/users/pools/edit/{pool}', 'UserController@poolEdit')->name('users.pools.edit')->middleware('admin', 'auth');
Route::put('/users/pools/update/{pool}', 'UserController@poolUpdate')->name('users.pools.update')->middleware('admin', 'auth');
Route::delete('/users/pools/delete/{pool}', 'UserController@poolDestroy')->name('users.pools.destroy')->middleware('admin', 'auth');
Route::post('/users/pools/store', 'UserController@poolStore')->name('users.pools.store')->middleware('admin', 'auth');


Route::get(
    '/data/download/user_data',
    [
        'as' => 'data/download/user_data',
        'uses' => 'UserController@getMainUserData'
    ]
);

Route::get(
    '/data/download/investment_data',
    [
        'as' => 'data/download/investment_data',
        'uses' => 'InvestController@getMainInvestmentData'
    ]
);

Route::get(
    '/data/download/transaction_data',
    [
        'as' => 'data/download/transaction_data',
        'uses' => 'TransactionController@getMainTransactionData'
    ]
);
