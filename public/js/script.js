$(document).ready(function() {

	$('.navbar-nav').hide();
    $('.navbar-toggle').click(function() {
        $('.navbar-nav').toggle();
    });

    $('#agree').click(function() {
        //check if checkbox is checked
        if ($(this).is(':checked')) {

            $('#pay').removeAttr('disabled'); //enable input

        } else {
            $('#pay').attr('disabled', true); //disable input
        }
    });

    $('#slots').change(function() {
        $('#slots-no').html($(this).val());
        if ($('#adminFees').val() != 0) {
            $('#subscription-subtotal').html(parseInt($(this).val()) * parseInt($('#upgrade').val()));
            $('#subscription-total').html( ( parseInt($(this).val()) * parseInt($('#upgrade').val()) ) + (parseInt($('#adminFees').val())) );
        }
    })

    $('#custom-investment').keyup(function() {
        if ($(this).val() > 4999999) {
            $('#custom-subscription-subtotal').html(parseInt($(this).val()));
            $('#custom-subscription-total').html( ( parseInt($(this).val()) ) + (parseInt($('#adminFees').val())) );
        }else{
            $('#custom-subscription-subtotal').html(parseInt(0));
            $('#custom-subscription-total').html( ( 0 ) + (parseInt($('#adminFees').val())) );
        }
    })

    
    $('#startMonth').change(function() {
        month = parseInt($(this).val()) + 8
        if (month > 12) {
            month -= 12;
        }
        $('#endMonth').val(month);
    });

    $('#pool').change(function() {
        $.ajax({
            url: window.location.origin + "/settings/pool",
            type: "get",
            dataType: "json",    
            data: { 'pool': $(this).val() },
            success: function(response){
                pool = response;
                $('#adminFees').val(pool.admin_fees);
                $('#upgrade').val(pool.upgrade);
                $('#expiry').val(pool.expiry);
            },
            error: function(XMLHttpRequest, textShown, error){
                console.log(error);
            }
        });
    });

    $('#edit-profile').click(function() {
        $('#user-details-info').hide();
        $('#user-details-form').show();
    });

    //Add datatables to tables on application
    $('.datatable').DataTable( {
        dom: 'Brt',
        "paging": false,
        "aaSorting": [],
        buttons: [
            'csv'
        ]
    });


        $(".content-wrap h1").replaceWith(function () {
            return "<p>" + $(this).text() + "</p>";
        });
        $(".content-wrap h2").replaceWith(function () {
            console.log($(this).attr('class'));
            return "<p>" + $(this).text() + "</p>";
        });
        $(".content-wrap h3:not(.inner-topic)").replaceWith(function () {
            return "<p>" + $(this).text() + "</p>";
        });
        $(".content-wrap h4").replaceWith(function () {
            return "<p>" + $(this).text() + "</p>";
        });
        $(".content-wrap h5").replaceWith(function () {
            return "<p>" + $(this).text() + "</p>";
        });
        $(".content-wrap h6").replaceWith(function () {
            return "<p>" + $(this).text() + "</p>";
        });

        var fluid_box = function () {
            var highest = 0;
            var hi = 0;
            $(".content-wrap").css('height', '').each(function () {
                var h = $(this).height();
                if (h > hi) {
                    hi = h;
                    highest = $(this).height() + 60;
                }

            });
            $(".content-wrap").css('height', highest);

        }


        fluid_box();
        $(window).resize(fluid_box);

});